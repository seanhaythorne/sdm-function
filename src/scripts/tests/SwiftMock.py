import os
import logging
from django.conf import settings

class SwiftMock:
    """ Minimal mock of SwiftStore.  
        Originally attempted to use 
        https://github.com/canonical/swiftmock 
        but way too many bugs.

        This does not store or download data but can be overriden to implement additional behaviour.
        """
    def __init__(self):
        self.conn = None

    def rooturl(self):
        return os.getenv('OS_STORAGE_URL', 'https://mock/')

    def get_swiftUrl(self, container, objname):
        return os.path.join(self.rooturl(), container, objname)
        
    def getContainerPath(self, url):
        return 'container', 'path/to/file'

    def upload(self, container, uploadObjs):
        return

    def delete(self, container, delObjs):
        return

    def list(self, container, prefix):
        return []

    def download_to(self, container, objs, destdir):
        return

    def download(self, container, obj):
        return None

    def temp_url(self, url):
        return url+'?tempurl'
