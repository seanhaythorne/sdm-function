import os
import io
import json
import shutil
from tempfile import NamedTemporaryFile, TemporaryDirectory, mkdtemp
from unittest.mock import patch
from django.test import TestCase, tag
from django.conf import settings
from django.http import HttpResponse

import mocks
import scripts.core.utils
import scripts.upload
from scripts.upload import upload
from SwiftMock import SwiftMock
from dump import dump

class AuthMock():
    def post(self, url, json):
        return HttpResponse('mock')


class TestUpload(TestCase):

    @tag("upload")
    @patch.object(scripts.upload, 'SwiftStore', return_value=SwiftMock())
    @patch.object(scripts.upload, 'AuthSession', return_value=AuthMock())
    def test_get_results_by_title(self, mock1, mock2):
        TMPDIR = mkdtemp()
        RESULTZIP_PATH = os.path.join(TMPDIR, 'results.zip')
        JOBINFO_PATH = os.path.join(TMPDIR, 'jobinfo.json')
        JOB_UUID = '484393ba-7053-11ed-a884-0242ac110002'

        shutil.copyfile('scripts/tests/data/helloworld_results.zip', RESULTZIP_PATH)
        shutil.copyfile('scripts/tests/data/helloworld_jobinfo.json', JOBINFO_PATH)

        resultset = upload(
            job_uuid=JOB_UUID,
            resultzip_path=RESULTZIP_PATH,
            jobinfo_path=JOBINFO_PATH
        )
        # dump(resultset)

        self.assertIsInstance(resultset, dict)
        self.assertEqual(
            resultset.get('results')[0].get('rangeAlternates').get('dmgr:file').get('url'),
            f"https://mock/test/{JOB_UUID}/helloworld.R"
            )
