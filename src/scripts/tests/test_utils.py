import os
import io
import json
import shutil
import zipfile
from tempfile import NamedTemporaryFile, TemporaryDirectory, mkdtemp
from unittest.mock import patch
from django.test import TestCase, tag
from django.conf import settings
import scripts.core.utils
import mocks
from dump import dump

class TestUtils(TestCase):

    @tag('utils')
    def test_file_tag(self):
        test_data = [
            ({'mimetype': 'image/tiff'}, 'dmgr:tiff'),
            ({'mimetype': 'text/csv', 'genre': 'DataGenreSpeciesOccurEnv'}, 'dmgr:csv'),
            ({'mimetype': 'text/csv', 'genre': 'DataGenreSpeciesAbsenceEnv'}, 'dmgr:csv'),
            ({'mimetype': 'text/csv', 'genre': 'DataGenreSpeciesAbsence'}, 'dmgr:csv'),
            ({'mimetype': 'text/csv', 'genre': 'any-other-genre'}, 'dmgr:file'),
            ({'mimetype': 'text/any'}, 'dmgr:file')
        ]
    
        for indata, expected in test_data:
            tag = scripts.core.utils.file_tag(indata)
            self.assertEqual(tag, expected)

    @tag('utils')
    def test_save_as_file(self):
        test_data = {'mimetype': 'text/csv', 'genre': 'DataGenreSpeciesOccurEnv'}

        # Test with json data
        saved_fd = NamedTemporaryFile()
        filepath = scripts.core.utils.save_as_file(test_data, saved_fd.name, ftype='json')
        self.assertEqual(filepath, saved_fd.name)
        loaded_data = json.load(open(filepath))
        self.assertEqual(loaded_data, test_data)
        saved_fd.close()

        # Test with text data
        test_data = "This is just a text test data"
        saved_fd = NamedTemporaryFile()
        filepath = scripts.core.utils.save_as_file(test_data, saved_fd.name, ftype='anything')
        self.assertEqual(filepath, saved_fd.name)
        loaded_data = open(filepath).read()
        self.assertEqual(loaded_data, test_data)
        saved_fd.close()

    @tag('utils')
    def test_ordered_list(self):
        test_data = { 
            'ten': {'order': 10}, 
            'five': {'order': 5},
            '2ndlast': {},
            'eight': {'order': 8}, 
            'three': {'order': 3},
            'last': {},
            'four': {'order': 4}
        }
        ordered_list = scripts.core.utils.ordered_list(test_data, ordermax=100)

        # Make sure the items are ordered according to 'order' field
        self.assertEqual([item[0] for item in ordered_list], ['three', 'four', 'five', 'eight', 'ten', '2ndlast', 'last'])
        for k, value in ordered_list:
            self.assertEqual(value, test_data[k])

    @tag('utils')
    def test_gen_rscript(self):
        basepath = os.path.join(os.path.dirname(os.path.dirname(__file__)), '..')
        function = 'bioclim'
        srcfiles = [
            'scripts/tests/data/utils.R',
            f'scripts/tests/data/{function}.R'
        ]
        savedir = TemporaryDirectory()
        scripts.core.utils.gen_rscript(function, basepath, srcfiles, savedir.name)
        gen_file = os.path.join(savedir.name, f'{function}.R')
        
        # Check that generated script file exists, and content is as expected
        self.assertTrue(os.path.exists(gen_file))
        expected_script = ''
        for infile in srcfiles:
            expected_script += open(infile).read() + '\n'
        self.assertEqual(open(gen_file).read(), expected_script)

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    def test_fetch_data(self, mock1, mock2):
        test_data = {
            'url': 'http://test-server:9000/tempurl/uuid',
            'filename': '/tmp/test_data_file'
        }
        scripts.core.utils.fetch_data(test_data)
        self.assertTrue(os.path.exists(test_data['filename']))
        self.assertEqual(open(test_data['filename'], 'rb').read(), mocks.FILE_TEST_DATA)
        # Clean up tmp file
        os.remove(test_data['filename'])

    @tag('utils', 'fetch_url')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    def test_fetch_url(self, mock1, mock2):
        url = 'http://test-server:9000/test_data_file.txt'
        expected = {
            'url': url,
            'filename': '/tmp/'+scripts.core.utils.str_to_md5(url)+'/test_data_file.txt'
        }
        ret = scripts.core.utils.fetch_url(expected.get('url'), '/tmp')

        # dump(ret)

        self.assertEqual(ret[0], expected)

        self.assertTrue(os.path.exists(expected['filename']))
        self.assertEqual(open(expected['filename'], 'rb').read(), mocks.FILE_TEST_DATA)
        # Clean up tmp file
        os.remove(expected['filename'])

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_fetch_result_data_with_model_object_result(self, mock1, mock2, mock3):
        savedir = TemporaryDirectory()
        uuid = mocks.MODEL_OBJECT_RESULT_UUID
        result = scripts.core.utils.fetch_result_data(uuid, inputdir=savedir.name)
        model_object = json.load(open('scripts/tests/data/model_object_koala_bioclim_RData.json'))
        md = model_object.get('bccvl:metadata')
        self.assertEqual(result['mimetype'], md['mimetype'])
        self.assertEqual(result['species'], md['species'])
        self.assertEqual(result['url'], model_object['rangeAlternates']['dmgr:file'].get('tempurl'))
        self.assertEqual(result['filename'], os.path.join(savedir.name, uuid, os.path.basename(md['url'])))
        self.assertTrue(os.path.exists(result['filename']))
        self.assertEqual(open(result['filename'], 'rb').read(), mocks.FILE_TEST_DATA)

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_fetch_result_data_with_occur_env_result(self, mock1, mock2, mock3):
        savedir = TemporaryDirectory()
        uuid = mocks.OCCURRENCE_ENV_RESULT_UUID
        result = scripts.core.utils.fetch_result_data(uuid, inputdir=savedir.name)
        model_object = json.load(open('scripts/tests/data/occur_env_result.json'))
        md = model_object.get('bccvl:metadata')
        self.assertEqual(result['mimetype'], md['mimetype'])
        self.assertEqual(result['scientificName'], md['scientificName'])
        self.assertEqual(result['url'], model_object['rangeAlternates']['dmgr:csv'].get('tempurl'))
        self.assertEqual(result['filename'], os.path.join(savedir.name, uuid, os.path.basename(md['url'])))
        self.assertTrue(os.path.exists(result['filename']))
        self.assertEqual(open(result['filename'], 'rb').read(), mocks.FILE_TEST_DATA)

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_get_results_by_title(self, mock1, mock2, mock3):
        # Results to be used as test data
        results = json.load(open('scripts/tests/data/resultset.json')).get('results')

        # Lookup by title
        savedir = mkdtemp()
        lookups = ['Projection to current climate', 'R SDM Model object']
        result_list = scripts.core.utils.get_results(results, lookups, inputdir=savedir, op='ALL')
        self.assertEqual(len(result_list), 2)
        for i in range(len(result_list)):
            md = mocks.get_result(results, lookups[i]).get('bccvl:metadata')
            self.assertEqual(result_list[i]['mimetype'], md['mimetype'])
            self.assertEqual(result_list[i]['filename'], 
                             os.path.join(savedir, md['uuid'], os.path.basename(md['url'])))
            self.assertEqual(open(result_list[i]['filename'], 'rb').read(), mocks.FILE_TEST_DATA)
        # Clean up
        shutil.rmtree(savedir)

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_get_results_by_genre(self, mock1, mock2, mock3):
        # Results to be used as test data
        results = json.load(open('scripts/tests/data/resultset.json')).get('results')

        # Lookup by genre
        savedir = mkdtemp()
        lookups = ['genre:DataGenreSDMModel', 'genre:DataGenreCP']
        result_list = scripts.core.utils.get_results(results, lookups, inputdir=savedir, op='ALL')
        self.assertEqual(len(result_list), 2)
        for i in range(len(result_list)):
            md = mocks.get_result(results, lookups[i]).get('bccvl:metadata')
            self.assertEqual(result_list[i]['mimetype'], md['mimetype'])
            self.assertEqual(result_list[i]['filename'], 
                             os.path.join(savedir, md['uuid'], os.path.basename(md['url'])))
            self.assertEqual(open(result_list[i]['filename'], 'rb').read(), mocks.FILE_TEST_DATA)
        # Clean up
        shutil.rmtree(savedir)

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_get_results_all_option_with_one_invalid_genre(self, mock1, mock2, mock3):
        # Results to be used as test data
        results = json.load(open('scripts/tests/data/resultset.json')).get('results')

        # One of the lookups does not exists
        savedir = mkdtemp()
        lookups = ['genre:DataGenreSDMModel', 'genre:DataGenreCP', 'genre:InvalidGenre']
        with self.assertRaises(Exception) as ex:
            result_list = scripts.core.utils.get_results(results, lookups, inputdir=savedir, op='ALL')
        self.assertEqual(str(ex.exception), 'Cannot find the following results: InvalidGenre')
        shutil.rmtree(savedir)        

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_get_results_any_option_with_one_invalid_genre(self, mock1, mock2, mock3):
        # Results to be used as test data
        results = json.load(open('scripts/tests/data/resultset.json')).get('results')

        # Return all lookup items that are successful
        savedir = mkdtemp()
        lookups = ['genre:DataGenreSDMModel', 'genre:DataGenreCP', 'genre:InvalidGenre']
        result_list = scripts.core.utils.get_results(results, lookups, inputdir=savedir, op='ANY')
        self.assertEqual(len(result_list), 2)
        for i in range(len(result_list)):
            md = mocks.get_result(results, lookups[i]).get('bccvl:metadata')
            self.assertEqual(result_list[i]['mimetype'], md['mimetype'])
            self.assertEqual(result_list[i]['filename'], 
                             os.path.join(savedir, md['uuid'], os.path.basename(md['url'])))
            self.assertEqual(open(result_list[i]['filename'], 'rb').read(), mocks.FILE_TEST_DATA)
        # Clean up
        shutil.rmtree(savedir)

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_get_results_any_option_with_invalid_genres(self, mock1, mock2, mock3):
        # Results to be used as test data
        results = json.load(open('scripts/tests/data/resultset.json')).get('results')

        # None of the lookups is successful
        savedir = mkdtemp()
        lookups = ['genre:InvalidGenre', 'genre:InvalidGenre2']
        with self.assertRaises(Exception) as ex:
            result_list = scripts.core.utils.get_results(results, lookups, inputdir=savedir, op='ANY')
        self.assertEqual(str(ex.exception), 'Cannot find the following results: InvalidGenre, InvalidGenre2')
        # Clean up
        shutil.rmtree(savedir)

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_fetch_resultsets_with_SDM_resultset(self, mock1, mock2, mock3):
        # Test with SDM resultset
        resultsets = json.load(open('scripts/tests/data/resultset.json'))
        resultset_uuids = [mocks.SDM_RESULTSET_UUID]
        savedir = TemporaryDirectory()
        lookup = ['Projection to current climate']
        items = scripts.core.utils.fetch_resultsets(resultset_uuids, savedir.name, titles=lookup, searchopt="ALL")
        result = items['resultset'][0]
        self.assertEqual(len(items['resultset']), 1)
        md = mocks.get_result(resultsets['results'], lookup[0]).get('bccvl:metadata')
        self.assertEqual(result['mimetype'], md['mimetype'])
        self.assertEqual(result['filename'], 
                         os.path.join(savedir.name, md['uuid'], os.path.basename(md['url'])))
        self.assertEqual(open(result['filename'], 'rb').read(), mocks.FILE_TEST_DATA)
        self.assertEqual(items['parent_resultset'], [])
        self.assertEqual(items['thresholds'], [])
        occr_env_md = mocks.get_result(resultsets['results'], 'genre:DataGenreSpeciesOccurEnv').get('bccvl:metadata')
        self.assertEqual(items['scientificName'], occr_env_md['scientificName'])

    @tag('utils')
    @patch('urllib.request.urlopen', side_effect=mocks.urlopen_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_fetch_resultsets_with_CC_resultset(self, mock1, mock2, mock3):
        # test with CC resultset
        resultset_uuids = [mocks.CC_RESULTSET_UUID]
        savedir = TemporaryDirectory()
        lookup = ['genre:DataGenreCP', 'genre:DataGenreFP']
        items = scripts.core.utils.fetch_resultsets(resultset_uuids, savedir.name, titles=lookup, searchopt="ANY")
        result = items['resultset'][0]
        cc_resultset = json.load(open('scripts/tests/data/CC-resultset2.json'))
        md = mocks.get_result(cc_resultset['results'], 'genre:DataGenreFP').get('bccvl:metadata')
        self.assertEqual(len(items['resultset']), 1)
        self.assertEqual(result['mimetype'], md['mimetype'])
        self.assertEqual(result['filename'], 
                         os.path.join(savedir.name, md['uuid'], os.path.basename(md['url'])))
        self.assertEqual(open(result['filename'], 'rb').read(), mocks.FILE_TEST_DATA)
        cc_params = json.load(open('scripts/tests/data/CC-job.json'))['parameters']
        self.assertEqual(items['thresholds'][0], cc_params['species_distribution_models']['threshold'])

        sdm_resultset = json.load(open('scripts/tests/data/resultset.json'))
        md = mocks.get_result(sdm_resultset['results'], 'genre:DataGenreCP').get('bccvl:metadata')
        self.assertEqual(items['parent_resultset'][0]['filename'],
                         os.path.join(savedir.name, md['uuid'], os.path.basename(md['url'])))

    @tag('utils')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_fetch_dataset_with_user_tif_dataset(self, mock1, mock2, mock3):
        # Test with user uploaded tiff file
        savedir = mkdtemp()
        uuid = mocks.ENV_TIF_DATASET_UUID
        varname = 'distribute_features_template'
        items = scripts.core.utils.fetch_dataset(uuid, savedir, vars=[varname])
        self.assertEqual(len(items), 1)
        dscov = json.load(open('scripts/tests/data/env_tif_dataset.json'))
        filename = dscov['rangeAlternates']['dmgr:tiff'][varname]['url']
        tempurl = dscov['rangeAlternates']['dmgr:tiff'][varname]['tempurl']
        expected_result = {
            "layer": varname,
            "filename": os.path.join(savedir, uuid, os.path.basename(filename)),
            "url": tempurl,
            "type": 'continuous'
        }
        self.assertEqual(items[0], expected_result)
        # Clean up
        shutil.rmtree(savedir)

    @tag('utils')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_fetch_dataset_with_species_occurrence_dataset(self, mock1, mock2, mock3):
        # Test with user upload csv/point file (i.e. species occurrence file)
        savedir = mkdtemp()
        uuid = mocks.SPECIES_OCCUR_CSV_DATASET_UUID
        items = scripts.core.utils.fetch_dataset(uuid, savedir, vars=None)
        self.assertEqual(len(items), 1)
        dscov = json.load(open('scripts/tests/data/species_occurrence_csv_dataset.json'))
        filename = dscov['rangeAlternates']['dmgr:csv']['url']
        tempurl = dscov['rangeAlternates']['dmgr:csv']['tempurl']
        speciesName = dscov['bccvl:metadata']['scientificName'][0]
        expected_result = {
            "species": speciesName.replace(' ', '.'),
            "filename": os.path.join(savedir, uuid, os.path.basename(filename)),
            "url": tempurl,
            "scientificName": speciesName 
        }
        self.assertEqual(items[0], expected_result)
        # Clean up
        shutil.rmtree(savedir)

    @tag('utils')
    @patch('scripts.core.utils.fetch_data', side_effect=mocks.fetch_data_mock)
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_fetch_dataset_with_user_zipped_shapefile_dataset(self, mock1, mock2, mock3):
        # Test with user uploaded zipped shapefile
        savedir = mkdtemp()
        uuid = mocks.ZIPPED_SHAPEFILE_DATASET_UUID
        items = scripts.core.utils.fetch_dataset(uuid, savedir, vars=None)
        self.assertEqual(len(items), 1)
        dscov = json.load(open('scripts/tests/data/zipped_shapefile_dataset.json'))

        name_list = zipfile.ZipFile('scripts/tests/data/shapefile.zip', 'r').namelist()
        filename = [f for f in name_list if f.endswith('.shp')][0]
        tempurl = dscov['rangeAlternates']['dmgr:file']['tempurl']
        expected_result = {
            "filename": os.path.join(savedir, uuid, filename),
            "url": tempurl,
            "mimetype": dscov['bccvl:metadata']['mimetype']
        }
        self.assertEqual(items[0], expected_result)
        # Clean up
        shutil.rmtree(savedir)

    @tag('utils')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_fetch_dataset_with_curated_tif_dataset(self, mock1, mock2, mock3):
        # Test with curated geotif dataset
        savedir = mkdtemp()
        uuid = mocks.CURATED_TASCLIM_DATASET_UUID
        varnames = ['bioclim_02', 'bioclim_04']
        items = scripts.core.utils.fetch_dataset(uuid, savedir, vars=varnames)
        self.assertEqual(len(items), 2)
        dscov = json.load(open('scripts/tests/data/curated_tasclim_dataset.json'))
        i = 0
        for varname in varnames:
            filename = dscov['rangeAlternates']['dmgr:tiff'][varname]['url']
            tempurl = dscov['rangeAlternates']['dmgr:tiff'][varname]['tempurl']
            expected_result = {
                "layer": varname,
                "filename": os.path.join(savedir, uuid, os.path.basename(filename)),
                "url": tempurl,
                "type": 'continuous'
            }
            self.assertEqual(items[i], expected_result)
            i = i + 1
        # Clean up
        shutil.rmtree(savedir)
