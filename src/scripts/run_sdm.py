from cmath import log
import os
import os.path
import json
import glob
import logging
from csv import DictReader
from zipfile import ZipFile, ZIP_DEFLATED
import subprocess
import shutil

from .core.coverage import generate_data_coverage
from .core.utils import zipdir

log = logging.getLogger(__name__)

def usage():
    usage_str = "Usage: run_sdm --script-args \n\
                params=<params.json>"
    print(usage_str)


def get_process_env(params):
    # build an environment for sub processes (compute tasks)
    proc_env = os.environ.copy()
    proc_env['WORKDIR'] = params['env']['workdir']
    return proc_env


def run_rscript(params: dict) -> int:
    """ 
        :param params: Script parameters
        :returns: Script exit status code
    """
    log.info(f"run_rscript: env.scriptdir={params['env']['scriptdir']}")
    # params['env']['outputdir'] = os.getcwd()
    function = params['params']['function']
    r_wrapper = os.path.join(params['env']['scriptdir'], 'r_wrapper.sh')
    r_scriptfile = os.path.join(params['env']['scriptdir'], function + ".R")
    metadatafile = os.path.join(params['env']['scriptdir'], "metadata.json")
    paramsfile = os.path.join(params['env']['scriptdir'], "params.json")
    with open(paramsfile, 'w') as pf:
        json.dump(params, pf, indent=4)

    # Add the R-script, params.json and metadata as output files
    shutil.copy(paramsfile, params['env']['outputdir'])
    shutil.copy(metadatafile, params['env']['outputdir'])
    shutil.copy(r_scriptfile, params['env']['outputdir'])
    outfile = os.path.join(params['env']['outputdir'], function + '.Rout')
    cmd = ["/bin/bash", "-l", r_wrapper, r_scriptfile]

    proc = subprocess.Popen(cmd, cwd=params['env']['scriptdir'],
                            close_fds=True,
                            env=get_process_env(params),
                            stdout=open(outfile, 'w'),
                            stderr=subprocess.STDOUT)
    rpid, status, rusage = os.wait4(proc.pid, 0)
    return status


def extract_threshold_values(fname):
    # Extract the threshold value from 'loss-function-intervals-table' file
    thresholds = {}
    csvfile = open(fname, 'r')
    dictreader = DictReader(csvfile)

    # Only use the result from Sama's evaluation script.
    # row header is name of threshold, and best column used as value
    # TODO: would be nice if threshold name column would have a column header
    # as well
    for row in dictreader:
        try:
            thresholds[row['']] = float(row['best'])
        except TypeError as e:
            print(f"Couldn't parse threshold value '{row['']}' ({row['best']})"
                  f" from file '{fname}': {repr(e)}")
    return thresholds


def tag_results(params, fileset, status, pconfigPath = None):
    # Tag and generate metadata for each result file
    out_dir = params['env']['outputdir']
    function = params['params']['function']
    srcdir = os.path.dirname(os.path.dirname(__file__))
    if pconfigPath is None:
        pconfigPath = os.path.join(srcdir, 'content/toolkit', function, function+'.txt')

    outputmap: dict = json.load(open(pconfigPath)).get('output', {})

    # For CC, need to update the projection layer name and data type
    # To do: This is a hack; can we do better?
    if function == "cc":
        for f in outputmap["files"]:
            if outputmap['files'][f].get('genre') in ['DataGenreFP', 'DataGenreFP_ENVLOP']:
                if params['params']['sdm_function'] == 'maxent':
                    outputmap['files'][f]['layer'] = 'projection_suitablity'
                    outputmap['files'][f]['data_type'] = 'Continuous'
                elif params['params']['sdm_function'] in ('circles', 'convhull', 'voronoihull'):
                    outputmap['files'][f]['layer'] = 'projection_binary'
                    outputmap['files'][f]['data_type'] = 'Discrete'
                else:
                    outputmap['files'][f]['layer'] = 'projection_probability'
                    outputmap['files'][f]['data_type'] = 'Continuous'

    # match files in output map (sorted by length of glob)
    globlist = sorted(outputmap.get('files', {}).items(),
                      key=lambda item: (-len(item[0]), item[0]))

    # go through list of globs from outputmap
    resultmd = {}
    thresholds = None
    # Get the species name; from occurrence metadata for SDM, from param in CC
    speciesName = params['params'].get('species')
    if not speciesName and 'species_occurrence_dataset' in params['params']:
        speciesName = params['params'].get('species_occurrence_dataset').get('scientificName')
    for fileglob, filedef in globlist:
        # Skip item with no title, or marked as skipped
        if not filedef.get('title') or filedef.get('skip', False):
            continue

        for fname in glob.glob(os.path.join(out_dir, fileglob)):
            if fname in fileset:
                filedef['type'] = 'FILE'
                filedef['size'] = os.path.getsize(fname)
                filedef['url'] = fname

                # Only get threshold value as from the output of Sama's evaluation script
                # FIXME: should not depend on file name (has already changed once
                # and caused disappearance of threshold values in biodiverse)
                # i.e. Loss-function-intervals-table_*.csv
                if filedef.get('title') == 'Loss functions table':
                    thresholds = extract_threshold_values(fname)
                    filedef['thresholds'] = thresholds
                resultmd[os.path.basename(fname)] = generate_data_coverage(fname, filedef)
                fileset.discard(fname)

    # check archives in outputmap
    for archname, archdef in outputmap.get('archives', {}).items():
        # create archive
        farchname = os.path.join(out_dir, archname)
        # check if we have added any files
        empty = True
        with ZipFile(farchname, 'w', ZIP_DEFLATED) as zipf:
            # files to add
            for fileglob in archdef.get('files', {}):
                for fname in glob.glob(os.path.join(out_dir, fileglob)):
                    empty = False
                    zipf.write(fname, os.path.relpath(fname, out_dir))
                    fileset.discard(fname)
        # Add archieved item to be uploaded
        if not empty:
            archdef.pop('files')
            archdef['type'] = 'FILE'
            archdef['size'] = os.path.getsize(farchname)
            archdef['url'] = farchname
            # Add species name to SDM model object
            if archdef['title'] == 'R SDM Model object':
                archdef['species'] = params['params']['species_occurrence_dataset'].get('scientificName')
            resultmd[archname] = generate_data_coverage(archname, archdef)

    # Todo:  some files left in out_dir, discard them??
    if fileset:
        log.info(f'There are left over items {json.dumps(list(fileset))}')
    
    return resultmd, thresholds, speciesName


def run(*args):
    try:
        opts = dict([i.split('=') for i in args])
        fparams = opts.get('params')
        if fparams is None:
            raise Exception("Missing parameters")
    except Exception:
        usage()
        return 1

    log.info(f"run: opts={str(opts)}")
    log.info(f"run: cwd={os.getcwd()} params={str(fparams)}")

    # Load func parameters
    params = json.load(open(fparams))

    # Copy params and wrapper script
    srcdir = os.path.dirname(os.path.dirname(__file__))
    shutil.copy(os.path.join(srcdir,  'r_wrapper.sh'), params['env']['scriptdir'])
    shutil.copy(fparams, params['env']['scriptdir'])
    status = run_rscript(params)
    log.info(f"run: status={str(status)}")

    # Tag the results
    files = glob.glob(f"{params['env']['outputdir']}/**", recursive=True)
    result_files = set([i for i in files if os.path.isfile(i)])
    result_md, thresholds, speciesName = tag_results(params, result_files, status)

    # Resultset metadata: threshold values
    resultset_md = {
        'algorithm': params['params']['function']
    }
    if thresholds:
        resultset_md['thresholds'] = thresholds
    if speciesName:
        resultset_md['scientificName'] = speciesName
    if params['params'].get('sdm_function'):
        resultset_md['sdm_algorithm'] = params['params'].get('sdm_function')

    # add user context to job info file
    output_md = {
        'user': params.get('user'),
        'status': status,
        'results': result_md,
        'metadata': resultset_md
    }
    with open('jobinfo.json', 'w') as f:
        f.write(json.dumps(output_md, indent=4))

    # zip the output directory
    zipdir(params['env']['outputdir'], 'results.zip')
    return status
