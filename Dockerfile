# Docker SDM image from EC-R package
ARG BASEIMG_VERSION=1.0
FROM registry.gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/ec-rpkg:${BASEIMG_VERSION}

USER root

RUN apt-get install -y procps

RUN mkdir -p /srv/app
WORKDIR /srv/app

# Install BSSDM package (Required for Range bagging and Climatch algorithms)
RUN R -vanilla -e 'devtools::install_github("cebra-analytics/bssdm@0.1.1", upgrade="never", dependencies = TRUE)'


COPY requirements.txt /srv/app
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt
COPY ./src /srv/app

# Clean up development libraries
RUN apt-get -y purge g++

CMD python manage.py runscript run_sdm --script-args params=/sdmwork/params.json
