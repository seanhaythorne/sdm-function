import os
import os.path
import json
import magic
import zipfile
import logging
import sys
import traceback
import shutil
from django.conf import settings
from swiftclient.exceptions import ClientException
from .core.swift import SwiftStore
from .core.authsession import AuthSession
from .core.utils import file_tag, exit
from .core.metadata import Metadata
from .core.fraction_sort import fraction_sort_resultmd
from .core.exceptions import (
    ResultError,
    JobError
)

log = logging.getLogger(__name__)


def usage():
    usage_str = "Usage: upload --script-args \n\
                uuid=<job-uuid> result=<result zip-file> jobinfo=<job-info json file>"
    print(usage_str)


def run(*args):
    try:
        opts = dict([i.split('=') for i in args])
        job_uuid = opts.get('uuid')
        resultzip_path = opts.get('result')
        jobinfo_path = opts.get('jobinfo')
        if job_uuid is None or resultzip_path is None or jobinfo_path is None:
            raise Exception("Missing parameters: result zipfile or job uuid or job info file")

        upload(job_uuid, resultzip_path, jobinfo_path)

    except Exception as e:
        log.error(f"Error: {str(e)}")
        usage()
        exit(1)


def upload(job_uuid: str, resultzip_path: str, jobinfo_path: str) -> dict:
    """ Upload Results to Swift and platform API
        :param job_uuid: Job UUID
        :param resultzip_path: All zipped result data
        :param jobinfo_path: Path to jobinfo.json (produced by 'run' stage)
        :returns: Resultset
    """

    RESULT_CONTAINER = os.getenv('RESULT_CONTAINER')
    if not RESULT_CONTAINER:
        raise Exception(f"RESULT_CONTAINER env not available")

    JOB_RESULT_API_URL = os.getenv('JOB_RESULT_API_URL')
    if not JOB_RESULT_API_URL:
        raise Exception(f"JOB_RESULT_API_URL env not available")

    # Extract the Result files
    result_file_paths = None
    with zipfile.ZipFile(resultzip_path) as zipf:
        result_file_paths = zipf.namelist()
        zipf.extractall()

    # Load the Job status and Job Results' metadata
    jobinfo = None
    with open(jobinfo_path) as f:
        jobinfo = json.load(f)
        log.debug(jobinfo)

    metadata = Metadata()

    # Write Result output DataGenreMetadata as file
    result_status = metadata.write_result_metadata(jobinfo.get('results', {}))

    # Files to be uploaded to object store
    obj_store_objects = [
        {
            'source': fpath,
            'object_name': os.path.join(job_uuid, fpath)
        } for fpath in result_file_paths
    ]

    # Save result files to swift object store
    try:
        log.info(f"Result upload Swift container: {str(RESULT_CONTAINER)}")
        log.info(f"Result upload Swift container path prefix: {str(job_uuid)}")

        swiftSt = SwiftStore()
        swiftSt.upload(RESULT_CONTAINER, obj_store_objects)

        # Update Job-manager of the results
        jobinfo_results = jobinfo.get('results')

        # Apply additional sort criteria to results
        try:
            jobinfo_results = fraction_sort_resultmd(jobinfo_results)
        except Exception as e:
            log.error(e)

        # - Transform jobinfo into a Resultset with Swift URLs
        # - Provide fallback mechanism to save minimal metadata if jobinfo.results is not available.
        results = []
        for obj in obj_store_objects:

            # Minimum Result metadata
            new_md = {
                'type': 'File',
                'url': swiftSt.get_swiftUrl(RESULT_CONTAINER, obj['object_name']),
                'size': os.path.getsize(obj['source']),
                'mimetype': magic.from_file(os.path.realpath(obj['source']), mime=True)
            }

            # If no jobinfo_results is specified, then take all results
            if not jobinfo_results:
                md = new_md

            else:
                # Take only results as specified in jobinfo_results
                rpath = os.path.basename(obj['object_name'])
                md = jobinfo_results.get(rpath)
                if not md:
                    log.warning(f"Result path '{str(rpath)}' not found in jobinfo.results")
                    continue

                # Update the url with swift url
                if md['type'] in ('File', 'Coverage'):
                    bccvlmd = md['bccvl:metadata']
                    bccvlmd['url'] = new_md['url']
                    # update url of auxfiles if any
                    for auxfile in bccvlmd.get('auxfiles', []):
                        auxfile['url'] = new_md['url']

                    # Update the url in 'rangeAlternates'
                    ftag = file_tag(bccvlmd)
                    md['rangeAlternates'][ftag]['url'] = new_md['url']

                else:
                    # Unrecognised format. Assume it is a flattern dictionary
                    md.update(new_md)
            results.append(md)

        resultset = {
            'results': results,
            'metadata': jobinfo.get('metadata', {}),
            'user': jobinfo.get('user')
        }

        # Update JobManager API via an authenticated session
        session = AuthSession()
        resp = session.post(
            JOB_RESULT_API_URL.replace('uuid', job_uuid),
            json=resultset
        )
        if resp.status_code != 200:
            log.error(str(resp.content))
            raise ResultError(f"Failed to save Job {str(job_uuid)} Results: {str(resp.status_code)}")

    except ClientException as e:
        log.error(f"Result upload process failed: (Swift.ClientException) {str(e)}")
        exit(300)

    except Exception as e:
        traceback.print_exc()
        log.error(f"Result upload process failed: ({str(type(e).__name__ )}) {str(e)}")
        exit(301)

    if result_status != 0:
        errormsg = f"Result upload process completed but there was an issue creating Result metadata (status: {str(result_status)})"
        log.error(errormsg)
        exit(result_status)

    # Read in job status from previous process, and fail the process if non-zero status
    func_status = jobinfo.get('status', 1)
    if func_status != 0:
        errormsg = f"Result upload process completed but the Function execution failed with status: {str(func_status)}"
        log.error(errormsg)
        exit(func_status)

    # Read in job status from previous process, and fail the process if non-zero status
    func_status = jobinfo.get('status', 1)
    if func_status != 0:
        errormsg = f"Function execution failed with status: {str(func_status)}"
        log.error(errormsg)
        exit(func_status)

    return resultset
