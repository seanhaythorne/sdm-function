## [1.2.3]
- Update ECR package 1.2.5
- Fix for Result data Coverage generation. Will now correctly read and store the geotiff CRS metadata.

## [1.2.2]
- Allow Func integrations scripts to be placed in namespace dir. eg `function=bsrmap/func` -> `toolkit/bsrmap/func/func.R`

## [1.2.1]
- Fix for metadata assuming `layers` always exists for dataset_layers interface

## [1.2.0]
- Allow prepare_inputs `dataset_layers` interface to support `source`: `external` with `url`
- Allow prepare_inputs to search for schemas in `docs/` and `docs/{func_name}/` for better organisation of schemas 

## [1.1.7]
- Improve exception handling

## [1.1.1]
- Use Pandas.read_csv na_filter=False when parsing CSV to create Result meta. This means blank, 'NA' and other Nanish strings will be processed as is.
- Make Utils deployment aware so overrides can be applied in CI/testing [bc-116]
- Override for parameter 'download_as' handling, ie geojson download to allow for isolated testing without a JobRequest being created [bc-116]
- Improve comments, some var names
