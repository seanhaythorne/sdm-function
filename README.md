# SDM function

The SDM function pipelines consist of 3 nextflow processes:
- Fetch and prepare datasets and SDM scripts
- Run the SDM scripts
- Upload the SDM results to object store and create platform Results via the JobManager API


## SDM Function (SDM R scripts)
[SDM Function doc](src/docs/README.md)

SDM Function Docker images are build via the Gitlab pipeline.

## SDM Function Utils (Python scripts)
SDM Function Utils Docker images are build via the Gitlab pipeline.  This image provides platform support for all Workflows.

## BSRMAP (Risk Mapping R scripts)

Risk Mapping Docker images are built in the Gitlab pipeline. 

The base image `Dockerfile-bsr` will only be built when the pipeline variable `BUILD_BASE=true` is provided when executing the pipeline (Can be done via the Gitlab UI). 

## Notes
- BSRMAP should be refactored out into it's own repository
- SDM Function Utils should be refactored out into it's own repository and renamed (It is not SDM specific anymore).


## Running a pipeline and R scripts locally

Local development has the following requirements:
- Nextflow 
- Docker
- ENV variable file with platform credentials (`bin/envfile.txt`)

### Install Nextflow

```
curl -s https://get.nextflow.io | bash
```

### Run pipeline
The script `./bin/run_pipeline.sh` is designed to run a pipeline using locally built docker images. 

The exact script arguments are provided by running with no arguments.

Images must first be built with the command.

```docker-compose build```

A pipeline can then be run locally to reproduce execution similar to how it occurs in the platform.

`run_pipeline.sh`  will run any pipeline in the repo eg. ‘bioclim.nf’ with any param.json file.  This will perform all the steps the platform would in terms of pulling the data based on UUIDs, setting up the folder structure all locally. 

```
./bin/run_pipeline.sh ./helloworld.nf ./src/docs/examples/helloworld.example.json
```

Note: The final `upload_result` pipeline step will fail locally if a legitimate job UUID `JOB_ID` in `./bin/run_pipeline.sh` isn't provided.

The artifacts created by the Nextflow job:
```
./bin/work/input/
./bin/work/output/
./bin/work/scripts/
```

The Nextflow pipeline internal scratch space:
```
./work/
```

### Debuging R scripts locally.

`run_rscript.sh` is a short cut to ‘re run’ just the R script.  This only works if run_pipleine.sh has been executed atleast once before hand.

This is where the real debugging occurs.  The integration script can be tweaked and rerun, or the docker images rebuilt (see `docker-compose.yml`)

```
./bin/run_rscript.sh SCRIPT_NAME
```

### Legacy run locally documentation steps (TODO review/remove)
### To run SDM function pipeline locally from gitlab:

- copy `prepareInputs-params-ann-example.json` as `sdm-params.json`
- edit `sdm-params.json` to specify input datasets, etc 
- pass `sdm-params.json` as 'inputs' parameter, job-uuid, a file containing the environment varaibales as 'envfile', and a folder to be mounted as a volume as 'volOption' parameter as working directory
- See example below:

``` shell
nextflow run 
    https://gitlab.com/ecocommons-australia/ecocommons-platform/sdm-function
    -w /Users/s2976990/dev/sdm-test/work
    --jobUuid 39869a40-74b3-11eb-a911-0242ac140003 
    --inputs /Users/s2976990/dev/test/ann-params.json
    --envfile "--env-file /Users/s2976990/dev/test/envfile.txt"
    --volOption "--volume /Users/s2976990/dev/test:/sdmwork"
    -name job_39869a40-74b3-11eb-a911-0242ac140003_<time-stamp>  
    -with-weblog http://localhost:8000/api/tasks/
    -main-script ann.nf
```


## Running unit tests locally
```
./bin/run_tests.sh
```
