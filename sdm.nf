#!/usr/bin/env nextflow

params.jobUuid = "job-uuid"
params.volOption = "--volume $baseDir:/sdmwork"
params.inputs = "$baseDir/params.json"
Channel.fromPath(params.inputs, checkIfExists: true).set{ input_ch }

process prepareInputs {
    
    containerOptions params.volOption

    input:
    file x from input_ch

    output:
    path 'input_params.json' into input_params 

    """
    python /srv/app/manage.py runscript prepare_inputs --script-args params=$x > prepareinput.log
    """
}

process runSDM {

    containerOptions params.volOption

    // use local disk in $TMPDIR for scratch
    scratch true

    //publishDir '/Users/s2976990/dev/nextflow/results', mode: 'copy', overwrite: true

    input:
    file x from input_params

    output:
    path '**' into results

    """
    python /srv/app/manage.py runscript run_sdm --script-args params=$x > runSDM.log
    """
}

process uploadResult {

    containerOptions params.volOption

    input:
    path x from results.collect()

    output:
    file 'uploadResult.log'

    """
    python /srv/app/manage.py runscript upload --script-args uuid=$params.jobUuid files="$x" > uploadResult.log
    """
}

//results.subscribe { println it }
