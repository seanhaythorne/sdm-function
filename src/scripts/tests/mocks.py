import os
import io
import json
import shutil
import logging
from django.core.management.utils import get_random_secret_key

TEMPURL_RANDOM_SECRET = get_random_secret_key()
FILE_TEST_DATA = b'abcdef'

# Result data UUID matching those in data/resultset.json, etc
CURRENT_PROJECTION_RESULT_UUID = '7cae4b48-0959-11ec-bb9b-0242ac110002'
CC_FUTURE_PROJECTION_MAP_RESULT_UUID = '470f15c6-0a17-11ec-a611-0242ac110002'
MODEL_OBJECT_RESULT_UUID = '7cad026a-0959-11ec-bb9b-0242ac110002'
OCCURRENCE_ENV_RESULT_UUID = '7ca914d4-0959-11ec-bb9b-0242ac110002'
CC_CURRENT_PROJECTION_RESULT_UUID = '0977d7a8-a95e-11eb-ac16-0242ac110002'
CC_JOB_UUID = '31ade0f8-09e6-11ec-88a2-0242ac130003'
SDM_JOB_UUID = '097b8be4-b00d-11ec-8454-0242ac1a0003'

# Resultset UUID matching those in data/CC-resultset.json and resultset.json
SDM_RESULTSET_UUID = '7dd73278-0954-11ec-b4f1-0242ac130003'
CC_RESULTSET_UUID = '91dd9b1e-0a16-11ec-9820-0242ac130003'

# Dataset UUID
ENV_TIF_DATASET_UUID = 'a2ed5156-b704-11ec-ba80-0242ac120006'
CURATED_TASCLIM_DATASET_UUID = '33ca89fd-d3af-5ed6-b32e-87a7e06f0652'
FUTURE_TASCLIM_DATASET_UUID = 'cb99b5fd-138a-511c-83a1-3c1de86d61b2'
SPECIES_OCCUR_CSV_DATASET_UUID = 'b5c4f5c8-5baf-11ec-a45a-0242ac130005'
ZIPPED_SHAPEFILE_DATASET_UUID = 'f8d77176-b62a-11ec-b408-0242ac120006'
BSSPREAD_GENERIC_TIF_DATASET_UUID = 'a2ed5156-b704-a45a-11ec-0242ac110002'
BSSPREAD_GENERIC_CSV_DATASET_UUID = 'b5c4f5c8-5baf-11ec-a45a-0242ac130005'

log = logging.getLogger(__name__)

def job_metadata_mock(uuid):
    return {
        "title": 'Job',
        "description": 'Job description'
    }

def fetch_schema(algorithm):
    return json.load(open(f"scripts/tests/data/{algorithm}.json"))

def get_result(results, lookup):
    if lookup.find(':') == -1:
        lookup = f'title:{lookup}'
    key, value = lookup.split(':')
    for result in results:
        if result['bccvl:metadata'].get(key) == value:
            return result

def fetch_data_mock(data):
    os.makedirs(os.path.dirname(data['filename']), exist_ok=True)
    shutil.copyfile('scripts/tests/data/shapefile.zip', data['filename'])


def request_object_mock(url):
    log.debug(f"Mocking response for {str(url)}")

    # Check for tempurl 1st
    if 'tempurl' in url:
        return {'url': f'https://swift-url/container/uuid/filename#{TEMPURL_RANDOM_SECRET}'}

    if url.endswith(CURRENT_PROJECTION_RESULT_UUID):
        return json.load(open('scripts/tests/data/proj_current_koala_bioclim.dataset.json'))
    if url.endswith(MODEL_OBJECT_RESULT_UUID):
        return json.load(open('scripts/tests/data/model_object_koala_bioclim_RData.json'))
    if url.endswith(OCCURRENCE_ENV_RESULT_UUID):
        return json.load(open('scripts/tests/data/occur_env_result.json'))
    if url.endswith(SDM_RESULTSET_UUID):
        return json.load(open('scripts/tests/data/resultset.json'))
    if url.endswith(CC_RESULTSET_UUID):
        return json.load(open('scripts/tests/data/CC-resultset2.json'))
    if url.endswith(CC_CURRENT_PROJECTION_RESULT_UUID):
        return json.load(open('scripts/tests/data/CC-current-projection.json'))
    if url.endswith(CC_FUTURE_PROJECTION_MAP_RESULT_UUID):
        return json.load(open('scripts/tests/data/CC_future_projection_map_result.json'))
    if url.endswith(SDM_JOB_UUID):
        return json.load(open('scripts/tests/data/sdm_job_details.json'))
    if url.endswith(CC_JOB_UUID):
        return json.load(open('scripts/tests/data/CC-job.json'))
    if url.endswith(ENV_TIF_DATASET_UUID):
        return json.load(open('scripts/tests/data/env_tif_dataset.json'))
    if url.endswith(CURATED_TASCLIM_DATASET_UUID):
        return json.load(open('scripts/tests/data/curated_tasclim_dataset.json'))
    if url.endswith(FUTURE_TASCLIM_DATASET_UUID):
        return json.load(open('scripts/tests/data/future_tasclim_dataset.json'))
    if url.endswith(SPECIES_OCCUR_CSV_DATASET_UUID):
        return json.load(open('scripts/tests/data/species_occurrence_csv_dataset.json'))
    if url.endswith(ZIPPED_SHAPEFILE_DATASET_UUID):
        return json.load(open('scripts/tests/data/zipped_shapefile_dataset.json'))
    if url.endswith(BSSPREAD_GENERIC_TIF_DATASET_UUID):
        return json.load(open('scripts/tests/data/bsspread_generic_tif_dataset.json'))
    if url.endswith(BSSPREAD_GENERIC_CSV_DATASET_UUID):
        return json.load(open('scripts/tests/data/bsspread_generic_csv_dataset.json'))

def urlopen_mock(url):
    return io.BytesIO(FILE_TEST_DATA)


def fetch_resultsets_mock(resultsets, inputdir, titles, searchopt="ALL"):
    resultset = json.load(open('scripts/tests/data/resultset.json'))
    md = get_result(resultset['results'], titles[0]).get('bccvl:metadata')
    occr_env_md = get_result(resultset['results'], 'genre:DataGenreSpeciesOccurEnv').get('bccvl:metadata')
    return {
        'resultset': [
            {
                'url': md['url'],
                'filename': os.path.join(inputdir, md['uuid'], os.path.basename(md['url'])),
                'mimetype': md['mimetype']
            }
        ],
        'parent_resultset': [],
        'thresholds': [],
        'scientificName': occr_env_md.get('scientificName')
    }