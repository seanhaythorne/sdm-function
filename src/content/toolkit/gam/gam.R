# GAM SDM model
#
# Load packages required
library(ecocommons)

# Read in the input parameters from param.json

source_file <- EC_read_json(file="params.json")

print.model_parameters(source_file)

EC.params <- source_file$params  # species, environment data and parameters

EC.env <- source_file$env  # set workplace environment

# Print out parameters used
parameter.print(EC.params)

# set random seed
EC_set_seed(EC.params$random_seed)

# Set temp directory for terra
terra::terraOptions(tempdir = EC.env$workdir)

# Set working directory (script runner takes care of it)
setwd(EC.env$workdir)

# Set data for modelling
response <- EC_build_response(EC.params)  # species data

predictor <- EC_build_predictor(EC.params)  # environmental data

constraint <- EC_build_constraint(EC.params)  # constraint area data

# read data and constraint to region of interest
dataset <- EC_build_dataset(EC.env, predictor, constraint, response) 

# 4. Set up your model for evaluation, rearranging the input data to fit 'biomod2' format
#    - It will create a pseudo-absence if no true absence is given
#    - It will generate csv files with occurence, true absence and pseudo absence
#      points with environmental data
model_data <- EC_format_biomod2(EC.env,
                                true.absen               = dataset$absen,
                                pseudo.absen.points      = dataset$pa_number_point,
                                pseudo.absen.strategy    = EC.params$pa_strategy,
                                pseudo.absen.disk.min    = EC.params$pa_disk_min,
                                pseudo.absen.disk.max    = EC.params$pa_disk_max,
                                pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
                                climate.data             = dataset$current_climate,
                                occur                    = dataset$occur,
                                species.name             = response$occur_species,
                                save.pseudo.absen        = TRUE,
                                save.env.absen           = TRUE,
                                save.env.occur           = TRUE,
                                generate.background.data = FALSE)

# Run model & save outputs
EC_modelling_gam(EC.params, EC.env, response, predictor, constraint, dataset, model_data)
