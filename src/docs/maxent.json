{
    "name": "MAXENT",
    "description": "Predicts species occurrences by finding the distribution that is most spread out, or closest to uniform, while taking into account the limits of the environmental variables of known locations.",
    "algorithm_category": "machineLearning",
    "dependencies": "Dismo",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "http://ecocommons.org.au/sdm-function/maxent.schema.json",
        "title": "MAXENT SDM Function Parameters",
        "description": "Schema for MAXENT SDM Function parameters",
        "type": "object",
        "properties": {
            "species": {
                "title": "species occurrence dataset",
                "description": "UUID of species occurrence dataset",
                "type": "string"
            },
            "absence": {
                "title": "species absence dataset",
                "description": "UUID of species absence dataset",
                "type": [
                    "string",
                    "null"
                ],
                "default": null
            },
            "bias": {
                "title": "bias dataset",
                "description": "UUID of bias dataset",
                "type": [
                    "string",
                    "null"
                ],
                "default": null
            },
            "predictors": {
                "title": "predictors",
                "description": "List of environmental dataset UUID with a list of selected layer names.",
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "uuid": {
                            "title": "dataset id",
                            "description": "UUID of environmental dataset",
                            "type": "string",
                            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                        },
                        "layers": {
                            "title": "layers",
                            "description": "List of layer names",
                            "type": "array",
                            "items": {
                                "description": "Layer name",
                                "type": "string"
                            },
                            "minItems": 1,
                            "uniqueItems": true
                        }
                    },
                    "required": [
                        "uuid",
                        "layers"
                    ]
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "randomnullrep": {
                "title": "number of repetitions",
                "description": "Number of repetitions. parameter to nullrandom() function",
                "type": "integer",
                "minimum": 1
            },
            "randomseed": {
                "title": "random seed",
                "description": "if selected, a different random seed will be used for each run, so a different random test/train partition will be made and a different random subset of the background will be used, if applicable.",
                "type": "boolean",
                "default": false
            },
            "removeduplicates": {
                "title": "remove duplicates",
                "description": "Remove duplicate presence records. if environmental data are in grids, duplicates are records in the same grid cell, otherwise, duplicates are records with identical coordinates.",
                "type": "boolean",
                "default": true
            },
            "maximumbackground": {
                "title": "number of background points",
                "description": "The number of background points; if the number of background points/grid cells is larger than this number, then this number of cells is chosen randomly for background points.",
                "type": "integer",
                "default": 10000,
                "minimum": 0
            },
            "addsamplestobackground": {
                "title": "add samples to background",
                "description": "Add to the background any sample for which has a combination of environmental values that isn't already present in the background",
                "type": "boolean",
                "default": true
            },
            "addallsamplestobackground": {
                "title": "add all samples to background",
                "description": "Add all samples to the background, even if they have combinations of environmental values that are already present in the background",
                "type": "boolean",
                "default": false
            },
            "allowpartialdata": {
                "title": "all partial data",
                "description": "During model training, allow use of samples that have nodata values for one or more environmental variables",
                "type": "boolean",
                "default": false
            },
            "jackknife": {
                "title": "jack knife",
                "description": "Measure importance of each environmental variable by training with each environmental variable first omitted, then used in isolation. default is false.",
                "type": "boolean",
                "default": false
            },
            "randomtestpoints": {
                "title": "random test points",
                "description": "Percentage of presence localities to be randomly set aside as test points, used to compute auc, omission, etc.",
                "type": "number",
                "minimum": 0.0,
                "maximum": 100.0
            },
            "replicates": {
                "title": "replicates",
                "description": "Number of replicate runs to do when cross-validating, bootstrapping or doing sampling with replacement runs.",
                "type": "integer",
                "minimum": 1
            },
            "replicatetype": {
                "title": "replicate type",
                "description": "If replicates > 1, do multiple runs of this type.",
                "type": "string",
                "default": "crossvalidate",
                "enum": [
                    "crossvalidate",
                    "bootstrap",
                    "subsample"
                ]
            },
            "maximumiterations": {
                "title": "maximum number of iterations",
                "description": "maximum number of iterations. (algorithm parameter)",
                "type": "integer",
                "default": 500
            },
            "convergencethreshold": {
                "title": "convergence threshold",
                "description": "Stop training when the drop in log loss per iteration drops below this number.",
                "type": "number",
                "default": 0.00001
            },
            "autofeature": {
                "title": "auto feature",
                "description": "Automatically select which feature classes (restrictions) to use, based on number of training samples.",
                "type": "boolean",
                "default": true
            },
            "defaultprevalence": {
                "title": "default prevalence",
                "description": "proportion of occupied locations; default is 0.5 meaning that the proportion of presences in the study area is equal to absences. (algorithm parameter)",
                "type": "number",
                "default": 0.5
            },
            "linear": {
                "title": "linear",
                "description": "allow linear features to be used. (algorithm parameter)",
                "type": "boolean",
                "default": true
            },
            "quadratic": {
                "title": "quadratic",
                "description": "allow quadratic features to be used. (algorithm parameter)",
                "type": "boolean",
                "default": true
            },
            "product": {
                "title": "product",
                "description": "allow product features to be used. (algorithm parameter)",
                "type": "boolean",
                "default": true
            },
            "threshold": {
                "title": "threshold",
                "description": "allow threshold features to be used. (algorithm parameter)",
                "type": "boolean",
                "default": true
            },
            "hinge": {
                "title": "hinge",
                "description": "allow hinge features to be used. (algorithm parameter)",
                "type": "boolean",
                "default": true
            },
            "lq2lqptthreshold": {
                "title": "product/threshold feature threshold",
                "description": "number of samples at which product and threshold features start being used. (algorithm parameter)",
                "type": "integer",
                "default": 80
            },
            "l2lqthreshold": {
                "title": "quadratic feature threshold",
                "description": "number of samples at which quadratic features start being used. (algorithm parameter)",
                "type": "integer",
                "default": 10
            },
            "hingethreshold": {
                "title": "hinge feature threshold",
                "description": "number of samples at which hinge features start being used. (algorithm parameter)",
                "type": "integer",
                "default": 15
            },
            "betamultiplier": {
                "title": "regularization multiplier",
                "description": "multiply all automatic regularization parameters by this number; a higher number gives a more spread-out distribution. (algorithm parameter)",
                "type": "number",
                "default": 1.0
            },
            "beta_threshold": {
                "title": "threshold feature regularization",
                "description": "regularization parameter to be applied to all threshold features; negative value enables automatic setting. (algorithm parameter)",
                "type": "number",
                "default": -1.0
            },
            "beta_categorical": {
                "title": "categorical feature regularization",
                "description": "regularization parameter to be applied to all categorical features; negative value enables automatic setting. (algorithm parameter)",
                "type": "number",
                "default": -1.0
            },
            "beta_lqp": {
                "title": "linear/quadratic/product feature regularization",
                "description": "regularization parameter to be applied to all linear, quadratic and product features; negative value enables automatic setting. (algorithm parameter)",
                "type": "number",
                "default": -1.0
            },
            "beta_hinge": {
                "title": "hinge feature regularization",
                "description": "regularization parameter to be applied to all hinge features; negative value enables automatic setting. (algorithm parameter)",
                "type": "number",
                "default": -1.0
            },
            "scale_down": {
                "title": "scale down",
                "description": "Resample by scaling down (false) to the lowest resolution, otherwise scaling up to the highest.",
                "type": "boolean",
                "default": true
            },
            "random_seed": {
                "title": "random seed",
                "description": "Seed used for generating random numbers. (algorithm parameter)",
                "type": [
                    "integer",
                    "null"
                ],
                "default": null,
                "minimum": -2147483648,
                "maximum": 2147483647
            },
            "modelling_region": {
                "title": "modelling region",
                "description": "Geojson object describing the geographic constraint region",
                "type": "object",
                "properties": {
                    "geojson": {
                        "type": "string",
                        "description": "Geojson string describing the geographic constraint region"
                    },
                    "method": {
                        "type": "string",
                        "description": "Method selected for generation",
                        "enum": [
                            "convex hull",
                            "pre-defined region",
                            "environmental envelope",
                            "user-drawn",
                            "shapefile"
                        ],
                        "default": "convex hull"
                    },
                    "radius": {
                        "type": "number",
                        "description": "Buffer radius in Km",
                        "minimum": 0.0,
                        "default": 0.0
                    },
                    "predefined_region": {
                        "type": "object",
                        "properties": {
                            "region_type": {
                                "type": "string",
                                "description": "Region type selected for pre-defined region"
                            },
                            "regions": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                },
                                "description": "Regions selected for pre-defined region",
                                "minItems": 1,
                                "uniqueItems": true
                            }
                        },
                        "required": [
                            "region_type",
                            "regions"
                        ]
                    }
                },
                "required": [
                    "geojson",
                    "method"
                ]
            },
            "species_filter": {
                "title": "species filter",
                "description": "Species names to be included, only for Multi-Species SDM",
                "type": "array",
                "items": {
                    "description": "scientific name of species",
                    "type": "string"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subsets": {
                "title": "subsets",
                "description": "list of subset items, for Migratory Modelling",
                "type": "array",
                "items": {"$ref": "#/$defs/subset"},
                "minItems": 1,
                "uniqueItems": true
            },
            "unconstraint_map": {
                "title": "unconstraint map",
                "description": "Indicates whether to generate an unconstraint map or not. True by default.",
                "type": "boolean",
                "default": true
            },
            "generate_convexhull": {
                "title": "generate convex-hull polygon",
                "description": "Indicates to generate and apply a convex-hull polygon of the occurrence dataset to constraint. False by default.",
                "type": "boolean",
                "default": false
            }
        },
        "required": [
            "absence",
            "addallsamplestobackground",
            "addsamplestobackground",
            "allowpartialdata",
            "autofeature",
            "beta_categorical",
            "beta_hinge",
            "beta_lqp",
            "beta_threshold",
            "betamultiplier",
            "bias",
            "convergencethreshold",
            "defaultprevalence",
            "hinge",
            "hingethreshold",
            "jackknife",
            "linear",
            "lq2lqptthreshold",
            "l2lqthreshold",
            "maximumbackground",
            "maximumiterations",
            "modelling_region",
            "predictors",
            "product",
            "quadratic",
            "random_seed",
            "randomnullrep",
            "randomseed",
            "randomtestpoints",
            "removeduplicates",
            "replicates",
            "replicatetype",
            "scale_down",
            "species",
            "threshold"
        ],
        "$defs": {
            "subset": {
                "title": "subset",
                "description": "subset for Migratory Modelling",
                "type": "array",
                "items": [
                    {"$ref": "#/$defs/subset_name"},
                    {"$ref": "#/$defs/month_filter"},
                    {"$ref": "#/$defs/subset_predictors"}
                ]
            },
            "subset_name": {
                "title": "subset name",
                "description": "name for a subset, only for Migratory Modelling",
                "type": "string"
            },
            "month_filter": {
                "title": "month filter",
                "description": "List of numerical months to use, only for Migratory Modelling",
                "type": "array",
                "items": {
                    "description": "Numerical month (1-12)",
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 12
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictors": {
                "title": "subset predictors",
                "type": "array",
                "description": "List of predictor items",
                "items": {
                    "$ref": "#/$defs/subset_predictor"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictor": {
                "title": "subset predictor",
                "description": "a reference to a selected curated dataset with some layers",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "type":  "string",
                        "description": "uuid of dataset in the database",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "type": "array",
                        "items": {
                            "type": "string",
                            "description": "name of layer in the dataset"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    }
                },
                "required": ["uuid", "layers"]
            }
        }
    },
    "output": {
        "proj_current_*_cloglog.tif": {
            "title": "Predicted habitat suitability under current conditions based on cloglog output",
            "mimetype": "image/geotiff",
            "layer": "projection_suitability",
            "data_type": "Continuous"
        },
        "proj_current_*_logistic.tif": {
            "title": "Predicted habitat suitability under current conditions based on logistic output",
            "mimetype": "image/geotiff",
            "layer": "projection_suitability",
            "data_type": "Continuous"
        },
        "proj_current_*_cloglog.png": {
            "title": "Predicted habitat suitability map under current conditions based on cloglog output",
            "mimetype": "image/png"
        },
        "proj_current_*_logistic.png": {
            "title": "Predicted habitat suitability map under current conditions based on logistic output",
            "mimetype": "image/png"
        },
        "proj_current_*_cloglog_unconstraint.tif": {
            "title": "Predicted habitat suitability under current conditions based on cloglog output - unconstraint",
            "mimetype": "image/geotiff",
            "layer": "projection_suitability",
            "data_type": "Continuous"
        },
        "proj_current_*_logistic_unconstraint.tif": {
            "title": "Predicted habitat suitability under current conditions based on logistic output - unconstraint",
            "mimetype": "image/geotiff",
            "layer": "projection_suitability",
            "data_type": "Continuous"
        },
        "equal.sens.spec_*_cloglog.tif": {
            "title": "Presence/absence map based on equal sensitivity and specificity threshold for cloglog output",
            "mimetype": "image/geotiff",
            "data_type": "Continuous"
        },
        "equal.sens.spec_*_cloglog.png": {
            "title": "Presence/absence map (png) based on equal sensitivity and specificity threshold for cloglog output",
            "mimetype": "image/png"
        },
        "response_curves*_cloglog.png": {
            "title": "Response curves based on cloglog output",
            "mimetype": "image/png"
        },
        "response_curves*_logistic.png": {
            "title": "Response curves based on logistic output",
            "mimetype": "image/png"
        },
        "mess_*.tif": {
            "title": "Multivariate Environmental Similarity Surface (MESS)",
            "mimetype": "image/geotiff",
            "data_type": "Continuous"
        },
        "mess_*.png": {
            "title": "Multivariate Environmental Similarity Surface (MESS) map",
            "mimetype": "image/png"
        },
        "limitfactors_*.png": {
            "title": "Limiting factors map",
            "mimetype": "image/png"
        },
        "nullmodel_*.csv": {
            "title": "NULL model evaluation",
            "mimetype": "text/csv"
        },
        "roc_*.png": {
            "title": "ROC plot",
            "mimetype": "image/png"
        },
        "omcom_*.png": {
            "title": "Omission/commission plot",
            "mimetype": "image/png"
        },
        "density_*.png": {
            "title": "Presence-absence density plot",
            "mimetype": "image/png"
        },
        "evaluation_*.csv": {
            "title": "Evaluation statistics",
            "mimetype": "text/csv"
        },
        "maxent_results_*.csv": {
            "title": "Maxent results table",
            "mimetype": "text/csv"
        },
        "aoo_eoo_*.csv": {
            "title": "Area of Occupancy (AOO) and Extent of Occurrence (EOO)",
            "mimetype": "text/csv"
        },
        "aoo_eoo_year_*.csv": {
            "title": "Area of Occupancy (AOO) and Extent of Occurrence (EOO) per year table",
            "mimetype": "text/csv"
        },
        "aoo_year_*.png": {
            "title": "Area of Occupancy (AOO) per year plot",
            "mimetype": "image/png"
        },
        "eoo_year_*.png": {
            "title": "Extent of Occurrence (EOO) per year plot",
            "mimetype": "image/png"
        },
        "*.R": {
            "title": "Job script",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "mimetype": "text/x-r-transcript"
        },
        "modelling_region.json": {
            "title": "Modelling region",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "mimetype": "application/json"
        },
        "params.json": {
            "title": "Input parameters",
            "mimetype": "text/x-r-transcript"
        },
        "response_curve_*_*_*.png": {
            "title": "Individual response curves",
            "mimetype": "image/png"
        },
        "occurrence_environmental_*.csv": {
            "title": "Occurrence records with environmental data",
            "mimetype": "text/csv"
        },
        "background_environmental_*.csv": {
            "title": "Background records with environmental data",
            "mimetype": "text/csv"
        },
        "occurrence_predictors_boxplot.png": {
            "title": "Environmental data (continuous) for occurrences - boxplots",
            "mimetype": "image/png"
        },
        "occurrence_predictors_histogram.png": {
            "title": "Environmental data (continuous) for occurrences - histograms",
            "mimetype": "image/png"
        },
        "occurrence_predictors_density.png": {
            "title": "Environmental data (continuous) for occurrences - density plots",
            "mimetype": "image/png"
        },
        "occurrence_predictors_barplot.png": {
            "title": "Environmental data (categorical) for occurrences - barplots",
            "mimetype": "image/png"
        },
        "background_predictors_boxplot.png": {
            "title": "Environmental data (continuous) for background - boxplots",
            "mimetype": "image/png"
        },
        "background_predictors_histogram.png": {
            "title": "Environmental data (continuous) for background - histograms",
            "mimetype": "image/png"
        },
        "background_predictors_density.png": {
            "title": "Environmental data (continuous) for background - density plots",
            "mimetype": "image/png"
        },
        "background_predictors_barplot.png": {
            "title": "Environmental data (categorical) for background - barplots",
            "mimetype": "image/png"
        },
        "occurrence_predictors_correlations.png": {
            "title": "Predictor variables - correlations",
            "mimetype": "image/png"
        },
        "maxent_results_raw.zip": {
            "files": [
                "raw/*",
                "raw/plots/*"
            ],
            "title": "Raw maxent results",
            "mimetype": "application/zip"
        },
        "maxent_results_cloglog.zip": {
            "files": [
                "cloglog/*",
                "cloglog/plots/*"
            ],
            "title": "Cloglog maxent results",
            "mimetype": "application/zip"
        },
        "maxent_results_logistic.zip": {
            "files": [
                "logistic/*",
                "logistic/plots/*"
            ],
            "title": "Logistic maxent results",
            "mimetype": "application/zip"
        },
        "sp_locations_plot.png": {
          "title": "Presence absence/background locations",
          "mimetype": "image/png"
        }
    }
}