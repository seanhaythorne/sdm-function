import os
import sys
import re
import logging
import json
import zipfile
import shutil
import copy
import glob
import hashlib
import urllib.request
from requests.utils import requote_uri, quote
from requests.exceptions import HTTPError
from urllib.parse import urlparse
from typing import Dict, List
from django.core.management.base import CommandError
from .exceptions import (
    DatasetError,
    ResultError
)

from .authsession import AuthSession

log = logging.getLogger(__name__)

DEPLOYMENT_CI = 'CI'


def str_to_md5(str: str) -> str:
    """ Convert a string to MD5 represented as hex """
    return hashlib.md5(str.encode()).hexdigest()


def file_type(resultmd: Dict) -> str:
    mimetype = resultmd.get('mimetype')
    ftype = 'FILE'
    if mimetype in ('image/tiff', 'image/geotiff'):
        ftype = 'GEOTIFF'
    elif mimetype == 'text/csv' and resultmd.get('genre') in \
            ('DataGenreSpeciesOccurEnv',
             'DataGenreSpeciesAbsenceEnv',
             'DataGenreSpeciesAbsence'):
        ftype = 'POINT'
    return ftype


def file_tag(resultmd: Dict) -> str:
    # determine the tag to used for different file type
    ftype = file_type(resultmd)
    if ftype == 'GEOTIFF':
        return 'dmgr:tiff'
    if ftype == 'POINT':
        return 'dmgr:csv'
    return 'dmgr:file'


def zipdir(path, zipfilename):
    with zipfile.ZipFile(f'{zipfilename}', 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(path):
            for file in files:
                zipf.write(
                    os.path.join(root, file),
                    os.path.relpath(os.path.join(root, file), path)
                )


def gen_rscript(function: str, basepath: str, srcfiles: List[str], savedir: str):
    """ Generate an R script by concatenating a list of source files.
        :param function: Function name or path relative to basepath.
        :param basepath: Source scripts basepath. 
        :param srcfiles: List of source scripts.

                        This will replace the named placeholders:
                        {function} and {function_basename} with actual paths.

                        eg. function=bsrmap/combine_layers

                        {function}/{function_basename}.R -> bsrmap/combine_layers/combine_layers.R  

        :param savedir: Save script path
    """
    log.debug(f"gen_rscript: "
              f"function={function}, "
              f"basepath={basepath}, "
              f"srcfiles={srcfiles}, "
              f"savedir={savedir}")

    function_basename = os.path.basename(function)

    with open(os.path.join(savedir, function_basename+'.R'), 'w') as f:
        for sfile in srcfiles:
            with open(os.path.join(basepath, sfile.format(
                function=function,
                function_basename=function_basename
            ))) as sf:
                f.write(sf.read() + '\n')


def request_obj(url: str):
    session = AuthSession()
    resp = session.get(url)
    if resp.status_code != 200:
        raise HTTPError(f"Request failed: {url}, status_code: {resp.status_code}")
    return resp.json()


def update_obj(url: str, payload: dict):
    session = AuthSession()
    resp = session.post(
        url,
        json=payload
    )
    if resp.status_code != 200:
        raise HTTPError(f"Update failed: url: {url}, payload: {json.dumps(payload)}")


def save_as_file(obj, fpath, ftype='json'):
    # Save the object to file
    with open(fpath, 'w') as f:
        if ftype == 'json':
            f.write(json.dumps(obj, indent=4))
        else:
            f.write(obj)
    return fpath


def ordered_list(objs, sorted_by='order', ordermax=99999):
    # Return a sorted listof objects based on the sorted_by field.
    # Object without sorted_by field is append to the list
    #ordermax = 99999
    items = {}
    for k, v in objs.items():
        items[k] = v.get(sorted_by, ordermax)
        if items[k] == ordermax:
            ordermax += 1
    return [(k, objs[k]) for k in sorted(items, key=items.get)]


def fetch_dataset(ds_uuid, inputdir, vars: list = None) -> List[Dict]:
    """ Fetch Dataset
        :param ds_uuid: Dataset UUID
        :param inputdir: Workspace input directory
        :param vars: Optional list of Data layer identifiers to be fetched.
        :returns: List[Dict] for each Data layer
    """
    if not vars:
        vars = []

    log.info(f"fetch_dataset: ds_uuid={str(ds_uuid)}, inputdir={str(inputdir)}, vars={str(vars)}")

    if not os.getenv('DATASET_ITEM_API_URL'):
        raise DatasetError('Configuration error, Envvar DATASET_ITEM_API_URL is not set')

    try:
        # Fetch dataset from data-ingester
        dataset = request_obj(url=os.getenv('DATASET_ITEM_API_URL').replace('uuid', ds_uuid))
    except HTTPError as e:
        raise DatasetError(f"Error fetching Dataset ({e})") from e

    if dataset['type'] == 'File':
        # This is file as dataset
        url = dataset['rangeAlternates']['dmgr:file']['url']
        data_objs = [
            {
                "filename": os.path.join(inputdir, ds_uuid, os.path.basename(url)),
                "url": dataset['rangeAlternates']['dmgr:file']['tempurl'],
                "mimetype": dataset['bccvl:metadata'].get("mimetype")
            }
        ]

    # TODO: explicit condition here
    else:
        # This is of type Coverage: point or geotiff data
        if dataset['domain']['referencing'][0].get('type', '') == 'Point':
            url = dataset['rangeAlternates']['dmgr:csv']['url']
            tempurl = dataset['rangeAlternates']['dmgr:csv']['tempurl']
            if dataset['bccvl:metadata'].get('genre') in (
                    'DataGenreSpeciesOccurrence',
                    'DataGenreSpeciesAbsence',
                    'DataGenreSpeciesOccurrenceCollection',
                    'DataGenreSpeciesAbsenceCollection',
                    'DataGenreSpeciesTraits'):
                scientificName = vars[0] if vars else \
                    dataset['bccvl:metadata'].get('scientificName', [''])[0]
                data_objs = [
                    {
                        # Note: must not have blank for '-' in species name
                        "species": (re.sub(r"[\\ _\-,'\"/\(\)\{\}\[\]]", ".", scientificName)
                                    if scientificName
                                    else "unknown.species"),
                        "filename": os.path.join(inputdir, ds_uuid, os.path.basename(url)),
                        "scientificName": scientificName,
                        "url": tempurl
                    }
                ]
            else:
                for varname in dataset['parameters']:
                    data_objs = [
                        {
                            "layer": varname,
                            "filename": os.path.join(inputdir, ds_uuid, os.path.basename(url)),
                            "url": tempurl,
                            "type": 'categorical' if 'categories' in
                                    dataset['parameters'][varname]['observedProperty'] else 'continuous'
                        }
                    ]

        # TODO: explicit condition here
        else:
            data_objs = []
            # use filename as parameter if variable name is not specified
            if not vars:
                log.info(f"fetch_dataset: variable name not specified. Falling back to bccvl:metadata.url")

                if 'bccvl:metadata' not in dataset:
                    raise Exception(f"'bccvl:metadata' not found in Dataset")

                if 'url' not in dataset['bccvl:metadata']:
                    raise Exception(f"'url' not found in 'bccvl:metadata': {str(dataset['bccvl:metadata'])}")

                vars = [os.path.splitext(os.path.basename(dataset['bccvl:metadata']['url']))[0]]

            for varname in vars:

                if 'rangeAlternates' not in dataset:
                    raise Exception(f"'Cannot fetch Dataset. 'rangeAlternates' not found")

                if 'dmgr:tiff' not in dataset['rangeAlternates']:
                    raise Exception(f"'Cannot fetch Dataset. dmgr:tiff' not found in 'rangeAlternates'")

                if varname not in dataset['rangeAlternates']['dmgr:tiff']:
                    raise Exception(f"Cannot fetch Dataset. '{str(varname)}' not found in 'rangeAlternates[dmgr:tiff]'")

                url = dataset['rangeAlternates']['dmgr:tiff'][varname]['url']
                tempurl = dataset['rangeAlternates']['dmgr:tiff'][varname]['tempurl']

                data_objs.append(
                    {
                        "layer": varname,
                        "filename": os.path.join(inputdir, ds_uuid, os.path.basename(url)),
                        "url": tempurl,
                        "type": 'categorical' if 'categories' in
                                dataset['parameters'][varname]['observedProperty'] else 'continuous'
                    }
                )

    # To do: download in parallels
    for data in data_objs:
        fetch_data(data)

    # Unzip file if it is a zipped-shapefile
    if dataset['type'] == 'File':
        datafile = data_objs[0].get('filename')

        # Zipped Shapefile
        if data_objs[0]['mimetype'] == 'file/zipped-shapefile':
            log.info(f"coverage type=File, mimetype=file/zipped-shapefile")

            # Unzip the zipped shapefile
            with zipfile.ZipFile(datafile, 'r') as zipf:
                zipf.extractall(path=os.path.dirname(datafile))
            # Update the data object to the .shp file
            data_objs[0]['filename'] = glob.glob(os.path.dirname(datafile) + '/**/*.shp', recursive=True)[0]

        # Other mimetype fallback
        else:
            log.info(f"coverage type=File, mimetype={data_objs[0].get('mimetype')}")
            data_objs[0]['filename'] = glob.glob(os.path.dirname(datafile) + '/**/*.csv', recursive=True)[0]

    log.debug(f"fetch_dataset: -> {str(data_objs)}")
    return data_objs


def fetch_data(data: dict):
    """ Download data using via tempurl.
        :param data: { url, filename }
    """
    log.debug(f"fetch_data: {str(data)}")

    try:
        # Get the temp url 1st
        tempurl = request_obj(url=data['url'])
        # requote url to handle space and some special characters
        tempurl = requote_uri(tempurl.get('url'))
        log.debug(f"fetch_data: {tempurl}")
        with urllib.request.urlopen(tempurl) as resp:
            # Make sure directory exists
            os.makedirs(os.path.dirname(data['filename']), exist_ok=True)
            with open(data['filename'], 'wb') as f:
                shutil.copyfileobj(resp, f)

    except Exception as e:
        raise Exception(f"Download failed for {str(data['url'])}: str({e})") from e


def fetch_url(url: str, inputdir: str) -> list:
    """ Fetch data from an external URL 
        :returns: List[Dict] containing fetched url and filepath
    """
    log.info(f"fetch_url: {str(url)}")
    url_parsed = urlparse(url)
    url_md5 = str_to_md5(url)

    data = {
        'url': url,
        'filename': os.path.join(inputdir, url_md5, os.path.basename(url_parsed.path))
    }

    try:
        with urllib.request.urlopen(url) as resp:
            # Make sure directory exists
            os.makedirs(os.path.dirname(data['filename']), exist_ok=True)
            with open(data['filename'], 'wb') as f:
                shutil.copyfileobj(resp, f)

    except Exception as e:
        raise Exception(f"Download failed for {str(url)}: str({e})") from e

    return [data]


def fetch_job(uuid) -> dict:
    """ Fetch a Job
        :param uuid: Job UUID (same as the corresponding Resultset UUID)
        :returns: Job object
    """
    log.info(f"fetch_job: {str(uuid)}")
    return request_obj(url=os.getenv('JOB_ITEM_API_URL').replace('uuid', uuid))


def fetch_resultsets(resultset_uuids: list, inputdir: str, titles: list, searchopt="ALL"):
    """ Fetch the result files with the specified titles """
    projections = []
    sdm_projections = []
    thresholds = []

    try:
        for uuid in resultset_uuids:
            resultset = request_obj(url=os.getenv('RESULTSET_API_URL').replace('uuid', uuid))
            if resultset is None or resultset.get('results') is None:
                raise ResultError(f"Result download failed for '{str(uuid)}'")

            projection = get_results(resultset['results'], titles, inputdir, op=searchopt)[0]
            #projections.append({"filename": projection['filename']})
            projections.append(projection)

            occur_env_result = {}
            # For ensemble, check if input is CC
            if "CC" in resultset.get('categories', []):
                # CC projection file
                cc_md = request_obj(url=os.getenv('JOB_ITEM_API_URL').replace('uuid', resultset.get('job')))
                # To do: shall replace this with job_results. Assume dataset id and job id are the same
                sdm_uuid = cc_md['parameters']['species_distribution_models']['uuid']
                thresholds.append(cc_md['parameters']['species_distribution_models']['threshold'])

                # Get SDM projection file
                sdm_resultset = request_obj(url=os.getenv('RESULTSET_API_URL').replace('uuid', sdm_uuid))
                sdm_projection = get_results(sdm_resultset['results'], titles, inputdir, op=searchopt)[0]
                sdm_projections.append({"filename": sdm_projection["filename"]})
                # Get SDM occurrence-environmental result data
                occur_env_result = get_results(sdm_resultset['results'], [
                                               'genre:DataGenreSpeciesOccurEnv'], inputdir)[0]
            elif "SDM" in resultset.get('categories', []):
                occur_env_result = get_results(resultset['results'], ['genre:DataGenreSpeciesOccurEnv'], inputdir)[0]

    except HTTPError as e:
        raise ResultError(f"Error fetching Resultset ({e})") from e

    return {
        "resultset": projections,
        "parent_resultset": sdm_projections,
        "thresholds": thresholds,
        "scientificName": occur_env_result.get("scientificName")
    }


def fetch_result_data(uuid, inputdir):
    # Download the result file with the specified uuid into the specified directory
    try:
        data = request_obj(url=os.getenv('RESULTDATA_API_URL').replace('uuid', uuid))
    except HTTPError as e:
        raise ResultError(f"Error fetching Resultset ({e})") from e

    result = {'mimetype': data['bccvl:metadata'].get('mimetype', '')}
    for specieskey in ['scientificName', 'species']:
        if data['bccvl:metadata'].get(specieskey):
            result[specieskey] = data['bccvl:metadata'].get(specieskey)

    if data['type'] == 'File':
        tempurl = data['rangeAlternates']['dmgr:file']['tempurl']
    elif data['type'] == 'Coverage':
        if 'dmgr:tiff' in data['rangeAlternates']:
            var = next(iter(data['parameters']))
            result['layer'] = var
            tempurl = data['rangeAlternates']['dmgr:tiff'][var]['tempurl']
        else:
            tempurl = data['rangeAlternates']['dmgr:csv']['tempurl']

    data_obj = {
        "url": tempurl,
        "filename": os.path.join(inputdir, uuid, os.path.basename(data['bccvl:metadata']['url']))
    }
    fetch_data(data_obj)
    result.update(data_obj)
    if data['bccvl:metadata'].get('thresholds'):
        result['thresholds'] = data['bccvl:metadata'].get('thresholds')
    return result


def get_results(results: list, lookups: list, inputdir, op='ALL'):
    # Download and save files with specified titles/genre in inputdir.
    # The file metadata are returned in same order as specified in lookups.
    # lookups - a list of title:<title> or genre:<genre value>. Default
    # lookup is title if not specified i.e. <title>.

    def sort_by_index(e):
        return e[0]

    resultmds = []

    # lookup can be based on title or genre
    lookup_list = []
    wanted_list = {
        "title": [],
        "genre": []
    }
    for item in lookups:
        s1 = item.split(':', 1)
        if len(s1) <= 1:
            # Default is search by title
            wanted_list['title'].append(s1[0])
            lookup_list.append(s1[0])
        else:
            wanted_list[s1[0]].append(s1[1])
            lookup_list.append(s1[1])

    lookup_list2 = lookup_list.copy()
    for result in results:
        md = result['bccvl:metadata']
        baseon = None
        # pylint: disable=consider-using-dict-items
        for base in wanted_list:
            if md.get(base) in wanted_list[base]:
                baseon = base
                break

        if baseon is not None:
            resultmds.append(
                (lookup_list.index(md[baseon]), fetch_result_data(md['uuid'], inputdir))
            )
            lookup_list2.remove(md[baseon])

            if not lookup_list2:
                break

    # Check if any unmatched result item left
    if lookup_list2:
        if op == "ALL":
            # All result items must be found
            raise ResultError(f"Cannot find the following results: {','.join(lookup_list2)}")
        if op == "ANY":
            # Any one result item must be found
            if len(lookup_list2) >= len(lookups):
                raise ResultError(f"Cannot find the following results: {', '.join(lookup_list2)}")

    resultmds.sort(key=sort_by_index)
    return [md[1] for md in resultmds]


def exit(code: int, filepath='/sdmwork/exitcode'):
    """ Exit with code and write file 'exit_code' to Job workdir 
        TODO: hardcoded path. 'workdir' prob needs to be added as a script arg to all pipelines :|
    """
    try:
        with open(filepath, 'w') as f:
            f.write(str(code))
    except Exception as e:
        log.error(e)

    raise CommandError(code)
