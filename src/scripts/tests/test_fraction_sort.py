import json
from django.test import TestCase, tag
from scripts.core import fraction_sort
from dump import dump

def _get_resultmd(file_name) -> dict:
    return json.load(open("scripts/tests/data/{}".format(file_name))).get("results", {})


test_file_names = ["bsspread_2_jobinfo.json"]

test_expected_results = json.load(open("scripts/tests/data/fraction_sort_results.json"))


class TestFractionSort(TestCase):
    
    @tag("fun")
    def test_fraction_sort_resultmd(self):
        for file_name in test_file_names:
            origin_resultmd = _get_resultmd(file_name)
            resultmd = fraction_sort.fraction_sort_resultmd(origin_resultmd)

            # test all results have new index as expected
            expected_resultmd_index_maps: dict = test_expected_results["expected_resultmd_index_maps"][file_name]

            for result_name, new_index in expected_resultmd_index_maps.items():
                self.assertEqual(
                    resultmd[result_name]["bccvl:metadata"]["order"],
                    new_index
                )

            # make sure 'non glob' results are unmodified
            non_golb_result_names: list = test_expected_results["non_golb_result_names"][file_name]

            for non_golb_result_name in non_golb_result_names:
                self.assertEqual(
                    resultmd[non_golb_result_name],
                    origin_resultmd[non_golb_result_name]
                )
