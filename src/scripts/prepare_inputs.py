import os
import json
import logging
from requests.exceptions import ConnectionError, HTTPError, Timeout, TooManyRedirects
from .core.utils import (
    update_obj,
    gen_rscript,
    exit,
)
from .core.parameters import (
    prepare_parameters,
)
from .core.exceptions import (
    DatasetError,
    ResultError,
    JobError
)
from .core.metadata import Metadata

log = logging.getLogger(__name__)

SRC_DIR = os.path.dirname(os.path.dirname(__file__))
SCHEMA_DIR_DEFAULT = os.path.join(SRC_DIR, 'docs')
TOOLKIT_DIR_DEFAULT = os.path.join(SRC_DIR, 'content/toolkit')
CURRENT_DIR = os.getcwd()
    

def usage():
    usage_str = "Usage: prepare_inputs --script-args \n\
                function=<function name> \n\
                algorithm=<algorithm name> DEPRECATED Use function\n\
                workdir=<working dir> \n\
                params=<params.json> \n\
                jobuuid=<jobuuid> \n\
                [schemadir=<schema dir>] \n\
                [toolkitdir=<toolkit dir>]"
    print(usage_str)


def init_params(user: str, workdir: str) -> dict:
    params = {
        "user": user,
        "env": {
            "workdir": workdir,
            "inputdir": f"{workdir}/input",
            "outputdir": f"{workdir}/output",
            "scriptdir": f"{workdir}/script",
            "tmpdir": f"{workdir}/tmp"
        }
    }
    for item in params['env']:
        log.debug(f"creating dir '{params['env'][item]}'")
        os.makedirs(params['env'][item], exist_ok=True)
    return params

def run(*args):
    pconfig = None
    params = None

    log.info(f"current_dir   = {CURRENT_DIR}")
    try:
        opts = dict([i.split('=') for i in args])
        function = str(opts.get('function', opts.get('algorithm'))).lower()
        params_path = opts.get('params')
        workdir = opts.get('workdir')
        jobuuid = opts.get('jobuuid')
        schemadir = opts.get('schemadir', SCHEMA_DIR_DEFAULT)
        toolkitdir = opts.get('toolkitdir', TOOLKIT_DIR_DEFAULT)
        if params_path is None or function is None or workdir is None:
            raise Exception("Missing parameters")

        log.info(f"function     = '{function}'")
        log.info(f"params_path  = '{params_path}'")
        log.info(f"work_dir     = '{workdir}'")
        log.info(f"jobuuid      = '{jobuuid}'")
        log.info(f"schema_dir   = '{schemadir}'")
        log.info(f"toolkit_dir  = '{toolkitdir}'")

    except Exception:
        usage()
        exit(1)

    try:
        params = json.load(open(params_path))
    except Exception as e:
        log.error(f"Could not parse input parameters '{str(params_path)}' ({e})")
        exit(100)

    user = params.get('user')
    if not user or not user.get('uuid'):
        log.error("User is not specified!")
        exit(101)

    script_params = init_params(user, workdir)
    inputdir = script_params['env']['inputdir']

    func_basename = os.path.basename(function)
    
    # Load the input parameters and metadata configurations
    pconfig_path = os.path.join(toolkitdir, function, func_basename+'.txt')

    with open(pconfig_path) as f1:
        try:
            log.info(f"Loading parameter config file '{function}.txt'")
            pconfig = json.load(f1)
        except Exception as e:
            log.error(f"Could not parse parameter config '{str(f1)}' ({e})")
            exit(102)

        # Prepare and fetch input datasets and parameters
        try:
            pparams, pinfo = prepare_parameters(func_basename, params, pconfig, inputdir)
            script_params['params'] = pparams
            log.debug(json.dumps(script_params, indent=4))

            paramfile = os.path.join(CURRENT_DIR, 'input_params.json')
            with open(paramfile, 'w') as f:
                f.write(json.dumps(script_params, indent=4))
                log.info(f"Written input_params.json")

            # Generate script file
            gen_rscript(function, toolkitdir, pconfig["script"], script_params['env']['scriptdir'])

            # Generate the input metadata information
            # Remove datasets and the derived parameters from input parameters
            # But skip those parameters needed for metadata generation
            skipped_params = [
                value.get('link_pparam')
                for key, value in pconfig['metadata'].items()
                if isinstance(value , dict) and value.get('link_pparam') is not None
            ]
            for item in pinfo["datasets"]:
                if item not in skipped_params:
                    pparams.pop(item, None)

            # Update job title to job-manager
            # To do: This is a hack. Remove it.
            if func_basename == "cc":
                try:
                    update_obj(
                        url=os.getenv('JOB_RESULT_API_URL').replace('uuid', jobuuid),
                        payload={
                            'projection_name': script_params['params']['projection_name'],
                            'user': user
                        }
                    )
                except Exception as e:
                    log.error(e)

            md = Metadata(schemadir = schemadir)

            md.generate_job_metadata(
                jobuuid, 
                params, 
                pparams, 
                pconfig, 
                script_params['env'], 
                skipped_params
            )

        except DatasetError as e:
            log.error(f"({str(type(e).__name__ )}) {str(e)}")
            exit(103)

        except ResultError as e:
            log.error(f"({str(type(e).__name__ )}) {str(e)}")
            exit(104)

        except JobError as e:
            log.error(f"({str(type(e).__name__ )}) {str(e)}")
            exit(105)
            