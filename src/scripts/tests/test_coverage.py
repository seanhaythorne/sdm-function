import os
import io
import json
import shutil
from tempfile import NamedTemporaryFile, TemporaryDirectory, mkdtemp
from unittest.mock import patch
from django.test import TestCase, tag
from django.conf import settings
from django.http import HttpResponse

import mocks
import scripts.core.utils
from scripts.core.coverage import generate_data_coverage
from SwiftMock import SwiftMock
from dump import dump


class AuthMock():
    def post(self, url, json):
        return HttpResponse('mock')


class TestCoverage(TestCase):

    @tag("ok", "coverage")
    def test_generate_data_coverage(self):

        EXPECTED_REF_ID = 'http://www.opengis.net/def/crs/EPSG/0/3577'
        FILENAME = 'scripts/tests/data/spatial.tif'

        MD = {
            "title": "Region",
            "genre": "DataGenreSpatialRaster",
            "mimetype": "image/geotiff",
            "layer": "region",
            "data_type": "Continuous",
            "type" : "FILE",
            "size" : os.path.getsize(FILENAME),
            "url" : FILENAME
        }

        cov = generate_data_coverage(FILENAME, MD)

        self.assertIsInstance(cov, dict)
        self.assertEqual(
            cov.get('domain')
            .get('referencing')[0]
            .get('system')
            .get('id')
            , EXPECTED_REF_ID)