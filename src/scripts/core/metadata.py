import os
import json
import logging
import scripts.core.utils
from requests.exceptions import HTTPError
from .exceptions import JobError, MetadataError

log = logging.getLogger(__name__)

EC_CITATION = os.getenv("EC_CITATION", "How to cite EcoCommons Australia: EcoCommons Australia 2022. EcoCommons Australia - " \
              "the platform of choice for ecological and environmental modelling " \
              "(https://www.ecocommons.org.au/). EcoCommons received investment from the Australian Research " \
              "Data Commons (ARDC) and all the other investors. The ARDC is funded by the National " \
              "Collaborative Research Infrastructure Strategy (NCRIS) https://doi.org/10.47486/PL108")


class Metadata:
    def __init__(self, schemadir: str = None):
        self.schemadir = schemadir

    def generate_job_metadata(
        self,
        jobuuid: str, 
        params: dict, 
        pparams: dict, 
        config: dict, 
        penv: dict, 
        skip_params: list = None
    ) -> dict:

        def process_dataset(p: dict) -> dict:
            if not isinstance(p, dict):
                log.debug(p)
                raise MetadataError(f"generate_job_metadata.process_dataset: Expected dict, got {type(p)}")

            dsitem = self.get_dataset_metadata(p.get("uuid"), vars=p.get("layers"))
            # Add in the layer associated item if any
            for dsk, dsv in p.items():
                if dsk in ('uuid', 'layers', 'source_type'):
                    # Exclude these
                    continue
                dsitem[dsk] = dsv
            return dsitem

        def process_param_layer(p: dict) -> dict:
            if not isinstance(p, dict):
                log.debug(p)
                raise MetadataError(f"generate_job_metadata.process_param_layer: Expected dict, got {type(p)}")

            source_type = p.get('source_type')

            if source_type == 'resultset':
                return self.get_input_job_metadata(p["uuid"], penv['inputdir'])

            if source_type == 'dataset':
                return process_dataset(p)

            log.warning(f"generate_job_metadata: pname '{p1}' unknown source_type '{source_type}'. Skipping.")
            return None

        if skip_params is None:
            skip_params = []

        md = {
            'Job': self.get_job_metadata(jobuuid),
            'Parameters': self.get_input_parameters(pparams, skip_params),
            "Citation": os.getenv('CITATION', EC_CITATION)
        }

        p1 = None

        for key, val in config['metadata'].items():
            p1 = val['param'] if isinstance(val, dict) else val
            if key in md:
                # replace the existing item in md
                if p1 != "default":
                    md[key] = p1
                continue

            if p1 not in config['input']:
                raise MetadataError(f"generate_job_metadata: Property '{str(p1)}' was not found in config[input]")

            pplural = config['input'].get(p1).get('plural', None)
            ptype = config['input'].get(p1).get('type', None)

            log.debug(f"generate_job_metadata: params: {str(params)}")

            if not ptype:
                raise MetadataError(f"generate_job_metadata: Property '{str(p1)}' has no type")

            ptype = config['input'].get(p1)['type']
            # Handling null value
            if params.get(p1) is None:
                md[key] = None
                continue

            if ptype == 'dataset':
                p2 = val.get('link_param') if isinstance(val, dict) else None
                md[key] = self.get_dataset_metadata(
                    params.get(p1),
                    vars=params.get(p2) if p2 is not None else None
                )
                
            elif ptype == 'dataset_layer':

                # One to one param
                if pplural == 'single':
                    md[key] = process_param_layer(params[p1])

                # One to many param
                else:
                    md[key] = [
                        process_param_layer(p) for p in params.pop(p1)
                    ]

            elif ptype == 'jobs':
                # input jobs' metadata (i.e. ensemble)
                md[key] = [
                    self.get_input_job_metadata(rs_uuid, penv['inputdir']) for rs_uuid in params.get(p1)
                ]

            elif ptype == 'job':
                # input job for CC
                rename = config['input'].get(p1)['rename']
                for key2, val2 in rename.items():
                    if val2['type'] == 'resultfile' and val2.get('key'):
                        rs_uuid =  params.get(p1)[val2.get('key')]
                        md[key] = self.get_input_job_metadata(rs_uuid, penv['inputdir'])
                        break

            elif ptype == 'future_dataset_layer':
                # future dataset layers fpr CC
                rename = config['input'].get(p1)['rename']
                p2 = val.get('link_pparam') if isinstance(val, dict) else None
                for key2, val2 in rename.items():
                    if val2['type'] == 'dataset_layer':
                        md[key] = [
                            self.get_dataset_metadata(ds_uuid, vars=pparams[p2]) for ds_uuid in params.get(p1)
                        ]

        with open(os.path.join(penv['scriptdir'], 'metadata.json'), 'w') as f:
            f.write(json.dumps(md, indent=4))

        return md


    def get_dataset_metadata(self, ds_uuid: str, vars: list = None):
        if not ds_uuid:
            return None
        covjson = scripts.core.utils.request_obj(url=os.getenv('DATASET_ITEM_API_URL').replace('uuid', ds_uuid))
        #log.debug(f"dataset_metadata: ds_uuid: {ds_uuid}, {str(covjson)}")

        if covjson['type'] == 'File':
            return {
                "title": covjson["bccvl:metadata"].get("title"),
                "description": covjson["bccvl:metadata"].get("description"),
                "attributions": covjson["bccvl:metadata"].get("attributions"),
                "mimetype": covjson["bccvl:metadata"].get("mimetype")
            }

        if covjson["domain"]["domainType"] == "Grid":
            if not vars:
                # use filename as layer name if not specified
                vars = [ os.path.splitext(os.path.basename(covjson["bccvl:metadata"]['url']))[0] ]
            md = {
                "title": covjson["bccvl:metadata"].get("title"),
                "citation": covjson["bccvl:metadata"].get('acknowledgement', []),
                "data link": covjson["bccvl:metadata"].get('external_url'),
                "geographic extent": covjson["bccvl:metadata"].get('spatial_domain'),
                "year": covjson["bccvl:metadata"].get("year"),
                "resolution": covjson["bccvl:metadata"].get("resolution"),
                "spatial projection": covjson["domain"].get("referencing")[0].get("system"),
                "version": covjson["bccvl:metadata"].get('version'),
                "license": covjson["bccvl:metadata"].get("license"),
                "layers": [
                    f'{varname} - {covjson["parameters"][varname]["observedProperty"]["label"]["en"]}'
                    for varname in vars
                ]
            }
            # future datasets
            if str(covjson["bccvl:metadata"].get("time_domain", "")).lower() == "future":
                md.update({
                    "emission scenario": covjson["bccvl:metadata"].get("emsc"),
                    "general circulation model": covjson["bccvl:metadata"].get("gcm"),
                    "regional climate model": covjson["bccvl:metadata"].get("rcm"),
                })
        else:
            # Can be occurrence/absence/species-trait/spatial-point
            # Assume it is occurrence/absence/multi-species/multi-absence
            md = {
                "title": covjson["bccvl:metadata"].get('title'),
                "species": covjson["bccvl:metadata"].get("scientificName"),
                "number of records": covjson["bccvl:metadata"].get("count"),
                "attributions": covjson["bccvl:metadata"].get("attributions"),
                "filters": None
            }

            genre = covjson["bccvl:metadata"].get('genre', '')
            if genre == 'DataGenreSpatialPoint':
                md.pop("species")
            elif genre == 'DataGenreSpeciesTraits':
                if vars:
                    # Traits/environmental variables
                    translation = {
                        "trait_con": "trait (continuous)",
                        "trait_ord": "trait (ordinal)",
                        "trait_nom": "trait (nominal)",
                        "env_var_con": "environmental variable (continuous)",
                        "env_var_cat": "environmental variable (categorical)",
                        "random_con": "random (continuous)",
                        "random_cat": "random (categorical)"
                    }
                    md.update({"traits/environmental variables": [f"{k} - {translation[v]}" for k, v in vars.items()]})
        return md


    def get_job_metadata(self, uuid: str) -> dict:
        if not os.getenv('JOB_ITEM_API_URL'):
            raise Exception('JOB_ITEM_API_URL is empty')

        try:
            job = scripts.core.utils.request_obj(url=os.getenv('JOB_ITEM_API_URL').replace('uuid', str(uuid)))
        except HTTPError as e:
            log.warning(f"get_job_metadata: Error fetching Job ({e}). No Job description available.")
            return {}

        return {
            "title": job['title'],
            "description": job['description']
        }


    def fetch_schema(self, function: str) -> dict:
        """ Load the Function metadata JSONSchema
            This will search:
                - {schemadir}/{function}.json
                - {schemadir}/{function}/{function}.json

            :param function: Function name
            :returns: JSONSchema metadata
        """
        if not self.schemadir:
            raise Exception('Cannot fetch metadata schema. Schemadir is not set')

        schema = None
        schema_path = str(os.path.join(self.schemadir, f"{function}.json"))

        if not os.path.isfile(schema_path):
            log.info(f"Cannot find metadata schema ({str(schema_path)}), trying {function}/{function}.json")
            schema_path = str(os.path.join(self.schemadir, function, f"{function}.json"))

        if not os.path.isfile(schema_path):
            raise Exception(f"Cannot find metadata schema ({str(schema_path)})")

        log.info(f"Fetching metadata schema '{schema_path}'")
        with open(schema_path) as f:
            schema = json.load(f)
        return schema


    def get_input_parameters(self, params: dict, skip_params=None) -> dict:
        if skip_params is None:
            skip_params = []

        md = {}
        properties = self.fetch_schema(params['function'])["input"]["properties"]
        for key, val in params.items():
            # Skip these parameters
            if key in skip_params:
                continue
            if key == 'scale_down':
                val = f"{val} {'(finest)' if val else '(coarsest)'}"
            if key in properties:
                md.update({f"{properties[key].get('title', key)} ({key})": val})
            else:
                md.update({key: val})
        return md


    def get_input_job_metadata(self, rs_uuid : str, savedir: str) -> dict:
        resultset = scripts.core.utils.request_obj(url=os.getenv('RESULTSET_API_URL').replace('uuid', rs_uuid)) 

        # Get the input SDM/CC job metadata
        try:
            md = scripts.core.utils.get_results(resultset['results'], ['Metadata'], savedir)[0]
            with open(md.get('filename')) as f1:
                return json.load(f1)
        except Exception as e:
            # To do: probably shall generate one via an API call
            print(f"Failed to get job's metadata: {e}")
            return {}


    def get_resultset_metadata(self, function: str) -> dict:
        schema = self.fetch_schema(function)["output"]
        md = {}
        for key, val in schema:
            md.update({key: {
                "title": val["title"],
                "file type": val["mimetype"]
            }})
        return md


    def write_result_metadata(self, results: dict) -> int:
        try:
            resultmds = [md.get('bccvl:metadata') for md in results.values()]
            for md in resultmds:
                if md.get('genre', '') == 'DataGenreMetadata':
                    with open(os.path.basename(md['url']), "r+") as f:
                        metadata = json.load(f)
                        metadata['Results'] = [
                            {
                                key: value
                                for key,value in md.items()
                                if key in ('title', 'genre', 'mimetype', 'layer', 'data_type')
                            } for md in resultmds
                        ]
                        # Overwrite the metadata file
                        f.seek(0)
                        json.dump(metadata, f, indent=4)
                        f.truncate()
                    break
        except Exception as e:
            # Ignore error
            print(f"Fail to write result output metadata: {e}")
            return 1
        return 0
