#!/usr/bin/env bash
# LOCAL DEV

# Execute and debug R scripts locally within a nextflow environment and container.
# This expects the appropriate Nextflow pipeline has been run at least once prior 
# (run_pipeline.sh) to setup the script data and parameter environment.

set +x

[[ -z $1 ]] && printf "Usage: run_rscript.sh SCRIPT_NAME [IMAGE]\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DOCKER_IMAGE=${2:-"sdm-function/sdm:latest"}

docker run -v "${SCRIPT_DIR}/work:/sdmwork/" --rm $DOCKER_IMAGE bash -c "cd /sdmwork/script/; R < /sdmwork/script/$1 --no-save"