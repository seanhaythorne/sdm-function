# Species Traits CTA model
#
# Load packages required
library(ecocommons)

# Read in the input parameters from param.json
params <- EC_read_json(file="params.json")

# set terra tmpdir to job scratch space
terra::terraOptions(tempdir = params$env$workdir)

# Run model & save outputs
EC_modelling_sptraits_CTA(params)
