# CLIMATCH SDM model
#
library(ecocommons)

# Read in the input parameters from param.json

source_file <- EC_read_json(file = "params.json")

print.model_parameters(source_file)

EC.params <- source_file$params # species, environment data and parameters

EC.env <- source_file$env # set workplace environment

# Print out parameters used
parameter.print(EC.params)

# set random seed
EC_set_seed(EC.params$random_seed)

# Set temp directory for terra
terra::terraOptions(tempdir = EC.env$workdir)

# Set working directory (script runner takes care of it)
setwd(EC.env$workdir)

# Set data for modelling
response <- EC_build_response(EC.params) # species data

predictor <- EC_build_predictor(EC.params) # environmental data

constraint <- EC_build_constraint(EC.params) # constraint area data

# read data and constraint to region of interest
dataset <- EC_build_dataset(EC.env, predictor, constraint, response)

# 4. Set up your model for evaluation, rearranging the input data to fit
#    'biomod2' format
#    - It will create a pseudo-absence if no true absence is given
#    - It will generate csv files with occurrence, true absence and pseudo
#      absence points with environmental data
model_data <- EC_format_biomod2(EC.env,
  true.absen               = dataset$absen,
  pseudo.absen.points      = dataset$pa_number_point,
  pseudo.absen.strategy    = EC.params$pa_strategy,
  pseudo.absen.disk.min    = EC.params$pa_disk_min,
  pseudo.absen.disk.max    = EC.params$pa_disk_max,
  pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
  climate.data             = dataset$current_climate,
  occur                    = dataset$occur,
  species.name             = response$occur_species,
  save.pseudo.absen        = TRUE,
  save.env.absen           = TRUE,
  save.env.occur           = TRUE,
  generate.background.data = FALSE
)


library(bssdm)
model_compute <- model_data

# Set parameters to perform modelling

# Specify the algorithm running the experiment
model_algorithm <- "climatch"

species_algo_str <- ifelse(is.null(EC.params$subset),
  paste0(
    response$occur_species, "_",
    model_algorithm
  ),
  paste0(
    response$occur_species, "_",
    model_algorithm, EC.params$subset
  )
)

# Extract occurrence and absence data
coord <- cbind(model_compute@coord, model_compute@data.env.var)
occur <- coord[c(which(model_compute@data.species == 1)), names(coord)]

# if no true absence is given, use pseudo-absence
absen <- coord[c(which(model_compute@data.species == 0 |
  is.na(model_compute@data.species))), names(coord)]

# rename x and y to lon and lat if needed
names(occur)[names(occur) == "x"] <- "lon"
names(occur)[names(occur) == "y"] <- "lat"

# Climatch specific requirements

if (!all(predictor$type == "continuous")) {
  stop("Model doesn't run as Climatch not support categorical data")
} else {
  setwd(EC.env$outputdir)

  # run climatch with raster of enviro data, occurrence points, and other config

  # sd_data isn't used currently?
  # reading fine res stack into mem as data frame will probably crash
  # sd_data <- raster::as.data.frame(dataset$current_climate_orig,
  #   xy = TRUE, na.rm = TRUE
  # ) #### conditional on params config

  model_sdm <- climatch(
    x = dataset$current_climate, # _orig,
    p = occur[, c("lon", "lat")],
    algorithm = EC.params$algorithm, #### from params
    d_max = EC.params$d_max, # km #### from params
    sd_data = NULL, #### from params - not implemented?
    as_score = EC.params$as_score
  )

  # Workaround for CC
  methods::setClass("Climatch",
                  contains = 'DistModel',
                  slots = c(method = "character",
                            algorithm = "character",
                            variables = "character",
                            sd = "numeric",
                            presence = "data.frame",
                            coordinates = "data.frame",
                            as_score = "logical"))
  model_sdm <- methods::new("Climatch",
                            method = model_sdm@method,
                            algorithm = model_sdm@algorithm,
                            variables = model_sdm@variables,
                            sd = model_sdm@sd,
                            presence = model_sdm@presence,
                            coordinates = model_sdm@coordinates,
                            as_score = model_sdm@as_score)

  # save out the model object
  EC_save(model_sdm,
    name = paste0(species_algo_str, "_model_object.rds"),
    EC.env$outputdir
  )

  # Prediction over current climate scenario

  # 1. projection without constraint if all env data layers are continuous
  if (constraint$genUnconstraintMap &&
    (!is.null(constraint$constraints) || constraint$generateCHull)) {
    model_proj <- predict(
      object = model_sdm,
      x = dataset$current_climate_orig,
      raw_output = FALSE
    )

    # save the projection in png and tif files
    projection_strings <- EC_save_projection_prep(
      EC.env,
      species_algo_str,
      filename_ext = "unconstrained"
    )

    model_proj <- terra::writeRaster(model_proj,
      filename = projection_strings$filename_tif
    )

    EC_save_projection_generic(
      model_proj = model_proj,
      filename_gg = projection_strings$filename_gg,
      filename_tif = projection_strings$filename_tif,
      plot_title = projection_strings$plot_title,
      move_tiff = FALSE
    )
  }

  # 2. projection with constraint
  model_proj <- predict(
    object = model_sdm,
    x = dataset$current_climate,
    raw_output = FALSE
  )

  projection_strings <- EC_save_projection_prep(
    EC.env,
    species_algo_str
  )

  model_proj <- terra::writeRaster(model_proj,
    filename = projection_strings$filename_tif
  )

  # save the projection in png and tif files
  EC_save_projection_generic(
    model_proj = model_proj,
    filename_gg = projection_strings$filename_gg,
    filename_tif = projection_strings$filename_tif,
    plot_title = projection_strings$plot_title,
    move_tiff = FALSE
  )

  # evaluate model
  if (!is.null(absen)) {
    EC_save_dismo_eval(
      EC.env,
      model_algorithm,
      model_sdm,
      occur,
      absen,
      species_algo_str
    )
  }
}

message("Climatch profile completed!\n")
