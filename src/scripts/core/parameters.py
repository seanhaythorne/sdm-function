import os
import sys
import re
import logging
import json
import zipfile
import copy
import jsonpath_rw_ext
from typing import Dict

import scripts.core.utils

from .authsession import AuthSession
from .exceptions import ParameterError, DatasetError
from .utils import (
    DEPLOYMENT_CI,
    ordered_list
)


log = logging.getLogger(__name__)


def prepare_parameters(function: str, params: dict, pconfig: dict, savedir: str) -> tuple:
    """ 
        Apply transformations for parameter values based on predefined 'parameter_config' rules specified for a Function.

        :param function: function name
        :param params: Parameter dictionary
        :param pconfig: Parameter definition
        :param savedir: Data save dir path
        :returns: prepared parameters, metadata

        Parameter config types:

            'dataset_layer'
                A generic interface to describe any platform Dataset or Result. This will extract the Dataset data into the workspace.
                - For Dataset is expected to provide Dataset UUID and include one or more layers (ignored if there is only one).
                - For Result is expected to provide UUID for specific Result 'data'.

                Parameter config implements the following:
                    - May set 'plural' boolean indicates whether is Dataset or Dataset[]
                    - May set 'rename' change the parameter key
                    - May implement 'resultset_files' for Result (TODO document me)

                Parameter object interface: (exclude [] for plural=false)

                [{
                    "uuid": string,
                    "layers": string[],
                    "source_type": "dataset" | "resultset",
                }]

                OR

                [{
                    "url": string,
                    "source_type": "external",
                }]

                'filename' will be injected into the original objects as a pointer to the data in workspace.


            'dataset'
                A basic interface to describe a platform Dataset by UUID only. Suitable for non layered Datasets.
                    - Parameter value should be UUID


            'parameter'
                Parameter values will be automatically saved as files or downloaded from object store via url pointer.

                It has two behaviour flags: 
                    'save_as'       - Saved the parameter value as a file 
                                    - Expects 'pvalue' to be an object with a key that matches the pconfig flag 'key'
                                    - Expects 'pvalue' to contain a string that represents either json or text.
                                    - Must set 'vtype' (json, text)
                                    - Must set 'filename'
                                    - Must set 'key'
                                    - May set 'rekey'

                    'download_as'   - Download the parameter data from Object Store as a file
                                    - Expects 'pvalue' to be an object with a key that matches the pconfig flag 'key'
                                    - Expects 'pvalue' to contain a URL fragment for the target data
                                    - Must set 'filename'
                                    - Must set 'key'
                                    - May set 'rekey'

                The original parameter value will be replaced with the filename. 
                If 'rekey' is set this parameter key will be used instead.


            'job'
            'jobs'
                Used by Ensemble. Describes Jobs. Will lookup Result data based on predefined rules.


            'future_dataset_layer'
                Used by CC. Lookup specific Dataset layers based on input SDM Result parameters.

    """

    # Download datasets and initialise parameters
    newparams = {
        'function': function
    }
    info = {
        "datasets": [],
        "files": []
    }

    _params = copy.deepcopy(params)
    ptype = None
    target_pname = None
    nested = None

    # Data holder used for 'future_dataset_layer' param type.
    # which expects a ptype 'job' to be prepared first.
    resultset_job_metadata = None 

    # Input metadata
    for pname, pconfig_flag in ordered_list(pconfig['input']):
        if 'type' not in pconfig_flag:
            raise ParameterError(f"Property {pname} is missing 'type' ({str(pconfig_flag)})")

        ptype           = pconfig_flag.get('type')              # pconfig parameter type
        target_pname    = pconfig_flag.get('rename', pname)     # pconfig 'rename' flag or the original param name
        nested          = '.' in pname                          # pconfig 'nested' flag indicating param name is a parameter chain 

        log.debug(f"prepare_parameters: pname:{str(pname)}, ptype:{str(ptype)}, target_pname:{str(target_pname)}, nested:{str(nested)}")


        def prepare_type_dataset(pname: str, pconfig_flag: dict):
            """ 
                Prepare ptype 'dataset'
                :param pname: Parameter name
                :param pconfig_flag: Parameter config flags 
            """
            for (dsuuid, resolved_pname, resolved_parent, resolved_parent_path) in resolve_param_chain(_params, pname):
                log.info(f"dsuuid:{dsuuid}, resolved_pname:{resolved_pname}, resolved_parent:{resolved_parent}")
                # Dataset uuid, with an optional associated parameter
                # Generally, for species, absence and bias dataset.
                # Also support for file as dataset.
                link_param = pconfig_flag.get('link_param')
                link_param = _params.get(link_param) if link_param else []
                # if not nested assign result directly using target_pname
                # if nested modify the result parent in place
                dataset = scripts.core.utils.fetch_dataset(dsuuid, savedir, vars=link_param)[0] if dsuuid else dsuuid
                if not nested:
                    newparams[target_pname] = dataset
                elif resolved_parent and resolved_pname:
                    resolved_parent[target_pname if target_pname else resolved_pname] = dataset

                if resolved_parent_path:
                    info["datasets"].append(f"{resolved_parent_path}.{target_pname}")
                else:
                    info["datasets"].append(target_pname)


        def prepare_type_dataset_layer(pname: str, pconfig_flag: dict):
            """ 
                Prepare ptype 'dataset_layer'
                :param pname: Parameter name
                :param pconfig_flag: Parameter config flags 
            """

            # Parameter interface supports resolving multiple parameters via JSONPath
            resolved_parameters = resolve_param_jsonpath(_params, pname)
            if not resolved_parameters:
                return

            for ds in resolved_parameters:
                if ds is None:
                    continue
                if not isinstance(ds, dict):
                    log.warning(f"dataset_layer of unknown type ({type(ds)}/{str(ds)}). Expected dict! Not processing")
                    continue

                param_data = []

                # log.info(f"prepare_parameters: pname {pname}, ds {str(ds)}")

                ## Validate source_type. If not present assume a default of 'dataset'.

                source_type = ds.get('source_type', 'dataset')

                if source_type in ('dataset', 'resultset'):
                    if 'uuid' not in ds or ds.get('uuid') is None:
                        log.warning(f"dataset_layer ({pname}) missing uuid")
                        continue
                    if 'layers' not in ds or ds.get('layers') is None:
                        log.warning(f"dataset_layer ({pname}) missing layers. Will use fallback value.")

                elif source_type in ('external'):
                    if 'url' not in ds or ds.get('url') is None:
                        raise ParameterError(f"dataset_layer ({pname}) of source_type 'external' missing 'url'")

                # Fetch Resultset
                if source_type == 'resultset':
                    # Get titles from layers input field if any. Otherwise from the config.
                    resultset = scripts.core.utils.fetch_resultsets(
                        [ds['uuid']],
                        savedir,
                        titles=ds.get('layers') or pconfig_flag.get('resultset_files', {}).get('titles'),
                        searchopt=pconfig_flag.get('resultset_files', {}).get('search_op', 'ALL')
                    )
                    param_data = resultset['resultset']

                # Fetch Dataset
                elif source_type == 'dataset':
                    param_data = scripts.core.utils.fetch_dataset(ds.get('uuid'), savedir, vars=ds.get('layers'))

                # Fetch external URL
                elif source_type == 'external':
                    param_data = scripts.core.utils.fetch_url(ds.get('url'), savedir)

                else:
                    log.warning(f"prepare_type_dataset_layer: pname '{pname}' unknown source_type '{source_type}'. Skipping.")
                    continue

                if len(param_data) == 0:
                    raise ParameterError(f"prepare_type_dataset_layer: pname '{pname}' No data could be fetched for '{str(ds)}'")

                # Carry forward any additional parameters other than uuid, url, layers, source_type
                for key, lvalue in ds.items():
                    if key in ('uuid', 'url', 'layers', 'source_type'):
                        continue

                    # Copy param to each layer if multi layer input specified
                    if isinstance(ds.get('layers'), list) and len(ds.get('layers')) > 0:
                        for i in range(len(ds.get('layers'))):
                            log.debug(f"layer [{i}][{key}] -> {lvalue}")
                            param_data[i][key] = lvalue
                    else:
                        param_data[0][key] = lvalue

                # If using pconfig 'rename' remove orginal parameter
                if pconfig_flag.get('rename'):
                    _params.pop(pname, None)

                # Check if an item is to be returned as an item or list of item.
                # if not nested assign result directly using target_pname
                # if nested modify the result parent in place
                if not nested:
                    if pconfig_flag.get('plural', '') == 'single':
                        newparams[target_pname] = param_data[0]
                    else:
                        if target_pname not in newparams:
                            newparams[target_pname] = []
                        newparams[target_pname].extend(param_data)
                else:
                    ds.clear()
                    ds.update(param_data[0])

                if target_pname not in info["datasets"]:
                    info["datasets"].append(target_pname)


        def prepare_type_parameter(pname: str, pconfig_flag: dict):
            """ 
                Prepare ptype 'parameter'
                :param pname: Parameter name
                :param pconfig_flag: Parameter config flags 
            """
            pvalue = _params.pop(pname, None)
            if pvalue is None:
                pvalue = copy.deepcopy(pconfig_flag.get('default'))

            newparams[target_pname] = pvalue
            if pvalue:
                save_as = pconfig_flag.get('save_as')
                if save_as:
                    # Save as file
                    key = save_as.get('key')
                    vtype = save_as.get('vtype', 'json')  # json, text
                    filename = os.path.join(savedir, save_as.get('filename'))
                    if key:
                        # Save the specified element given by key
                        pvalue[save_as.get('rekey', key)] = scripts.core.utils.save_as_file(pvalue.pop(key), filename, vtype)
                        newparams[target_pname] = pvalue
                    else:
                        # No key means save parameter as file
                        newparams[target_pname] = scripts.core.utils.save_as_file(pvalue, filename, vtype)
                    info["files"].append((pname, target_pname))

                else:
                    # Download as a file
                    download_as = pconfig_flag.get('download_as')
                    if download_as:
                        key = download_as.get('key')
                        filename = os.path.join(savedir, download_as.get('filename'))

                        # Override for isolated pipeline testing where no JobReq exists.
                        # Just assume key is the actual geojson.
                        if os.getenv('DEPLOYMENT') == DEPLOYMENT_CI:
                            log.info(f"TEST MODE skipping download of input '{str(pname)}' of type 'parameter' with flag 'download_as'")
                            pvalue[download_as.get('rekey', key)] = scripts.core.utils.save_as_file(pvalue.pop(key), filename, 'text')

                        # Fetch the file using temp url auth
                        else:
                            if os.getenv('JOBREQ_TEMPURL_URL') is None:
                                raise ParameterError(f"Cannot prepare input '{str(pname)}' of type 'parameter' with flag 'download_as'. "
                                                "Env JOBREQ_TEMPURL_URL is empty.")

                            swifturl = pvalue.pop(key)
                            data_obj = {
                                'url': os.getenv('JOBREQ_TEMPURL_URL').replace('swifturl', swifturl),
                                'filename': filename
                            }
                            scripts.core.utils.fetch_data(data_obj)
                            pvalue[download_as.get('rekey', key)] = data_obj['filename']

                        newparams[target_pname] = pvalue
                        info["files"].append((pname, target_pname))


        def prepare_type_job(pname: str, pconfig_flag: dict) -> dict:
            """ 
                Prepare ptype 'job'
                :param pname: Parameter name
                :param pconfig_flag: Parameter config flags 
                :returns: Job entity (required for subsequent prep of 'future_dataset_layer' ptype)
            """
            results = None
            pvalue = None
            resultset_uuid = None                   # input resultset uuid
            rename = pconfig_flag.get('rename')     # pconfig 'rename' maps input parameter names to new names

            # Get the parent job's object model file as results
            for key, value in rename.items():
                if value['type'] != 'modelfile' or value.get('key') is None:
                    continue
                log.info(f"prepare_parameters: scripts.core.utils.fetch_resultsets pname:{pname}")
                pvalue = _params.pop(pname)
                # log.debug(f"prepare_parameters: {str(pvalue)} ({str(value['key'])})")
                if value['key'] not in pvalue:
                    raise ParameterError(f"prepare_parameters: '{str(value['key'])}' not in found in param '{pname}'")

                resultset_uuid = pvalue[value['key']]
                resultset_job_metadata = scripts.core.utils.fetch_job(resultset_uuid)
                print(str(resultset_job_metadata))
                results = scripts.core.utils.fetch_resultsets(
                    [resultset_uuid],
                    savedir,
                    titles=value.get('title'),
                    searchopt=value.get('search_op', 'ALL')
                )
                # Unzip file if it is zipped
                modelfile = results['resultset'][0]
                if modelfile.get("mimetype") == "application/zip":
                    with zipfile.ZipFile(modelfile.get('filename'), 'r') as zipf:
                        zipf.extractall(path=os.path.dirname(modelfile.get('filename')))
                newparams[key] = modelfile
                info["datasets"].append(key)

            # Process subsequent parameters as derived parameters
            for key, value in rename.items():
                vkey = value.get('key')
                if value['type'] == 'parameter':
                    if vkey in pvalue:
                        # derived parameters from its value
                        newparams[key] = pvalue[vkey]
                elif value['type'] == 'mdparameter':
                    # derived parameter from parent/SDM job's parameter
                    newparams[key] = resultset_job_metadata['parameters'].get(vkey)
                elif value['type'] == 'rsparameter':
                    # derived parameter from parent/SDM job's resultset object model
                    newparams[key] = results.get(vkey)
                elif value['type'] == 'projectionfile':
                    # derived parameter: projection result file as parameter
                    proj_results = scripts.core.utils.fetch_resultsets(
                        [resultset_uuid],
                        savedir,
                        titles=value.get('title'),
                        searchopt=value.get('search_op', 'ALL')
                    )
                    newparams[key] = proj_results['resultset']
                # Indicate to remove the derived parameters later
                if key in newparams and key not in info['datasets']:
                    info['datasets'].append(key)

            return resultset_job_metadata
    
        def prepare_type_future_dataset_layer(pname: str, pconfig_flag: dict, resultset_job_metadata: dict):
            """ 
                Prepare ptype 'future_dataset_layer'
                :param pname: Parameter name
                :param pconfig_flag: Parameter config flags 
                :param resultset_job_metadata: Job entity
            """
            if not os.getenv('DATASET_ITEM_API_URL'):
                raise DatasetError('Configuration error, Envvar DATASET_ITEM_API_URL is not set')

            # Make sure rsmetadata is available
            if resultset_job_metadata is None:
                raise ParameterError("Resultset Job metadata is not fetched")
            if resultset_job_metadata.get('parameters') is None:
                raise ParameterError("Resultset Job metadata does not contain 'parameters'")

            # pconfig 'rename' maps input parameter names to new names
            rename = pconfig_flag.get('rename')    

            # Prepare the dataset layers first
            dataset_layers = None    # dictionary of datasets and its layers used for projection
            for key, value in rename.items():
                if value['type'] not in ('dataset_layer', 'selected_layers'):
                    continue

                # Work up the dataset layers required
                if dataset_layers is None:
                    future_dsuuid = _params.pop(key)[0]
                    future_dataset = scripts.core.utils.request_obj(url=os.getenv('DATASET_ITEM_API_URL').replace('uuid', future_dsuuid))
                    future_md = future_dataset['bccvl:metadata']
                    dataset_layers = project_layers(future_dataset, resultset_job_metadata.get('parameters', {}).get('predictors'))

                if value['type'] == 'dataset_layer':
                    log.info(f"dataset_layers: {str(dataset_layers)}")
                    # Download the data layers
                    newparams[key] = []
                    for dsuuid, layers in dataset_layers.items():
                        if dsuuid:
                            newparams[key].extend(scripts.core.utils.fetch_dataset(dsuuid, savedir, vars=layers))
                else:
                    # selected layers from future dataset
                    newparams[key] = dataset_layers.get(future_dsuuid)
                # Remove the dataset and derived parameters
                info["datasets"].append(key)

            # Process other parameters
            for key, value in rename.items():
                vkey = value.get('key')
                if value['type'] == 'mdparameter':
                    newparams[key] = future_md.get(vkey)
                elif value['type'] == 'create_parameter':
                    nameparts = {name: future_md.get(name) for name in value['nameparts']}
                    newparams[key] = value['name'].format(**nameparts)

                # Remove the derived parameters
                info["datasets"].append(key)


        def prepare_type_jobs(pname: str, pconfig_flag: dict):
            """ 
                Prepare ptype 'future_dataset_layer'
                :param pname: Parameter name
                :param pconfig_flag: Parameter config flags 
            """
            # jobs' resultsets are mapped into different parameters
            resultset_uuids = _params.pop(pname)
            # Fetch the resultset 1st
            results = None
            # pconfig 'rename' maps input parameter names to new names
            rename = pconfig_flag.get('rename')    

            # Set resultset first
            for key, value in rename.items():
                if value['type'] != 'resultsets':
                    continue
                results = scripts.core.utils.fetch_resultsets(
                    resultset_uuids,
                    savedir,
                    titles=value.get('titles'),
                    searchopt=value.get('search_op', 'ALL')
                )
                newparams[key] = results['resultset']
                info["datasets"].append(key)

            # set other parameters from resultset
            for key, value in rename.items():
                if value['type'] == 'parameter':
                    if key == 'thresholds':
                        newparams[key] = results.get('thresholds')
                    elif key == 'sdm_projections':
                        newparams[key] = results.get('parent_resultset')


        if ptype == 'dataset':
            prepare_type_dataset(pname, pconfig_flag)
        elif ptype == 'dataset_layer':
            prepare_type_dataset_layer(pname, pconfig_flag)
        elif ptype == 'parameter':
            prepare_type_parameter(pname, pconfig_flag)
        elif ptype == 'job':
            resultset_job_metadata = prepare_type_job(pname, pconfig_flag)
        elif ptype == 'jobs':
            prepare_type_jobs(pname, pconfig_flag)
        elif ptype == 'future_dataset_layer':
            prepare_type_future_dataset_layer(pname, pconfig_flag, resultset_job_metadata)

    # print(newparams)

    # Remove user
    _params.pop('user', None)

    # Combine newly resolved parameters with existing
    _params.update(newparams)

    return _params, info


def resolve_param_chain(params: list, pname: str) -> list:
    """ 
    Resolve a chained 'pconfig' parameter name and return uuid and param object context.

    :param params: Parameters list
    :param pname: Parameter name or jsonpath 
    :returns: List of resolved paths containing tuple (uuid, resolved_pname, resolved_parent, resolved_parent_path)
    """
    log.debug(f"resolve_param_chain: params={str(params)}, pname={pname}")

    # simple prop names
    if pname in params:
        return [(params.pop(pname), pname, params, None)]

    # JSONPath match
    parts = pname.split('.')
    if len(parts) > 1:
        pname_leaf = parts.pop()
        pname_parent = '.'.join(parts)
        resolved = []
        matches = jsonpath_rw_ext.match("$."+pname_parent, params)
        if isinstance(matches, list) and len(matches) > 0:
            for m in matches:
                if pname_leaf in m:
                    resolved.append((m[pname_leaf], pname_leaf, m, pname_parent))
                else:
                    log.warning(f"resolve_param_chain: resolved pname_parent={pname_parent} but pname_leaf={pname_leaf} not present")

            return resolved

    log.warning(f"resolve_param_chain: could not resolve pname={pname}")
    return []


def resolve_param_jsonpath(params: dict, pname: str) -> list:
    """ 
    Return list of all parameters that match pname, with pname being either a literal name
    of a parameter or a JSONPath that can match several parameters.

    If a single parameter matches pname and it's a list itself then it will be the return val.

    :param params: Parameters dictionary
    :param pname: Parameter name or JSONPath 
    :returns: list of dataset_layer param objects that match string or JSONPath pname
    """
    log.debug(f"resolve_param_jsonpath: params:{str(params)}, pname:{pname}")
    if pname in params:
        p = params.get(pname, [])
        if isinstance(p, list):
            log.info(f"resolve_param_jsonpath: ({str(pname)}) single param matched is a List, returning")
            return p
        return [p]

    # JSONPath match
    p: list = jsonpath_rw_ext.match("$."+pname, params)
    if p:
        log.debug(f"resolve_param_jsonpath: JSONPath matched ({p}) from '{str(pname)}'")
        # If there is a single match that is a list then return it
        if len(p) == 1 and isinstance(p[0], list):
            log.info(f"resolve_param_jsonpath: JSONPath ({str(pname)}) single param matched is a List, returning")
            return p[0]
        return p

    log.info(f"resolve_param_jsonpath: Could not resolve param ({pname})")
    return []


def project_layers(future_cjson, sdm_predictors):
    # TODO: This needs better naming and documentation, 
    # not currently clear what it does and why.
    #
    # Return the projection layers to be used by matching
    # the predictor layers used in SDM

    # Get future dataset layers
    # log.info(f"project_layers: future_cjson={str(future_cjson)}")
    log.info(f"project_layers: sdm_predictors={str(sdm_predictors)}")

    dsuuid = future_cjson['bccvl:metadata']['uuid']
    futurelayers = set(future_cjson['parameters'].keys())

    projlayers = {}
    for predictor in sdm_predictors:
        dslayerset = set(predictor.get('layers', []))
        # add matching layers
        projlayers.setdefault(dsuuid, set()).update(
            dslayerset.intersection(futurelayers))
        # remove matching layers
        projlayers[predictor['uuid']] = dslayerset - futurelayers
        if not projlayers[predictor['uuid']]:
            # remove if all layers replaced
            del projlayers[predictor['uuid']]
    for ds in projlayers:
        projlayers[ds] = list(projlayers[ds])
    return projlayers
    
