import re
import json
import logging

log = logging.getLogger(__name__)

def get_sort_criteria(result_name: str) -> str:
    """
        :param result_name: Result name
        :returns: Subset of result name used for sorting 
    """
    sort_digit = re.search(r"(\d+)", result_name)
    sort_non_digit = re.sub(r"\d+", '', result_name)
    if sort_digit:
        return sort_digit.group(), sort_non_digit
    return sort_non_digit

def fraction_sort_resultmd(resultmd: dict) -> dict:
    """ 
        Sort files within the same order number by filename using a fraction
        e.g. from - 
        "result_t07_sd.tif": {"order": 1} & "result_t03_mean.tif": {"order": 1}
        to -
        "result_t07_sd.tif": {"order": 1.2} & "result_t03_mean.tif": {"order": 1.1}

        :param resultmd: Result metadata
        :returns: Sorted Result metadata
    """

    # Step 1: filter results with order parameter,
    # save it to result_order_map - {key: order value, value: [result names]}
    # e.g. {1: ["result_t07_sd.tif", "result_t03_mean.tif"]}
    result_order_map = {}
    for result_name, result_value in resultmd.items():
        if result_value.get("bccvl:metadata", {}).get("order") is not None:
            result_order_map[result_value["bccvl:metadata"]["order"]] = result_order_map.get(result_value["bccvl:metadata"]["order"], []) + [result_name]

    log.debug(f"fraction_sort_resultmd: sorting '{str(result_order_map.values())}'")

    # Step 2: create sort_criteria mapping dict sort_criteria_map - {key: result_name, value: sort_criteria}
    # for and only for results that same order has more than one results
    # e.g. {"result_t07_sd.tif": (7, "result_t_sd.tif"), "result_t03_mean.tif": (3, "result_t_mean.tif")}
    for order, result_names in result_order_map.items():
        sort_criteria_map = {}
        if len(result_names) > 1:
            sort_criteria_map = {result_name: get_sort_criteria(result_name) for result_name in result_names}

    # Step 3: sort sort_criteria_map and return sorted_results - [results_name]
    # if digits found in result names, sort digits in sorted names
    # e.g. return sorted_results = ["result_t04_mean.tif", "result_t12_mean.tif", "result_t07_sd.tif", "result_t10_sd.tif"]
            # sort result names with no digits:
            if sort_criteria_map[result_names[0]] == result_names[0]:
                sorted_results = sorted(sort_criteria_map)
            else:
                sort_by_digit = sorted(sort_criteria_map.items(), key=lambda item: item[1][0])
                sort_by_non_digit = sorted(sort_by_digit, key=lambda item: item[1][1])
                sorted_results = [result_name_map[0] for result_name_map in sort_by_non_digit]

    # Step 4: refactor order
            floating_bits = len(str(len(sorted_results) - 1))
            for index, result_name in enumerate(sorted_results):
                str_index = str(index).rjust(floating_bits, "0")
                new_order = float('.'.join([str(order), str_index]))
                resultmd[result_name]["bccvl:metadata"]["order"] = new_order
                # helper for test - can use below print to generate test data
                # print('"{}": {},'.format(result_name, new_order))

    # another helper for test - can use below print to generate test data
    # test_result_data = {result_name: result_value["bccvl:metadata"]["order"] for result_name, result_value in sorted(resultmd.items(), key=lambda item: item[1]["bccvl:metadata"]["order"])}
    # # print("test_result_data", test_result_data)
    # print("test_result_data", json.dumps(test_result_data, indent=4))
    
    return resultmd
