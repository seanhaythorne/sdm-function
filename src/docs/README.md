## Main Page

### Introduction

Welcome to the documentation for the SDM Functions.


### Function Metadata

Function metadata describe a function, its input parameters, and output files.
Each function repository shall include function metadata JSON-formatted file as part of its documentation.
This metadata will be imported during the function registration process into the “Function-Catalogue”.

The function metadata shall have the following mandatory attributes (see examples below):
- **name**: The function name.
- **description**: Description of function.
- **input**: [JSON-schema](https://json-schema.org/) -formatted describing the input parameters.
- **output**: JSON-formatted dictionary of a regular expression matching output filename, 
  and its mandatory associated attributes such as `title` and `mimetype`.
  
Follow the links below to go directly to metadata and examples for specific functions:

| Function | Input Example |
| --- | --- |
| [Artificial Neural Network (ANN)](ann.json) | [ANN](examples/ann-params-example.json)|
| [BioClim](bioclim.json) | [BIOCLIM](examples/bioclim-params-example.json)|
| [Boosted Regression Tree (BRT)](brt.json) | [BRT](examples/brt-params-example.json)|
| [Circles Range (CIRCLES)](circles.json) | [CIRCLES](examples/circles-params-example.json)|
| [Convex Hull](convhull.json) | [CONVHULL](examples/convhull-params-example.json)|
| [Classification Tree (CTA)](cta.json) | [CTA](examples/cta-params-example.json)|
| [Flexible Discriminant Analysis (FDA)](fda.json) | [FDA](examples/fda-params-example.json)|
| [Generalized Additive Model (GAM)](gam.json) | [GAM](examples/gam-params-example.json)|
| [Generalized Boosting Model (GBM)](gbm.json) | [GBM](examples/gbm-params-example.json)|
| [Generalized Linear Model (GLM)](glm.json) | [GLM](examples/glm-params-example.json)|
| [Geographic Distance (GEODIST)](geoDist.json) | [GEODIST](examples/geodist-params-example.json)|
| [Inverse Distance Weighted Model (GEOIDW)](geoIDW.json) | [GEOIDW](examples/geoidw-params-example.json)|
| [MaxEnt](maxent.json) | [MAXENT](examples/maxent-params-example.json)|
| [Multivariate Adaptive Regression Splines (MARS)](mars.json) | [MARS](examples/mars-params-example.json)|
| [Random Forest (RF)](rf.json) | [RF](examples/rf-params-example.json)|
| [Surface Range Envelope (SRE)](sre.json) | [SRE](examples/sre-params-example.json)|
| [VoronoiHull](voronoiHull.json) | [VORONOIHULL](examples/voronoihull-params-example.json)|
| [Climate Change (CC)](cc.json) | [CC](examples/cc-params-example.json) |
| [Ensemble Analysis (ENSEMBLE)](ensemble.json) | [ENSEMBLE](examples/ensemble-params-example.json) |


####Species Traits Functions


| Function | Input Example |
| --- | --- |
|[Species Traits - CTA](speciestrait_cta.json)|[Traits - CTA](examples/traits-cta-params-example.json)|
|[Species Traits - GAM](speciestrait_gam.json)|[Traits - GAM](examples/traits-gam-params-example.json)|
|[Species Traits - GLM](speciestrait_glm.json)|[Traits - GLM](examples/traits-glm-params-example.json)|
|[Species Traits - GLMM](speciestrait_glmm.json)|[Traits - GLMM](examples/traits-glmm-params-example.json)|
|[TraitDiff - GLM](traitdiff_glm.json)|[TraitDiff - GLM](examples/traitdiff-glm-params-example.json)|
