{
    "input": {
        "species": {
            "rename": "species_occurrence_dataset",
            "type": "dataset",
            "link_param": "species_filter"
        },
        "absence": {
            "rename": "species_absence_dataset",
            "type": "dataset",
            "link_param": "species_filter"
        },
        "bias": {
            "rename": "bias_dataset",
            "type": "dataset"
        },
        "predictors": {
            "rename": "environmental_datasets",
            "type": "dataset_layer"
        },
        "modeling_id": {
            "type": "parameter",
            "default": "bccvl"
        },
        "selected_models": {
            "type": "parameter",
            "default": "all"
        },
        "generate_convexhull": {
            "type": "parameter",
            "default": false
        },
        "unconstraint_map": {
            "type": "parameter",
            "default": true
        },
        "modelling_region": {
            "type": "parameter",
            "download_as": {
                "key": "geojson",
                "rekey": "filename",
                "filename": "modelling_region.json"
            }
        }
    },
    "script": [
        "{function}/{function}.R"
    ],
    "metadata": {
        "Species occurrence": "species",
        "True absence": "absence",
        "Bias data": "bias",
        "Predictors": "predictors",
        "Citation": "default"
    },
    "output": {
        "files": {
            "Rplots.pdf": {
                "skip": true
            },
            "eval/pROC.Full.png": {
                "skip": true
            },
            "eval/Proj_current_*.tif": {
                "title": "Projection to current climate",
                "genre": "DataGenreCP",
                "mimetype": "image/geotiff",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "order": 1
            },
            "eval/Proj_current_*_unconstrained.tif": {
                "title": "Projection to current climate - unconstrained",
                "genre": "DataGenreCP_ENVLOP",
                "mimetype": "image/geotiff",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "order": 2
            },
            "eval/Proj_current_*.png": {
                "title": "Projection plot",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 3
            },
            "eval/Proj_current_*_unconstrained.png": {
                "title": "Projection plot - unconstrained",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 4
            },
            "eval/Proj_current_ClampingMask.tif": {
                "title": "Clamping Mask",
                "genre": "DataGenreClampingMask",
                "mimetype": "image/geotiff",
                "layer": "clamping_mask",
                "data_type": "Discrete",
                "order": 5
            },
            "eval_tables/Pseudo_absences_*.csv": {
                "title": "Absence records (map)",
                "genre": "DataGenreSpeciesAbsence",
                "mimetype": "text/csv",
                "order": 7
            },
            "eval_tables/Absence_*.csv": {
                "title": "Absence records (map)",
                "genre": "DataGenreSpeciesAbsence",
                "mimetype": "text/csv",
                "order": 7
            },
            "eval_tables/Occurrence_environmental_*.csv": {
                "title": "Occurrence points with environmental data",
                "genre": "DataGenreSpeciesOccurEnv",
                "mimetype": "text/csv",
                "order": 6
            },
            "eval_tables/Absence_environmental_*.csv": {
                "title": "Absence points with environmental data",
                "genre": "DataGenreSpeciesAbsenceEnv",
                "mimetype": "text/csv",
                "order": 8
            },
            "eval/Mean_response_curves*.png": {
                "title": "Response curves",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 9
            },
            "eval/*_mean_response_curves*.png": {
                "hidden": true,
                "title": "Response curves",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 9
            },
            "eval_tables/Evaluation-statistics_*.csv": {
                "title": "Model Evaluation",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 10
            },
            "eval_tables/Combined.Full.modelEvaluation.csv": {
                "title": "Model Evaluation",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 11
            },
            "eval/Full-presence-absence-plot_*.png": {
                "title": "Presence/absence density plot",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 12
            },
            "eval/Full-presence-absence-hist_*.png": {
                "title": "Presence/absence histogram",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 13
            },
            "eval/Full-occurence_absence_pdf.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 14
            },
            "eval/Full-TPR-TNR_*.png": {
                "title": "Sensitivity/Specificity plot",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 15
            },
            "eval/Full-error-rates_*.png": {
                "title": "Error rates plot",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 16
            },
            "eval/Full-ROC_*.png": {
                "title": "ROC plot",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 17
            },
            "eval/Full-loss-functions_*.png": {
                "title": "Loss functions plot",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 18
            },
            "eval_tables/Loss-function-intervals-table_*.csv": {
                "title": "Loss functions table",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 19
            },
            "eval/Full-loss-intervals.png": {
                "title": "Loss functions intervals",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 20
            },
            "eval_tables/Evaluation-data_*.csv": {
                "title": "Model evaluation data",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 21
            },
            "*.R": {
                "title": "Job Script",
                "genre": "JobScript",
                "mimetype": "text/x-r",
                "order": 30
            },
            "*.Rout": {
                "title": "Log file",
                "genre": "DataGenreLog",
                "mimetype": "text/x-r-transcript",
                "order": 32
            },
            "modelling_region.json": {
                "title": "modelling region",
                "hidden": true,
                "genre": "DataGenreSDMModellingRegion",
                "mimetype": "text/x-r-transcript",
                "order": 33
            },
            "metadata.json": {
                "title": "Metadata",
                "genre": "DataGenreMetadata",
                "mimetype": "application/json",
                "order": 34
            },
            "eval/Mean_sd_evaluation_*.png": {
                "title": "Mean and SD plot of model evaluation",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 35
            },
            "eval/Boxplot_evaluation_*.png": {
                "title": "Box-plots of model evaluation scores",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 36
            },
            "eval_tables/*.csv": {
                "title": "Model Evaluation",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 40
            },
            "eval/*.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 50
            },
            "params.json": {
                "title": "Input parameters",
                "genre": "InputParams",
                "mimetype": "text/x-r-transcript",
                "order": 100
            }
        },
        "archives": {
            "model.object.rds.zip": {
                "files": ["*_model_object.rds",
                          "*/*.bccvl.models.out",
                          "*/.BIOMOD_DATA/bccvl/*",
                          "*/models/bccvl/*",
                          "*/proj_current/*.current.projection.out"
                          ],
                "title": "R SDM Model object",
                "genre": "DataGenreSDMModel",
                "mimetype": "application/zip",
                "order": 31
            }
        }
    }
}
