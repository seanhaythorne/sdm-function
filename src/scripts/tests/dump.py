import json

def dump(out):
    """ TDD helper. Nicer print."""
    try:
        out = json.dumps(out, indent=4, sort_keys=True)
    except Exception as e:
        pass

    CYAN = '\033[96m'
    ENDC = '\033[0m'
    print(CYAN + str(out).replace('\\n', '\n').replace('\\t', '\t') + ENDC)