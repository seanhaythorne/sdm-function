#!/usr/bin/env bash
# LOCAL DEV

# Requires: ./bin/envfile.txt
# Example: ./bin/run_pipeline.sh ./helloworld.nf ./src/docs/examples/helloworld.example.json

set +x

[[ -z $1 || -z $2 ]] && printf "Usage: run_pipeline.sh PIPELINE_PATH PARAMS_PATH [JOB_ID] [PROFILE]\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TIMESTAMP=$(date +%s)
PIPELINE=$1
PARAMS=$2
JOB_ID=${3:-"1b5f64f0-960f-11ed-8454-0a580a64030b"}  # default to a dummy JobId (from the Test environment)
# JOB_ID=${3:-"709e17c0-aa71-11ec-916d-0a580a641575"}  # default to a dummy JobId (from the Dev environment)

PROFILE=${4:-"local"}

# Cleanup previous runs
rm -rf ${SCRIPT_DIR}/work/* rm -rf ${SCRIPT_DIR}/../work/*

# Run pipeline
nextflow run $PIPELINE \
    -w ./work \
    -profile $PROFILE \
    --jobUuid $JOB_ID \
    --inputs $PARAMS \
    --envfile "--env-file ${SCRIPT_DIR}/envfile.txt" \
    --volOption "--volume ${SCRIPT_DIR}/work/:/sdmwork" \
    -name "job_${JOB_ID}_${TIMESTAMP}" \
    -with-weblog http://localhost:8000/api/tasks/ \
    -main-script $PIPELINE

nextflow log "job_${JOB_ID}_${TIMESTAMP}" -f stdout,stderr