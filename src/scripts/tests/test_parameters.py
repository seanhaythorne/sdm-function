import os
import io
import json
import shutil
import zipfile
from tempfile import NamedTemporaryFile, TemporaryDirectory, mkdtemp
from unittest.mock import patch
from django.test import TestCase, tag
from django.conf import settings
import scripts.core.utils
import scripts.core.parameters
import mocks
from dump import dump

class TestParameters(TestCase):

    @tag('param', 'resolve')
    def test_resolve_param_jsonpath(self):
        params = {
            'dataset_layer': { 'uuid': 'UUID', 'layers': [] },
            'nested': { 
                'dataset_layer': { 'uuid': 'UUID', 'layers': [] },
                'dataset_layers': [
                    { 'uuid': 'UUID1', 'layers': ['l1'] },
                    { 'uuid': 'UUID2', 'layers': ['l2'] }
                ],
            }
        }

        p = scripts.core.parameters.resolve_param_jsonpath(params, 'dataset_layer')
        self.assertEqual(p, [params['dataset_layer']])

        p = scripts.core.parameters.resolve_param_jsonpath(params, 'nested.dataset_layer')
        self.assertEqual(p, [params['nested']['dataset_layer']])       

        p = scripts.core.parameters.resolve_param_jsonpath(params, 'nested.dataset_layers')
        self.assertEqual(p, params['nested']['dataset_layers'])     


    @tag('param', 'resolve')
    def test_resolve_param_chain(self):
        params = {
            'dataset': { 'uuid': 'UUID1' },
            'nested': { 
                'dataset': { 'uuid': 'UUID2' },
            }
        }
        p = scripts.core.parameters.resolve_param_chain(params, 'dataset')
        # dump(p)
        self.assertEqual(p, [({ 'uuid': 'UUID1' }, 'dataset', params, None)])

        p = scripts.core.parameters.resolve_param_chain(params, 'nested.dataset')
        # dump(p)
        self.assertEqual(p, [({ 'uuid': 'UUID2' }, 'dataset', params['nested'], 'nested')])      


    @tag('param')
    def test_project_layers(self):
        future_dscov = json.load(open('scripts/tests/data/curated_tasclim_dataset.json'))
        sdm_predictors = [
            {'uuid': 'uuid1', 'layers': ['bioclim_02', 'bioclim_04', 'rainfall']},
            {'uuid': 'uuid2', 'layers': ['bioclim_10', 'height', 'bioclim_05']}
        ]
        proj_layers = scripts.core.parameters.project_layers(future_dscov, sdm_predictors)
        self.assertEqual(set(proj_layers[future_dscov['bccvl:metadata']['uuid']]), 
                         set(['bioclim_02', 'bioclim_04', 'bioclim_05', 'bioclim_10']))
        self.assertEqual(proj_layers['uuid1'], ['rainfall'])
        self.assertEqual(proj_layers['uuid2'], ['height'])

    @tag('param', 'dataset')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_prepare_parameters_with_dataset(self, mock1, mock2, mock3):
        # Test dataset with species dataset
        pname = 'species'
        algorithm = 'ann'
        uuid = mocks.SPECIES_OCCUR_CSV_DATASET_UUID
        params = {pname: uuid}
        savedir = mkdtemp()
        config = { pname: json.load(open('scripts/tests/data/sdm_ann.pconfig.json'))['input'].get(pname)}
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)

        dscov = json.load(open('scripts/tests/data/species_occurrence_csv_dataset.json'))
        filename = dscov['rangeAlternates']['dmgr:csv']['url']
        tempurl = dscov['rangeAlternates']['dmgr:csv']['tempurl']
        speciesName = dscov['bccvl:metadata']['scientificName'][0]
        expected_params = {
            "function": algorithm,
            config[pname].get('rename', pname): {
                "species": speciesName.replace(' ', '.'),
                "filename": os.path.join(savedir, uuid, os.path.basename(filename)),
                "url": tempurl,
                "scientificName": speciesName
            }
        }
        self.assertEqual(newparams, expected_params)
        expected_info = {
            'datasets': [config[pname].get('rename', pname)],
            'files': []
        }
        self.assertEqual(info, expected_info)
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'dataset_layer', 'x')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_prepare_parameters_with_dataset_layer_dataset_list(self, mock1, mock2, mock3):
        # Test dataset_layer with environmental dataset
        dscov = json.load(open('scripts/tests/data/env_tif_dataset.json'))

        pname = 'predictors'
        algorithm = 'ann'
        uuid = mocks.ENV_TIF_DATASET_UUID
        params = { 
            pname: [
                {
                    'uuid': uuid,
                    'layers': list(dscov['parameters'].keys())
                }
            ]
        }
        savedir = mkdtemp()
        varname = 'distribute_features_template'
        config = { pname: json.load(open('scripts/tests/data/sdm_ann.pconfig.json'))['input'].get(pname)}
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)

        filename = dscov['rangeAlternates']['dmgr:tiff'][varname]['url']
        tempurl = dscov['rangeAlternates']['dmgr:tiff'][varname]['tempurl']
        expected_params = {
            "function": algorithm,
            config[pname].get('rename', pname): [
                {
                    "layer": varname,
                    "filename": os.path.join(savedir, uuid, os.path.basename(filename)),
                    "url": tempurl,
                    "type": 'continuous'
                }
            ]
        }
        # dump(newparams)
        self.assertEqual(newparams, expected_params)
        self.assertEqual(info, 
            {
                'datasets': [config[pname].get('rename', pname)],
                'files': []
            }
        )
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'dataset_layer')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_prepare_parameters_with_dataset_layer_two_params(self, mock1, mock2, mock3):
        # Test dataset_layer with environmental dataset
        dscov = json.load(open('scripts/tests/data/env_tif_dataset.json'))
        algorithm = 'bsrmap'
        pname = 'spatial'
        uuid = mocks.ENV_TIF_DATASET_UUID
        params = {
          "function": algorithm,
          "template": {
            "source_type": "dataset",
            "uuid": uuid,
            "layers": list(dscov['parameters'].keys())
          },
          "spatial": {
            "source_type": "dataset",
            "uuid": uuid,
            "layers": list(dscov['parameters'].keys())
          },
          "normalize": False,
          "binarize": False
        }

        savedir = mkdtemp()
        varname = 'distribute_features_template'
        pconfig = json.load(open('scripts/tests/data/bsrmap_conform_layer.pconfig.json'))
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, pconfig, savedir)

        filename = dscov['rangeAlternates']['dmgr:tiff'][varname]['url']
        tempurl = dscov['rangeAlternates']['dmgr:tiff'][varname]['tempurl']
        expected_params = {
            "function": algorithm,
            "template": {
                "layer": varname,
                "filename": os.path.join(savedir, uuid, os.path.basename(filename)),
                "url": tempurl,
                "type": 'continuous'
            },
            pconfig['input'][pname].get('rename', pname): {
                "layer": varname,
                "filename": os.path.join(savedir, uuid, os.path.basename(filename)),
                "url": tempurl,
                "type": 'continuous'
            },
            "normalize": False,
            "binarize": False
        }

        #dump(expected_params)
        #dump(newparams)
        #dump(info)

        self.assertEqual(newparams, expected_params)
        self.assertEqual(info, 
            {
                'datasets': [pconfig['input'][pname].get('rename', pname), "template"],
                'files': []
            }
        )
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'dataset_layer')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_prepare_parameters_with_nested_dataset_layer(self, mock1, mock2, mock3):
        # Test dataset_layer with nested objects containing datasets and resultsets
        params = json.load(open('scripts/tests/data/bsspread_nested_param.params.json'))

        pname = 'region.region_rast'
        algorithm = 'bsspread'
        
        savedir = mkdtemp()

        pconfig = json.load(open('scripts/tests/data/bsspread_pconfig.json'))['input']
        new_params, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': pconfig}, savedir)

        # rast
        rast_uuid = mocks.BSSPREAD_GENERIC_TIF_DATASET_UUID
        rast_dscov = json.load(open('scripts/tests/data/bsspread_generic_tif_dataset.json'))
        rast_filename = rast_dscov['rangeAlternates']['dmgr:tiff']['template_au']['url']
        rast_tempurl = rast_dscov['rangeAlternates']['dmgr:tiff']['template_au']['tempurl']

        # csv
        csv_uuid = mocks.BSSPREAD_GENERIC_CSV_DATASET_UUID
        csv_dscov = json.load(open('scripts/tests/data/bsspread_generic_csv_dataset.json'))
        csv_filename = csv_dscov['rangeAlternates']['dmgr:csv']['url']
        csv_tempurl = csv_dscov['rangeAlternates']['dmgr:csv']['tempurl']

        expected_params = {
            "function": algorithm,
            "region": {
                "region_rast": {
                    "layer": "template_au",
                    "filename": os.path.join(savedir, rast_uuid, os.path.basename(rast_filename)).strip(),
                    "url": rast_tempurl,
                    "type": "continuous",
                    "name": "template_au", 
                    "label": "Australia 1km"
                },
                "region_point": {
                    "uuid": "b5c4f5c8-5baf-11ec-a45a-0242ac130005",
                    "source_type": "dataset",
                    "data": {
                        "filename": os.path.join(savedir, csv_uuid, os.path.basename(csv_filename)).strip(),
                        "species": "Macadamia.integrifolia",
                        "scientificName": "Macadamia integrifolia",
                        "url": csv_tempurl
                    }
                }
            },
            "population_model": {
                "type": "presence_only",
                "capacity_rast": {
                  "layers": [
                  ]
                },
                "establish_pr_rast": {
                  "layers": [
                  ]
                }
            },
            "dispersal_models": [
                {
                    "type": "diffusion",
                    "attractors": [],
                    "permeability_rast": {
                        "layer": "template_au",
                        "filename": os.path.join(savedir, rast_uuid, os.path.basename(rast_filename)).strip(),
                        "url": rast_tempurl,
                        "type": "continuous",
                        "name": "template_au", 
                        "label": "Australia 1km"
                    }
                },
                {
                    "type": "gravity",
                    "attractors": [],
                    "permeability_rast": {
                        "layer": "template_au",
                        "filename": os.path.join(savedir, rast_uuid, os.path.basename(rast_filename)).strip(),
                        "url": rast_tempurl,
                        "type": "continuous",
                        "name": "template_au", 
                        "label": "Australia 1km"
                    }
                }
            ]
        }

        #dump(expected_params)
        #dump(newparams)
        #dump(info)

        self.assertEqual(
            new_params.get('region', {}).get('region_rast', {}).get('file_name'), 
            expected_params.get('region', {}).get('region_rast', {}).get('file_name')
            )
        self.assertEqual(
            new_params.get('region', {}).get('region_point', {}).get('file_name'), 
            expected_params.get('region', {}).get('region_point', {}).get('file_name')
            )

        self.assertEqual(new_params, expected_params)
        self.assertEqual(info, 
            {
                "datasets": ["region.region_rast", "region.region_point.data", "dispersal_models[*].permeability_rast"],
                "files": []
            }
        )
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'dataset_layer', 'xx')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_prepare_parameters_with_null_dataset_layer(self, mock1, mock2, mock3):
        # Test dataset_layer with nested objects containing datasets and resultsets
        params = json.load(open('scripts/tests/data/bsrmap_arrival_likelihood.params.json'))

        pname = 'template'
        algorithm = 'arrival_likelihood'
        uuid = mocks.CURATED_TASCLIM_DATASET_UUID

        savedir = mkdtemp()
        pconfig = json.load(open('scripts/tests/data/bsrmap_arrival_likelihood.pconfig.json'))
        new_params, info = scripts.core.parameters.prepare_parameters(algorithm, params, pconfig, savedir)

        # Check to ensure that template is null
        dscov = json.load(open('scripts/tests/data/curated_tasclim_dataset.json'))
        filename = dscov['rangeAlternates']['dmgr:tiff']['bioclim_02']['url']
        tempurl = dscov['rangeAlternates']['dmgr:tiff']['bioclim_02']['tempurl']
        expected_params = {
            "function": algorithm,
            "pathway_likelihood_layers": [
                {
                    "layer": "bioclim_02",
                    "filename": os.path.join(savedir, uuid, os.path.basename(filename)).strip(),
                    "url": tempurl,
                    "type": "continuous"
                }
            ],
            "template": None,
            "use_fun": "union",
            "na.rm": False
        }
        # dump(new_params)
        # dump(info)

        self.assertEqual(new_params, expected_params)
        self.assertEqual(info,
            {
                "datasets": ["pathway_likelihood_layers"],
                "files": []
            }
        )
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'dataset_layer')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_prepare_parameters_with_dataset_layer_resultset(self, mock1, mock2, mock3):
        # Test dataset_layer with resultset
        pname = 'pathway_likelihood_layers'
        algorithm = 'risk_mapping_algo1'
        uuid = mocks.SDM_RESULTSET_UUID
        # the result title to use 
        varname = 'Projection to current climate'
        for title, lookup in [
            (varname, 'genre:InvalidGenre'), # Specify the result title
            (None, 'genre:DataGenreCP') # Using the default genre in config file
        ]: 
            params = { 
                pname: {
                    'uuid': uuid,
                    'layers': [title] if title else [],
                    'source_type': 'resultset'
                }
            }
            config = {
                pname: {
                    "type": "dataset_layer",
                    "resultset_files": {
                        "titles": [lookup],
                        "search_op": "ANY"
                    },
                    'plural': 'single'
                }
            }
            savedir = mkdtemp()
            newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)
            results = json.load(open('scripts/tests/data/resultset.json')).get('results')
            result = mocks.get_result(results, varname)
            filename = result['rangeAlternates']['dmgr:tiff'][varname]['url']
            tempurl = result['rangeAlternates']['dmgr:tiff'][varname]['tempurl']
            md = result['bccvl:metadata']

            # Note: for resultset, store file per result data uuid
            expected_params = {
                "function": algorithm,
                config[pname].get('rename', pname): {
                    "layer": varname,
                    "filename": os.path.join(savedir, md['uuid'], os.path.basename(filename)),
                    "url": tempurl,
                    "mimetype": md["mimetype"]
                    #"type": 'continuous'
                }
            }
            self.assertEqual(newparams, expected_params)
            self.assertEqual(info, 
                {
                    'datasets': [config[pname].get('rename', pname)],
                    'files': []
                }
            )
            # Clean up
            shutil.rmtree(savedir)

    @tag('param', 'dataset_layer')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_prepare_parameters_with_dataset_layer_external_url(self, mock1, mock2, mock3):
        savedir = mkdtemp()
        function = 'helloworld'
        pname = 'spatial'
        url = 'https://gitlab.com/ecocommons-australia/ecocommons-platform/sdm-function/-/blob/master/src/scripts/tests/data/spatial.tif'
        params = {
            "function": function,
            "spatial": {
                "source_type": "external",
                "url": url
            }
        }
        pconfig = json.load(open('scripts/tests/data/helloworld_pconfig.json'))
        newparams, info = scripts.core.parameters.prepare_parameters(function, params, pconfig, savedir)

        expected_params = {
            "function": function,
            "spatial": {
                "filename": os.path.join(savedir, scripts.core.utils.str_to_md5(url), 'spatial.tif'),
                "url": url
            }
        }

        #dump(expected_params)
        #dump(newparams)
        #dump(info)

        self.assertEqual(newparams, expected_params)
        self.assertEqual(info, 
            {
                'datasets': ["spatial"],
                'files': []
            }
        )
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'parameter')
    def test_prepare_parameters_with_parameter(self):
        # Test parameter
        algorithm = 'risk_mapping_algo1'
        config = {
            "modeling_id": {
                "type": "parameter",
                "default": "bccvl"
            },
            "selected_models": {
                "type": "parameter",
                "default": "all"
            }
        }
        savedir = 'not-required'
        params = {"modeling_id": "ec"}
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)
        expected_params = params
        expected_params.update({'function': algorithm, 'selected_models': 'all'})
        self.assertEqual(newparams, expected_params)
        self.assertEqual(info, {'datasets': [], 'files': []})

    @tag('param', 'parameter')
    def test_prepare_parameters_with_parameter_saveas_file(self):
        # Test parameter
        algorithm = 'risk_mapping_algo1'
        config = {
            "modeling_id": {
                "type": "parameter",
                "default": 'bccvl',
                "save_as": {
                    'filename': 'modeling_id.txt',
                    'vtype': 'txt'
                }
            },
            "selected_models": {
                "type": "parameter",
                "default": {"sdm_model": {"ann": "some default models"}},
                "save_as": {
                    'key': 'sdm_model',
                    'filename': 'selected_models.json',
                    'vtype': 'json'
                },
                "rename": "models"
            }
        }
        savedir = mkdtemp()
        params = {"modeling_id": "ec modeling id"}
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)     
        expected_params = {
            'function': algorithm,
            'modeling_id': os.path.join(savedir, 'modeling_id.txt'),
            'models': {'sdm_model': os.path.join(savedir, 'selected_models.json')}
        }
        self.assertEqual(newparams, expected_params)
        self.assertEqual(
            info, 
            {
                'datasets': [], 
                'files': [('modeling_id', 'modeling_id'), ('selected_models', 'models')]
            }
        )
        self.assertEqual(open(os.path.join(savedir, 'modeling_id.txt')).read(), params['modeling_id'])
        self.assertEqual(
            json.load(open(os.path.join(savedir, 'selected_models.json'))), 
            config['selected_models']['default']['sdm_model']
        )
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'parameter')
    @patch('scripts.core.utils.fetch_data')
    @patch('os.getenv', return_value='http://job-manager-api:9000/api/tempur?url=swifturl')
    def test_prepare_parameters_with_parameter_downloadas_file(self, mock1, mock2):
        # Test parameter
        algorithm = 'risk_mapping_algo1'
        config = {
            "modelling_region": {
                "type": "parameter",
                "download_as": {
                    "key": "geojson",
                    "rekey": "filename",
                    "filename": "modelling_region.json"
                }
            }
        }
        savedir = mkdtemp()
        params =  {
            "modelling_region": {
                "geojson": "https://swift.rc.nectar.org.au/v1/project_id/test_result/10428026-abea-11ec-a3e2-0242ac1a0003/constraint_region.txt",
                "method": "convex hull",
                "radius": 10
            }
        }
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)     
        expected_params = {
            'function': algorithm,
            'modelling_region': {
                'filename': os.path.join(savedir, 'modelling_region.json'),
                "method": "convex hull",
                "radius": 10
            }
        }
        self.assertEqual(newparams, expected_params)
        self.assertEqual(
            info, 
            {
                'datasets': [], 
                'files': [('modelling_region', 'modelling_region')]
            }
        )
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'job')
    @patch('scripts.core.utils.request_obj', return_value=json.load(open('scripts/tests/data/sdm_job_details.json')))
    @patch('scripts.core.utils.fetch_resultsets', side_effect=mocks.fetch_resultsets_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_prepare_parameters_with_job(self, mock1, mock2, mock3):
        # Test parameter
        algorithm = 'cc'
        config = {
            "species_distribution_models": {
                "type": "job",
                "order": 1,
                "rename": {
                    "species_distribution_models": {
                        "type": "modelfile",
                        "key": "uuid",
                        "title": ["R SDM Model object"]
                    },
                    "sdm_projections": {
                        "type": "projectionfile",
                        "title": [
                            "Projection to current climate",
                            "Predicted habitat suitability under current conditions based on cloglog output"
                        ],
                        "search_op": "ANY"
                    },
                    "threshold": {
                        "type": "parameter",
                        "key": "threshold"
                    },
                    "sdm_function": {
                        "type": "parameter",
                        "key": "algorithm"
                    },
                    "species": {
                        "type": "rsparameter",
                        "key": "scientificName"
                    },
                    "subset": {
                        "type": "mdparameter",
                        "key": "subset"
                    },
                    "tails": {
                        "type": "mdparameter",
                        "key": "tails"
                    }
                }
            }
        }
        savedir = mkdtemp()
        params =  {
            "species_distribution_models": {
                "uuid": mocks.SDM_JOB_UUID,  # Same as SDM resultset uuid
                "threshold": 0.6,
                "algorithm": "ann"
            }
        }
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)

        # Check test results
        resultset = json.load(open('scripts/tests/data/resultset.json'))
        md = mocks.get_result(resultset['results'], 'R SDM Model object').get('bccvl:metadata')
        md2 = mocks.get_result(resultset['results'], 'Projection to current climate').get('bccvl:metadata')
        md3 = mocks.get_result(resultset['results'], 'genre:DataGenreSpeciesOccurEnv').get('bccvl:metadata')
        expected_params = {
            'function': algorithm,
            'sdm_function': params['species_distribution_models']['algorithm'],
            'threshold': params['species_distribution_models']['threshold'],
            'subset': None,
            'tails': None,
            'species': md3.get('scientificName'),
            'species_distribution_models': {
                'mimetype': md['mimetype'],
                'url': md['url'],
                'filename': os.path.join(savedir, md['uuid'], os.path.basename(md['url']))

            },
            'sdm_projections': [{
                'url': md2['url'],
                'filename': os.path.join(savedir, md2['uuid'], os.path.basename(md2['url'])),
                'mimetype': md2['mimetype']
            }]
        }
        self.assertEqual(newparams, expected_params)
        self.assertEqual(set(info['datasets']), 
                        set(config['species_distribution_models']['rename'].keys()))
        self.assertEqual(info['files'], [])
  
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'future_dataset_layer')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('scripts.core.utils.fetch_resultsets', side_effect=mocks.fetch_resultsets_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_prepare_parameters_with_future_dataset_layer(self, mock1, mock2, mock3, mock5):
        # Test parameter
        algorithm = 'cc'
        config = {
            "species_distribution_models": {
                "type": "job",
                "order": 1,
                "rename": {
                    "species_distribution_models": {
                        "type": "modelfile",
                        "key": "uuid",
                        "title": ["R SDM Model object"]
                    },
                    "sdm_projections": {
                        "type": "projectionfile",
                        "title": [
                            "Projection to current climate",
                            "Predicted habitat suitability under current conditions based on cloglog output"
                        ],
                        "search_op": "ANY"
                    },
                    "threshold": {
                        "type": "parameter",
                        "key": "threshold"
                    },
                    "sdm_function": {
                        "type": "parameter",
                        "key": "algorithm"
                    },
                    "species": {
                        "type": "rsparameter",
                        "key": "scientificName"
                    },
                    "subset": {
                        "type": "mdparameter",
                        "key": "subset"
                    },
                    "tails": {
                        "type": "mdparameter",
                        "key": "tails"
                    }
                }
            },
            "future_climate_datasets": {
                "type": "future_dataset_layer",
                "order": 2,
                "rename": {
                    "future_climate_datasets": {
                        "type": "dataset_layer"
                    },
                    "selected_future_layers": {
                        "type": "selected_layers"
                    },
                    "gcm": {
                        "type": "mdparameter",
                        "key": "gcm"
                    },
                    "emsc": {
                        "type": "mdparameter",
                        "key": "emsc"
                    },
                    "month": {
                        "type": "mdparameter",
                        "key": "month"
                    },
                    "year": {
                        "type": "mdparameter",
                        "key": "year"
                    },
                    "projection_name": {
                        "type": "create_parameter",
                        "name": "{emsc}_{gcm}_{year}",
                        "nameparts": ["emsc", "gcm", "year"]
                    }
                }
            }
        }
        savedir = mkdtemp()
        params =  {
            "species_distribution_models": {
                "uuid": mocks.SDM_JOB_UUID,  # Same as SMD resultset uuid
                "threshold": 0.6,
                "algorithm": "ann"
            },
            "future_climate_datasets": [mocks.FUTURE_TASCLIM_DATASET_UUID]
        }
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)

        # Check test results
        resultset = json.load(open('scripts/tests/data/resultset.json'))
        md = mocks.get_result(resultset['results'], 'R SDM Model object').get('bccvl:metadata')
        md2 = mocks.get_result(resultset['results'], 'Projection to current climate').get('bccvl:metadata')
        md3 = mocks.get_result(resultset['results'], 'genre:DataGenreSpeciesOccurEnv').get('bccvl:metadata')
        future_dataset = json.load(open('scripts/tests/data/future_tasclim_dataset.json'))
        future_md = future_dataset.get('bccvl:metadata')
        sdm_job_details = json.load(open('scripts/tests/data/sdm_job_details.json'))

        sdm_params = {
            'function': algorithm,
            'sdm_function': params['species_distribution_models']['algorithm'],
            'threshold': params['species_distribution_models']['threshold'],
            'subset': None,
            'tails': None,
            'species': md3['scientificName'],
            'species_distribution_models': {
                'mimetype': md['mimetype'],
                'url': md['url'],
                'filename': os.path.join(savedir, md['uuid'], os.path.basename(md['url']))

            },
            'sdm_projections': [{
                'url': md2['url'],
                'filename': os.path.join(savedir, md2['uuid'], os.path.basename(md2['url'])),
                'mimetype': md2['mimetype']
            }],
        }
        future_rename = config['future_climate_datasets']['rename']
        exp_future_ds =  dict([(k, future_md.get(v['key'])) 
                                    for k, v in future_rename.items() 
                                    if v['type'] == 'mdparameter'])
        exp_future_ds['projection_name'] = f"{future_md['emsc']}_{future_md['gcm']}_{future_md['year']}"
        exp_future_ds['selected_future_layers'] = sdm_job_details['parameters']['predictors'][0]['layers']
        exp_future_ds['future_climate_datasets'] = [
            {
                'layer': layer,
                'url': future_dataset['rangeAlternates']['dmgr:tiff'][layer]['tempurl'],
                'filename': os.path.join(savedir, future_md['uuid'], os.path.basename(future_dataset['rangeAlternates']['dmgr:tiff'][layer]['url'])),
                'type': 'continuous'
            } 
            for layer in sdm_job_details['parameters']['predictors'][0]['layers']
        ]
        
        # Check that derived parameters for SDM model are as expected
        for i in sdm_params:
            self.assertEqual(newparams[i], sdm_params[i])

        # Check that derived parameters for future dataset are as expected
        for i in exp_future_ds:
            if isinstance(newparams[i], list):
                self.assertEqual(len(newparams[i]), len(exp_future_ds[i]))
                for j in newparams[i]:
                    self.assertTrue(j in exp_future_ds[i])
            else:
                self.assertEqual(newparams[i], exp_future_ds[i])
        self.assertEqual(set(info['datasets']), 
                        set(list(config['species_distribution_models']['rename'].keys()) +
                            list(config['future_climate_datasets']['rename'].keys()))
        )
        self.assertEqual(info['files'], [])
  
        # Clean up
        shutil.rmtree(savedir)

    @tag('param', 'jobs')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    #@patch('scripts.core.utils.fetch_resultsets', side_effect=mocks.fetch_resultsets_mock)
    @patch('os.getenv', return_value='http://test-server:9000/resultmanager/result/uuid')
    def test_prepare_parameters_with_jobs(self, mock1, mock2, mock3):
        algorithm = 'ensemble'
        config = {
            "resultsets": {
                "type": "jobs",
                "rename": {
                    "datasets": {
                        "type": "resultsets",
                        "titles": [
                            "Projection to current climate",
                            "Predicted habitat suitability under current conditions based on cloglog output",
                            "Future Projection map"
                        ],
                        "search_op": "ANY"
                    },
                    "thresholds": {
                        "type": "parameter"
                    },
                    "sdm_projections": {
                        "type": "parameter"
                    }
                }
            }
        }
        savedir = mkdtemp()
        params =  {"resultsets": [mocks.CC_RESULTSET_UUID] }
        newparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, {'input': config}, savedir)

        # Check that the params are as expected
        cc_resultset = json.load(open('scripts/tests/data/CC-resultset2.json'))
        cc_projection_map = mocks.get_result(cc_resultset['results'], "Future Projection map") 
        cc_projection_md = cc_projection_map.get('bccvl:metadata')
        sdm_projmodel_md = json.load(open('scripts/tests/data/proj_current_koala_bioclim.dataset.json'))['bccvl:metadata']
        cc_job_params = json.load(open('scripts/tests/data/CC-job.json'))['parameters']
        expected_params = {
            'function': algorithm,
            'datasets': [
                {
                    'mimetype': cc_projection_md['mimetype'],
                    "layer": "Future Projection map",
                    'url': cc_projection_map['rangeAlternates']['dmgr:tiff']['Future Projection map'].get('tempurl'),
                    'filename': os.path.join(savedir, cc_projection_md['uuid'], os.path.basename(cc_projection_md['url']))
                }
            ],
            'sdm_projections': [{
                'filename': os.path.join(savedir, sdm_projmodel_md['uuid'], os.path.basename(sdm_projmodel_md['url'])),
            }],
            'thresholds': [cc_job_params['species_distribution_models']['threshold']]
        }
        self.assertEqual(newparams, expected_params)
