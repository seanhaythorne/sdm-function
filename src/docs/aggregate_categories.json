{
    "name": "Aggregate Categories",
    "description": "Aggregate categories within a spatial layer",
    "algorithm_category": "",
    "dependencies": "bsrmap",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "http://ecocommons.org.au/sdm-function/aggregate_categories.schema.json",
        "title": "Aggregate Categories Function Parameters",
        "description": "Schema for Aggregate Categories Function parameters",
        "type": "object",
        "properties": {
            "spatial": {
                "title": "spatial",
                "description": "A dataset/resultset UUID with a list of a selected layer name.",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset/resultset id",
                        "description": "UUID of environmental dataset/resultset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    },
                    "source_type": {
                        "title": "source type",
                        "description": "Type of input source",
                        "type": "string",
                        "enum": [
                            "dataset",
                            "resultset"
                        ],
                        "default": "dataset"
                    }
                },
                "required": [
                    "uuid",
                    "layers",
                    "source_type"
                ]
            },
            "template": {
                "title": "template",
                "description": "A dataset/resultset UUID with a list of a selected layer name.",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset/resultset id",
                        "description": "UUID of environmental dataset/resultset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    },
                    "source_type": {
                        "title": "source type",
                        "description": "Type of input source",
                        "type": "string",
                        "enum": [
                            "dataset",
                            "resultset"
                        ],
                        "default": "dataset"
                    }
                },
                "required": [
                    "uuid",
                    "layers",
                    "source_type"
                ]
            },
            "categories": {
                "title": "possible categories",
                "description": "A numeric vector of all possible category values.",
                "type": [
                    "null",
                    "array"
                ],
                "items": {
                    "description": "Category value",
                    "type": "number"
                },
                "default": null
            },
            "selected": {
                "title": "selected categories",
                "description": "A numeric vector of selected category values.",
                "type": [
                    "null",
                    "array"
                ],
                "items": {
                    "description": "Category value",
                    "type": "number"
                },
                "default": null
            },
            "binarize": {
                "title": "binarize",
                "description": "Indicate if aggregated cells should be binarized",
                "type": "boolean",
                "default": true
            }
        },
        "required": [
            "spatial",
            "template",
            "categories",
            "selected",
            "binarize"
        ]
    },
    "output": {
        "aggregate_categories.tif": {
            "title": "Aggregate Categories",
            "genre": "DataGenreRiskMappingResult",
            "mimetype": "image/geotiff",
            "layer": "aggregate_categories",
            "data_type": "Continuous"
        },
        "*.R": {
            "title": "Job script",
            "genre": "JobScript",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json"
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript"
        }
    }
}