from .run_sdm import run as sdm_run

def run(*args):
    """ This is a shim to assist in migrating to a more generic run script """
    sdm_run(args)