## Generate CoverageJSON metadata for Geotiff, CSV and generic file types.
## https://covjson.org/

## This code is duplicated in the DataIngester.  
## TODO: Merge with DI implementation and turn into a library or maybe a standalone service.

import os
import uuid
import numpy
import math
import pandas as pd
from pandas.api.types import is_numeric_dtype
import xmltodict
# pylint: disable=import-error
from osgeo import gdal, osr, gdal_array

from typing import Tuple, Dict

from .utils import file_type


def transform_pixel(ds, x: float, y: float) -> Tuple[float, float]:
    """convert pixel to projection unit"""
    xoff, a, b, yoff, d, e = ds.GetGeoTransform()
    return (
        # round lat/lon to 5 digits which is about 1cm at equator
        round(a * x + b * y + xoff, 5),
        round(d * x + e * y + yoff, 5),
    )


def gen_cov_domain_axes(ds) -> Dict:
    """
    calculate raster mid points
    axes ranges are defined as closed interval
    bottom left
    """
    # TODO: replace transform_pixel with gdal.ApplyGeoTransform(gt, x, y)
    p0 = transform_pixel(ds, 0.5, 0.5)
    # top right
    p1 = transform_pixel(ds, ds.RasterXSize - 0.5, ds.RasterYSize - 0.5)
    # TODO: do I need to do anything about image being upside / down?

    return {
        "x": {"start": min(p0[0], p1[0]), "stop": max(p0[0], p1[0]), "num": ds.RasterXSize},
        "y": {"start": min(p0[1], p1[1]), "stop": max(p0[1], p1[1]), "num": ds.RasterYSize},
        # tiffs are only 2 dimensional, so no need to add any further axes
    }


def get_cov_extent(coverage: Dict) -> Dict:
    """
    generate bbox from converage
    :param coverage: covjson axes data
    :return: bbox: bounding box coords
    """
    axes = coverage['domain']['axes']
    x_size = (axes['x']['stop'] - axes['x']['start']) / (axes['x']['num'] - 1) / 2
    y_size = abs((axes['y']['stop'] - axes['y']['start']) / (axes['y']['num'] - 1) / 2)

    # we have to subtract/add half a step to get full extent
    # Can I get rid of sorted here? (now that axes should be sorted already)
    xs = sorted([axes['x']['start'], axes['x']['stop']])
    ys = sorted([axes['y']['start'], axes['y']['stop']])
    xs = [xs[0] - x_size, xs[1] + x_size]
    ys = [ys[0] - y_size, ys[1] + y_size]
    bbox = {
        # 5 decimal digits is roughly 1m
        'left': round(xs[0], 5),
        'bottom': round(ys[0], 5),
        'right': round(xs[1], 5),
        'top': round(ys[1], 5),
    }
    return bbox


# generate coverage for csv experiment result file
# url: url to the file species occurrence with env data, or absence
#
def generate_point_cov(result: Dict, filename: str) -> Dict:
    df = pd.read_csv(filename, na_filter = False)
    cov = {
        "type": "Coverage",
        "domain": {
            "type": "Domain",
            "domainType": "FeatureCollection",
            "axes": {},
            "referencing": [
                {
                    "type": "Point",
                    "columns": ["lon", "lat"],
                    "system": {
                        "type": "GeographicCRS",
                        "id": "http://www.opengis.net/def/crs/EPSG/0/4326",
                        "wkt": 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",'
                               '6378137,298.257223563,AUTHORITY["EPSG","7030"]],'
                               'AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0],'
                               'UNIT["degree",0.0174532925199433],AUTHORITY["EPSG","4326"]]',
                    },
                }
            ],
        },
        "parameters": {},
        "ranges": {},
        "rangeAlternates": {
            "dmgr:csv": {
                "url": result['url']
            }
        },
        "bccvl:metadata": {
            "count": len(df),
            "extent_wgs84": {
                "left": df["lon"].min().item(),
                "bottom": df["lat"].min().item(),
                "right": df["lon"].max().item(),
                "top": df["lat"].max().item(),
            },
            "url": result['url'],
            "uuid": str(uuid.uuid1()),
            "mimetype": "application/csv",
            "layername": result.get("title"),
        },
    }

    # Include other metadata as well
    cov['bccvl:metadata'].update(result)

    # absence does not have species??
    if 'species' in df.columns:
        cov['bccvl:metadata']['scientificName'] = df["species"].unique().tolist()

    # TODO: would be nice to have some sort of axes encoding
    #       but coverage json would require us to put all point coordinates
    #       into an axes object which is not practical for large datasets
    axes = {}
    parameters = {}

    for colname in df.columns:
        parameter = {}
        if colname in ('lon', 'lat'):
            parameter = {
                'type': 'Parameter',
                'observedProperty': {
                    'label': {
                        'en': 'Longitude' if colname == 'lon' else 'Latitude'
                    }
                }
            }
            # unit
            parameter["unit"] = {
                "label": {"en": "degree"},
                "symbol": {
                    "value": "degree_east" if colname == "lon" else "degree_north"
                },
            }
            # ranges
            parameter["dmgr:range"] = [df[colname].min().item(), df[colname].max().item()]
        elif colname == 'species':
            parameter = {
                'type': 'Parameter',
                'observedProperty': {
                    'label': {
                        'en': 'species'
                    }
                }
            }
            # ranges
            parameter["dmgr:range"] = cov['bccvl:metadata']['scientificName']
        else:
            # environmental variables
            parameter = {
                'type': 'Parameter',
                'observedProperty': {
                    'label': {
                        'en': f'environmental variable {colname}'
                    }
                }
            }
            # ranges
            if is_numeric_dtype(df[colname]):
                parameter["dmgr:range"] = [df[colname].min().item(), df[colname].max().item()]
            else:
                parameter["dmgr:range"] = list(set(df[colname]))
        cov['parameters'][colname] = parameter
    return cov


def get_key_value_from_dict(input_data: list or dict, key: str):
    """
    returns the value of key if found in dict, else returns empty list

    Args:
        input_data (list or dict): data to be searched
        key (str): key to searched

    Yields:
        generator: a generator to return object if found.
    """
    if isinstance(input_data, list):
        for list_item in input_data:
            for item in get_key_value_from_dict(list_item, key):
                yield item
    elif isinstance(input_data, dict):
        if key in input_data:
            yield input_data[key]
        for value in input_data.values():
            for item in get_key_value_from_dict(value, key):
                yield item


def get_categories_from_xml_file(filename: str) -> Tuple[dict, list] :

    """
    Searches XML files produced by experiments for category information and returns 2 payloads:

    inputs:
        str: name of xml file.
    Returns:
        dict: category_encoding: Dictionary outlining category values and labels
        list: list of dictionaries outlining category labels in more descriptive human readable way.
    """

    with open(filename) as xml_file:
        data_dict = xmltodict.parse(xml_file.read())
        try:
            value = list(get_key_value_from_dict(data_dict,"Category"))[0]
        except (KeyError, IndexError):
            return None, None
        # cdict = {c.Category:c.value for c in value}
        filtered_values = [v for v in value if v is not None]
        categories = [] # This goes inside observerdProperty
        category_encoding = {} # This goes inside parameters
        counter = 0
        for value in filtered_values:
            # Temporary Adhoc solution to rename category values coming from R code.
            # Due to Andrew (R Developer) unavailability, a temporary solution has to be implemented here
            value = 'Absent - No Change' if value == 'Blank' else value
            value = 'Present - No Change' if value == 'No change' else value
            category_encoding[value] = counter
            categories.append({"id": value, "label": {"en": value}})
            counter = counter + 1
        return category_encoding, categories


def gen_cov_referencing(ds):
    """
    Generate referencing/coordinate system for the raster.
    Assume WGS84 if raster has no coordinate system.
    :param ds: gdal raster object
    :return: Dict, coordinate system
    """
    projection = ds.GetProjection()
    if not projection:
        # Assume WGS84 if no coordinate system
        return [{
            "coordinates": ["x", "y"],
            "system": {
                "type": "GeographicCRS",
                "id": "http://www.opengis.net/def/crs/EPSG/0/4326",
                "wkt": 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,'
                       'AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0],'
                       'UNIT["degree",0.0174532925199433],AUTHORITY["EPSG","4326"]]'
            }
        }]

    crs = osr.SpatialReference(projection)
    # assumes that projection has an EPSG code
    return [{
        "coordinates": ["x", "y"],
        "system": {
            "type": "ProjectedCRS" if crs.IsProjected() else "GeographicCRS",
            "id": f"http://www.opengis.net/def/crs/EPSG/0/{crs.GetAttrValue('AUTHORITY', 1)}",
            "wkt": crs.ExportToWkt()
        }
    }]

def generate_raster_cov(result: Dict, filename: str) -> Dict:
    """
    generate coverage for geotiff file
    :param result: with title
    :param filename: geotiff file
    :return: Dict, coveragejson structure
    """
    def _fix_infinity(val, dtype):
        # Fix infinity value with its max/min value for the data type
        if not math.isinf(val):
            return(val)
        # replace infinity value with its max/min value
        if dtype in (numpy.float16, numpy.float32, numpy.float64):
            return(float(numpy.finfo(dtype).max if val > 0 else numpy.finfo(dtype).min))

        return(int(numpy.iinfo(dtype).max if val > 0 else numpy.iinfo(dtype).min))

    ds_data = gdal.Open(filename)

    cov = {
        "type": "Coverage",
        "domain": {
            "type": "Domain",
            "domainType": "Grid",
            "axes": {},
            "referencing": gen_cov_referencing(ds_data)
        },
        "parameters": {},
        "ranges": {},
        "rangeAlternates": {},
        "bccvl:metadata": {},
    }

    band = ds_data.GetRasterBand(1)
    # Todo: shall recompute stats in R-script??
    band.ComputeStatistics(True)
    dtype = gdal_array.GDALTypeCodeToNumericTypeCode(band.DataType)
    stats = band.GetStatistics(True, True)
    stats = [_fix_infinity(i, dtype) if not numpy.isnan(i) else None for i in stats]
    nodataValue = band.GetNoDataValue()
    if numpy.isnan(nodataValue):
        nodataValue = None

    cov['domain']['axes'] = gen_cov_domain_axes(ds_data)
    cov['parameters'] = {
        # Use title as parameter name as it is unique
        result['title']: {
            'type': 'Parameter',
            'observedProperty': {
                'label': {
                    'en': result['title']
                },
                'dmgr:statistics': {
                    'min': stats[0],
                    'max': stats[1],
                    'mean': stats[2],
                    'stddev': stats[3]
                },
                "dmgr:nodata": nodataValue
            },
        }
    }
    # Find categories in XML file associated with TIF file
    aux_file_name = filename.replace(".tif", ".tif.aux.xml")
    if os.path.isfile(aux_file_name):
        category_encdoing, categories = get_categories_from_xml_file(aux_file_name)
    else:
        categories = None
    # Add categories and categoryEncoding if categories exist. add unit if categories don't exist
    if categories and category_encdoing:
        cov['parameters'][result['title']]['observedProperty']['categories'] = categories
        cov['parameters'][result['title']]['categoryEncoding'] = category_encdoing
    else:
        cov['parameters'][result['title']]['unit'] = {"symbol": ''}

    cov['rangeAlternates'] = {
        'dmgr:tiff': {
            result['title']: {
                "type": "dmgr:TIFF2DArray",
                # datatype should be a json datatype?
                "datatype": 'float',
                "axisNames": ['y', 'x'],
                "shape": [ds_data.RasterYSize, ds_data.RasterXSize],
                # TODO: ... fill in this stuff from appropriate data object ...
                #           need to populate those as well
                "dmgr:band": 1,
                "dmgr:offset": band.GetOffset(),
                "dmgr:scale": band.GetScale(),
                "dmgr:missingValue": nodataValue,
                "dmgr:datatype": 'Float32',
                "url": result['url'],
            }
        }
    }

    # check for auxilliary file
    auxfiles = []
    if 'auxfile' in result:
        auxfiles.append(
            {
                'type': 'PAMDataset',  # this is a auxilliary metadata like RAT
                # construct auxfile path relative to tiffile
                'path': os.path.relpath(result['auxfile'], os.path.dirname(result['url'])),
                'url': result['url']
            }
        )
    cov['bccvl:metadata'] = {
        "extent_wgs84": get_cov_extent(cov),
        "url": result['url'],
        "genre": result.get("genre"),
        # "categories": result.get("categories", "Result"),  # To do: Set to Result or SDM??
        "uuid": str(uuid.uuid1()),
        "mimetype": result["mimetype"],
        "resolution": result.get("resolution"),
        "acknowledgement": result.get("acknowledgement"),
        "auxfiles": auxfiles
    }
    cov['bccvl:metadata'].update(result)
    return cov


def generate_file_cov(result: Dict):
    """
    Generate coverage for file
    """
    md = result.copy()
    md['uuid'] = str(uuid.uuid1())
    datacov = {
        'type': 'File',
        'bccvl:metadata': md,
        'rangeAlternates': {
            'dmgr:file': {
                'url': result['url']
            }
        }
    }
    return datacov


def generate_data_coverage(datafile: str, md: Dict) -> Dict:
    """ Generate data coverage for job result file 
        :param datafile: Data file path
        :param md: Result metadata
    """

    ftype = file_type(md)
    md["type"] = ftype
    if ftype == 'FILE':
        return generate_file_cov(md)
    if ftype == 'POINT':
        cov = generate_point_cov(md, datafile)
    else:
        cov = generate_raster_cov(md, datafile)
    return cov
