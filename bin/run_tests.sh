#!/bin/bash
set -e

TEST_IMAGE=${TEST_IMAGE:-"utils"}

docker-compose run \
	-e RESULT_CONTAINER='test' \
	-e JOB_RESULT_API_URL='https://test/' \
	--rm $TEST_IMAGE ./manage.py test ./scripts/tests "$@"
