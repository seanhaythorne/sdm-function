class DatasetError(Exception):
    pass

class JobError(Exception):
    pass

class ResultError(Exception):
    pass

class ParameterError(Exception):
    pass

class MetadataError(Exception):
    pass