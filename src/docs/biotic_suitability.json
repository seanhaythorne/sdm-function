{
    "name": "Biotic Suitability",
    "description": "Combines biotic layers",
    "algorithm_category": "",
    "dependencies": "bsrmap",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "http://ecocommons.org.au/sdm-function/biotic_suitability.schema.json",
        "title": "Biotic Suitability Function Parameters",
        "description": "Schema for Biotic Suitability Function parameters",
        "type": "object",
        "properties": {
            "predictors": {
                "title": "predictors",
                "description": "List of environmental dataset/resultset UUID with a list of selected layer names.",
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "uuid": {
                            "title": "dataset id",
                            "description": "UUID of environmental dataset/resultset",
                            "type": "string",
                            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                        },
                        "layers": {
                            "title": "layers",
                            "description": "List of layer names",
                            "type": "array",
                            "items": {
                                "description": "Layer name",
                                "type": "string"
                            },
                            "minItems": 1,
                            "uniqueItems": true
                        },
                        "source_type": {
                            "title": "source type",
                            "description": "Type of input source",
                            "type": "string",
                            "enum": [
                                "dataset",
                                "resultset"
                            ],
                            "default": "dataset"
                        }
                    },
                    "required": [
                        "uuid",
                        "layers",
                        "source_type"
                    ]
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "template": {
                "title": "Risk mapping template dataset/resultset file",
                "description": "A dataset/resultset UUID with a list of a selected layer name.",
                "type": [
                    "object",
                    "null"
                ],
                "default": null,
                "properties": {
                    "uuid": {
                        "title": "dataset/resultset id",
                        "description": "UUID of dataset/resultset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    },
                    "source_type": {
                        "title": "source type",
                        "description": "Type of input source",
                        "type": "string",
                        "enum": [
                            "dataset",
                            "resultset"
                        ],
                        "default": "dataset"
                    }
                },
                "required": [
                    "uuid",
                    "layers",
                    "source_type"
                ]
            },
            "normalize": {
                "title": "normalize",
                "description": "Indicating if the conformed cells should be normalized.",
                "type": "boolean",
                "default": false
            },
            "binarize": {
                "title": "binarize",
                "description": "Indicating if the combined cells should be binarized.",
                "type": "boolean",
                "default": false
            },
            "use_fun": {
                "title": "Use function",
                "description": "use function",
                "type": "string",
                "enum": [
                    "prod",
                    "sum",
                    "union"
                ],
                "default": "prod"
            },
            "na.rm": {
                "title": "Remove Null data",
                "description": "if true, remove null data",
                "type": "boolean",
                "default": false
            }
        },
        "required": [
            "predictors",
            "template",
            "use_fun",
            "na.rm"
        ]
    },
    "output": {
        "biotic_suitability.tif": {
            "title": "Biotic Suitability",
            "genre": "DataGenreRiskMappingResult",
            "mimetype": "image/geotiff",
            "layer": "biotic_suitability",
            "data_type": "Continuous"
        },
        "*.R": {
            "title": "Job script",
            "genre": "JobScript",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json"
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript"
        }
    }
}