import os
import io
import json
import shutil
import zipfile
from tempfile import NamedTemporaryFile, TemporaryDirectory, mkdtemp
from unittest.mock import patch
from django.test import TestCase, tag
from django.conf import settings
import scripts.core.utils
import scripts.core.metadata
import scripts.tests.mocks as mocks
from scripts.prepare_inputs import SCHEMA_DIR_DEFAULT
from scripts.core.metadata import Metadata
from dump import dump

class TestMetadata(TestCase):

    @tag('metadata')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('scripts.core.metadata.Metadata.get_job_metadata', side_effect=mocks.job_metadata_mock)
    @patch('scripts.core.metadata.Metadata.fetch_schema', side_effect=mocks.fetch_schema)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_generate_job_metadata_with_dataset_layer_dataset_or_resultset(self, mock1, mock2, mock3, mock4, mock5):
        algorithm = 'resultset_schema'

        for title, lookup, uuid, source_type in [
            ('Projection to current climate', 'genre:DataGenreCP', mocks.SDM_RESULTSET_UUID, 'resultset'),
            ('distribute_features_template', 'genre:DataGenreRiskMapping', mocks.ENV_TIF_DATASET_UUID, 'dataset')
        ]: 
            params = { 
                'pathway_layers_single': {
                    'uuid': uuid,
                    'layers': [title] if title else [],
                    'source_type': source_type
                },
                'pathway_layers_plural': [{
                    'uuid': uuid,
                    'layers': [title] if title else [],
                    'source_type': source_type
                }]
            }
            pconfig = {
                "input": {
                    "pathway_layers_single": {
                        "type": "dataset_layer",
                        "resultset_files": {
                            "titles": [lookup],
                            "search_op": "ANY"
                        },
                        'plural': 'single'
                    },
                    "pathway_layers_plural": {
                        "type": "dataset_layer",
                        "resultset_files": {
                            "titles": [lookup],
                            "search_op": "ANY"
                        }
                    }
                },
                "metadata": {
                    "Pathway Likelihood Layer": "pathway_layers_single",
                    "Pathway Likelihood Layers": "pathway_layers_plural",
                    "Citation": "default"
                }
            }
            savedir = mkdtemp()
            pparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, pconfig, savedir)

            self.assertEqual(info, 
                {
                    'datasets': ['pathway_layers_single', 'pathway_layers_plural'],
                    'files': []
                }
            )

            script_params = {
                "user": None,
                "env": {
                    "workdir": savedir,
                    "inputdir": f"{savedir}/",
                    "outputdir": f"{savedir}/",
                    "scriptdir": f"{savedir}/"
                }
            }

            script_params['params'] = pparams

            md = Metadata(schemadir = SCHEMA_DIR_DEFAULT)

            # Test generate metadata
            meta = md.generate_job_metadata(mocks.SDM_JOB_UUID, params, pparams, pconfig, script_params['env'], [])

            # TODO testing successful execution but not validating output yet
            #print(meta)

            # Clean up
            shutil.rmtree(savedir)

    @tag('metadata')
    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('scripts.core.metadata.Metadata.get_job_metadata', side_effect=mocks.job_metadata_mock)
    @patch('scripts.core.metadata.Metadata.fetch_schema', side_effect=mocks.fetch_schema)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_generate_job_metadata_with_dataset_layer(self, mock1, mock2, mock3, mock4, mock5):
        dscov = json.load(open('scripts/tests/data/env_tif_dataset.json'))
        algorithm = 'bsrmap_conform_layer'

        for title, lookup, uuid, source_type in [
            ('distribute_features_template', 'genre:DataGenreRiskMapping', mocks.ENV_TIF_DATASET_UUID, 'dataset')
        ]: 
            params = {
              "function": algorithm,
              "template": {
                "source_type": "dataset",
                "uuid": uuid,
                "layers": list(dscov['parameters'].keys())
              },
              "spatial": {
                "source_type": "dataset",
                "uuid": uuid,
                "layers": list(dscov['parameters'].keys())
              },
              "normalize": False,
              "binarize": False
            }
            pconfig = json.load(open('scripts/tests/data/bsrmap_conform_layer.pconfig.json'))

            savedir = mkdtemp()
            pparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, pconfig, savedir)

            self.assertEqual(info, 
                {
                    'datasets': ['spatial', 'template'],
                    'files': []
                }
            )

            script_params = {
                "user": None,
                "env": {
                    "workdir": savedir,
                    "inputdir": f"{savedir}/",
                    "outputdir": f"{savedir}/",
                    "scriptdir": f"{savedir}/"
                }
            }

            script_params['params'] = pparams

            md = Metadata(schemadir = SCHEMA_DIR_DEFAULT)

            # Test generate metadata
            meta = md.generate_job_metadata(mocks.SDM_JOB_UUID, params, pparams, pconfig, script_params['env'], [])

            # TODO testing successful execution but not validating output yet
            #print(meta)

            # Clean up
            shutil.rmtree(savedir)


    @patch('scripts.core.utils.fetch_data')
    @patch('scripts.core.utils.request_obj', side_effect=mocks.request_object_mock)
    @patch('scripts.core.metadata.Metadata.get_job_metadata', side_effect=mocks.job_metadata_mock)
    @patch('scripts.core.metadata.Metadata.fetch_schema', side_effect=mocks.fetch_schema)
    @patch('os.getenv', return_value='http://test-server:8000/datamanager/uuid')
    def test_generate_job_metadata_with_dataset_layer_plural_dataset_and_resultset(self, mock1, mock2, mock3, mock4, mock5):
        algorithm = 'resultset_schema'
        params = { 
            'pathway_layers_plural': [
                {
                    'uuid': mocks.SDM_RESULTSET_UUID,
                    'layers': ['Projection to current climate'],
                    'source_type': 'resultset'
                },
                {
                    'uuid': mocks.ENV_TIF_DATASET_UUID,
                    'layers': ['distribute_features_template'],
                    'source_type': 'dataset'
                }
            ]
        }
        config = {
            "input": {
                "pathway_layers_plural": {
                    "type": "dataset_layer",
                    "resultset_files": {
                        "titles": ['genre:DataGenreCP'],
                        "search_op": "ANY"
                    }
                }
            },
            "metadata": {
                "Pathway Likelihood Layers": "pathway_layers_plural",
                "Citation": "default"
            }
        }
        savedir = mkdtemp()
        pparams, info = scripts.core.parameters.prepare_parameters(algorithm, params, config, savedir)

        self.assertEqual(info, 
            {
                'datasets': ['pathway_layers_plural'],
                'files': []
            }
        )

        script_params = {
            "user": None,
            "env": {
                "workdir": savedir,
                "inputdir": f"{savedir}/",
                "outputdir": f"{savedir}/",
                "scriptdir": f"{savedir}/"
            }
        }

        script_params['params'] = pparams

        md = Metadata(schemadir = SCHEMA_DIR_DEFAULT)

        # Test generate metadata
        meta = md.generate_job_metadata(mocks.SDM_JOB_UUID, params, pparams, config, script_params['env'], [])

        # TODO testing successful execution but not validating output yet
        #print(meta)

        # Clean up
        shutil.rmtree(savedir)

