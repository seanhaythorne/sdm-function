#!/usr/bin/env nextflow

params.jobUuid = "job-uuid"
params.volOption = "--volume $baseDir:/sdmwork"
params.envfile = "--env-file $baseDir/envfile.txt"
params.inputs = "$baseDir/params.json"
Channel.fromPath(params.inputs, checkIfExists: true).set{ input_ch } 

algorithm = "cc"
workdir = params.volOption.split(':')[1]
jobuuid = params.jobUuid

process prepareInputs2 {
    
    containerOptions params.volOption + " " + params.envfile


    input:
    file x from input_ch

    output:
    path 'input_params.json' into input_params 

    """
    python /srv/app/manage.py runscript prepare_inputs --script-args uuid=$params.jobUuid algorithm=$algorithm params=$x workdir=$workdir  jobuuid=$jobuuid > prepareinput.log
    """
}

process runSDM2 {

    containerOptions params.volOption

    // use local disk in $TMPDIR for scratch
    scratch true

    //publishDir '/Users/s2976990/dev/nextflow/results', mode: 'copy', overwrite: true

    input:
    file x from input_params

    output:
    path 'results.zip' into results
    path 'jobinfo.json' into jobinfo

    """
    python /srv/app/manage.py runscript run_sdm --script-args params=$x > runSDM.log
    """
}

process uploadResult {

    containerOptions params.volOption + " " + params.envfile

    input:
    path result_zip from results
    path job_info from jobinfo

    output:
    file 'uploadResult.log'

    """
    python /srv/app/manage.py runscript upload --script-args uuid=$params.jobUuid result=$result_zip jobinfo=$job_info > uploadResult.log
    """
}

//results.subscribe { println it }
