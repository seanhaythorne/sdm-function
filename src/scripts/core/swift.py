import os
import os.path
import logging
from io import BytesIO
from urllib.parse import urlparse, urljoin
from django.conf import settings
from swiftclient.service import SwiftService, SwiftUploadObject, get_conn
from swiftclient.utils import generate_temp_url

log = logging.getLogger(__name__)

class SwiftStore:
    def __init__(self):
        self.swift = SwiftService()

        # workaround to discover storage_url
        self.conn = get_conn(self.swift._options)

        # need to make at least one request
        self.conn.head_account()

    def rooturl(self):
        return self.conn.url

    def get_swiftUrl(self, container: str, objname):
        storage_url = os.getenv('OS_STORAGE_URL')
        return os.path.join(storage_url, container, objname)

    def getContainerPath(self, url: str):
        # return the container and the object path of the swift url
        # To do: Improve this?
        parts = url.split(self.conn.url)
        if not parts[0]:
            parts = parts[1].split('/')
            if len(parts) > 2:
                return parts[1], '/'.join(parts[2:])
        return None

    def upload(self, container: str, uploadObjs):
        # Up load data file and its associated converage json files to object store
        # Check and return errors if any
        objs = [SwiftUploadObject(**obj) for obj in uploadObjs]
        errors = []
        for result in self.swift.upload(container, objs):
            if result['success']:
                continue
            errors.append(result['error'])
        if errors:
            raise Exception(f"Swift upload failed: {str(errors)}")

    def delete(self, container: str, delObjs):
        errors = []
        for result in self.swift.delete(container, delObjs):
            if result['success']:
                continue
            errors.append(result['errors'])
        if errors:
            raise Exception(f"Swift delete failed: {str(errors)}")

    def list(self, container: str, prefix):
        objs = []
        results = self.swift.list(container, {'prefix': prefix})
        for result in results:
            if result['success']:
                objs.extend([item['name'] for item in result['listing']])
        return objs

    def download(self, container: str, obj):
        resp_headers, content = self.conn.get_object(container, obj)
        buf = BytesIO(content)
        buf.name = os.path.basename(obj)
        buf.mode = "rb"
        buf.seek(0)
        return buf

    def temp_url(self, url):
        # validate url against our swift settings (swift service)
        if not settings.TEMP_URL_KEY:
            return url

        urlparts = urlparse(url)
        # scheme, netloc(port), path
        # generate swift path /v1/<account>/<container>/<object_path>
        # TODO: valid for 5 minutes
        temp_url = generate_temp_url(
            urlparts.path, 300, settings.TEMP_URL_KEY, method='GET'
        )
        return urljoin(url, temp_url)
