{
    "input": {
        "traits_dataset": {
            "type": "dataset"
        },
        "predictors": {
            "rename": "environmental_datasets",
            "type": "dataset_layer"
        },
        "generate_convexhull": {
            "type": "parameter",
            "default": false
        },
        "modelling_region": {
            "type": "parameter",
            "download_as": {
                "key": "geojson",
                "rekey": "filename",
                "filename": "modelling_region.json"
            }
        }
    },
    "script": [
        "{function}/{function}.R"
    ],
    "metadata": {
        "Species Traits Dataset ": {
            "param": "traits_dataset",
            "link_param": "traits_dataset_params"
        },
        "Predictors": "predictors",
        "Citation": "default"
    },
    "output": {
        "files": {
            "eval_tables/*_results.txt": {
                "title": "Model summary",
                "genre": "DataGenreSTResult",
                "mimetype": "text/plain",
                "order": 1
            },
            "eval_tables/*_trait_environmental.csv": {
                "title": "Trait data with environmental data",
                "genre": "DataGenreSpeciesOccurEnv",
                "mimetype": "text/csv",
                "order": 2
            },
            "*.R": {
                "title": "Job Script",
                "genre": "JobScript",
                "mimetype": "text/x-r",
                "order": 9
            },
             "*.Rout": {
                "title": "Log file",
                "genre": "DataGenreLog",
                "mimetype": "text/x-r-transcript",
                "order": 10
            },
            "modelling_region.json": {
                "title": "modelling region",
                "hidden": true,
                "genre": "DataGenreSDMModellingRegion",
                "mimetype": "text/x-r-transcript",
                "order": 11
            },
            "metadata.json": {
                "title": "Metadata",
                "genre": "DataGenreMetadata",
                "mimetype": "application/json",
                "order": 12
            },
            "*.rds": {
                "title": "R Species Traits Model object",
                "genre": "DataGenreSTModel",
                "mimetype": "application/x-r-data",
                "order": 20
            },
            "params.json": {
                "title": "Input parameters",
                "genre": "InputParams",
                "mimetype": "text/x-r-transcript",
                "order": 100
            }
        }
    }
}
