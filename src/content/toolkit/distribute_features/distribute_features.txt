{
    "input": {
        "template": {
            "type": "dataset_layer",
            "plural": "single",
            "resultset_files": {
                "titles": ["genre:DataGenreRiskMappingResult"],
                "search_op": "ANY"
            }
        },
        "feature": {
            "type": "dataset",
            "plural": "single"
        }
    },
    "script": [
        "{function}/{function}.R"
    ],
    "metadata": {
        "Feature": "feature",
        "Template": "template",
        "Citation": "default"
    },
    "output": {
        "files": {
            "distribute_features.tif": {
                "title": "Distribute Features Layer",
                "genre": "DataGenreRiskMappingResult",
                "mimetype": "image/geotiff",
                "layer": "distribute_features",
                "data_type": "Continuous",
                "order": 1
            },
            "*.R": {
                "title": "Job script",
                "genre": "JobScript",
                "mimetype": "text/x-r",
                "order": 10
            },
            "*.Rout": {
                "title": "Log file",
                "genre": "DataGenreLog",
                "mimetype": "text/x-r-transcript",
                "order": 11
            },
            "metadata.json": {
                "title": "Metadata",
                "genre": "DataGenreMetadata",
                "mimetype": "application/json",
                "order": 12
            },
            "params.json": {
                "title": "Input parameters",
                "genre": "InputParams",
                "mimetype": "text/x-r-transcript",
                "order": 15
            }
        }
    }
}
